import { Optional } from 'shared/util/functional';
import { WebAddress, webOf } from 'shared/model/address/address';
import { AddressParser } from './address-parser';

const webAddressRegex = new RegExp(
  // http://urlregex.com/
  // tslint:disable-next-line:max-line-length
  /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/);

export class WebAddressParser implements AddressParser<WebAddress> {
  parse(text: string): Optional<WebAddress> {
    return webAddressRegex.test(text) ? webOf(text) : undefined;
  }
}
