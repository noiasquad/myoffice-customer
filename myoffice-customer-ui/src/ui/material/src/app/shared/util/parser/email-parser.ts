import { EmailAddress, emailOf } from 'shared/model/address/address';
import { Optional } from 'shared/util/functional';
import { AddressParser } from './address-parser';

const emailAddressRegex = new RegExp(
  // https://emailregex.com/
  // tslint:disable-next-line:max-line-length
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

export class EmailAddressParser implements AddressParser<EmailAddress> {
  parse(text: string): Optional<EmailAddress> {
    return emailAddressRegex.test(text) ? emailOf(text) : undefined;
  }
}
