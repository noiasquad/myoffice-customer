import { PhoneAddress, phoneOf } from 'shared/model/address/address';
import { Optional } from 'shared/util/functional';
import { AddressParser } from './address-parser';

const phoneAddressRegex = new RegExp(
  // http://phoneregex.com/
  // tslint:disable-next-line:max-line-length
  /^\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$/);

export class PhoneAddressParser implements AddressParser<PhoneAddress> {
  parse(text: string): Optional<PhoneAddress> {
    try {
      const PNF = require('google-libphonenumber').PhoneNumberFormat;
      const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
      const number = phoneUtil.parse(text, 'CH');
      return phoneUtil.isPossibleNumber(number) ? phoneOf(phoneUtil.format(number, PNF.INTERNATIONAL)) : undefined;
    } catch (e) {
      return undefined;
    }
  }
}
