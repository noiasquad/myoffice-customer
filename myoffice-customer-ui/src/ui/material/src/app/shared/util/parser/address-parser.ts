import { Optional } from 'shared/util/functional';
import { Address, PostalAddress, postalOf } from 'shared/model/address/address';
import { EmailAddressParser } from './email-parser';
import { PhoneAddressParser } from './phone-parser';
import { WebAddressParser } from './web-url-parser';

export interface AddressParser<T extends Address> {
  parse(text: string): Optional<T>;
}

class PostalAddressParser implements AddressParser<PostalAddress> {
  parse(text: string): Optional<PostalAddress> {
    // https://smartystreets.com/articles/regular-expressions-for-street-addresses
    // no validation by elimination of the other cases xx
    return postalOf(text, '', '', '', '');
  }
}

export class AnyAddressParser implements AddressParser<Address> {

  private parsers: AddressParser<Address>[];

  constructor() {
    this.parsers = [
      new EmailAddressParser(),
      new WebAddressParser(),
      new PhoneAddressParser(),
      new PostalAddressParser()
    ];
  }

  parse(text: string): Optional<Address> {
    let address: Optional<Address>;
    if (text.length > 0) {
      this.parsers.forEach(parser => {
        address = address ? address : parser.parse(text);
      });
    }
    return address;
  }
}
