import {Observable} from 'rxjs';
import {CData, PData, SData} from 'shared/model/common/data';
import {FilterCombination} from 'shared/model/common/filter';
import {SortCombination} from 'shared/model/common/sort';
import {FindService} from './find.service';
import {UpdateService} from './update.service';
import {PageSpec} from 'shared/model/common/pageable';

export class BaseCrudService<I, T> implements FindService<I, T>, UpdateService<I, T> {

  constructor(private finder: FindService<I, T>, private updater: UpdateService<I, T>) {
  }

  findOne$(id: I): Observable<SData<T>> {
    return this.finder.findOne$(id);
  }

  findOnes$(ids: I[]): Observable<CData<T>> {
    return this.finder.findOnes$(ids);
  }

  findAll$(page?: PageSpec, sorts?: SortCombination): Observable<PData<T>> {
    return this.finder.findAll$(page, sorts);
  }

  findSome$(filters?: FilterCombination, page?: PageSpec, sorts?: SortCombination): Observable<PData<T>> {
    return this.finder.findSome$(filters, page, sorts);
  }

  search$(term: string, page?: PageSpec, sorts?: SortCombination): Observable<PData<T>> {
    return this.finder.search$(term, page, sorts);
  }

  put$(state: T, id?: I): Observable<SData<T>> {
    return this.updater.put$(state, id);
  }

  remove$(id: I): Observable<any> {
    return this.updater.remove$(id);
  }
}
