import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UpdateService} from '../update.service';
import {SData} from 'shared/model/common/data';

export class HttpUpdateService<I extends string, T> implements UpdateService<I, T> {

  constructor(private http: HttpClient, private baseUrl: string) {
  }

  put$(state: T, id?: I): Observable<SData<T>> {
    return id ?
      this.http.put<T>(`${this.baseUrl}/${id}`, state) :
      this.http.post<T>(`${this.baseUrl}`, state);
  }

  remove$(id: I): Observable<any> {
    return this.http.delete<any>(`${this.baseUrl}/${id}`);
  }
}
