import {HttpParams} from '@angular/common/http';
import {SortCombination} from 'shared/model/common/sort';
import {FilterCombination} from 'shared/model/common/filter';
import {PageSpec} from 'shared/model/common/pageable';

export class HttpQueryBuilder {
  private query = new HttpParams();

  build(): HttpParams {
    return this.query;
  }

  param(name: string, value?: string | number): HttpQueryBuilder {
    this.query = value ? this.query.append(name, `${value}`) : this.query;
    return this;
  }

  page(spec?: PageSpec): HttpQueryBuilder {
    return spec ? this.param('page', `${spec.number}`) : this;
  }

  term(term: string): HttpQueryBuilder {
    return this.param('term', term);
  }

  sorts(sorts?: SortCombination): HttpQueryBuilder {
    this.query = sorts ? this.encodeSorts(this.query, sorts) : this.query;
    return this;
  }

  filters(filters?: FilterCombination): HttpQueryBuilder {
    this.query = filters ? this.encodeFilters(this.query, filters) : this.query;
    return this;
  }

  ones(ids: string[]) {
    ids.forEach(id => this.query = this.query.append('id', id));
    return this;
  }

  private encodeSorts(query: HttpParams, sorts: SortCombination): HttpParams {
    sorts.forEach(sort =>
      query = query.append('sort', `${sort.attribute},${sort.direction}`));
    return query;
  }

  private encodeFilters(query: HttpParams, filters: FilterCombination): HttpParams {
    filters.forEach(filter =>
      query = query.append('filter[]', `${filter.attribute}${filter.operation}${filter.value}`));
    return query;
  }
}
