import {Observable} from 'rxjs';
import {CData, PData, SData} from 'shared/model/common/data';
import {FilterCombination} from 'shared/model/common/filter';
import {SortCombination} from 'shared/model/common/sort';
import {PageSpec} from 'shared/model/common/pageable';

export interface FindService<I, T> {

  findOne$(id: I): Observable<SData<T>>;

  findOnes$(ids: I[]): Observable<CData<T>>;

  findAll$(page?: PageSpec, sorts?: SortCombination): Observable<PData<T>>;

  findSome$(filters?: FilterCombination, page?: PageSpec, sorts?: SortCombination): Observable<PData<T>>;

  search$(term: string, page?: PageSpec, sorts?: SortCombination): Observable<PData<T>>;
}
