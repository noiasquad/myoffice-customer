import {Observable} from 'rxjs';
import {SData} from 'shared/model/common/data';

export interface UpdateService<I, S> {

  put$(state: S, id?: I): Observable<SData<S>>;

  remove$(id: I): Observable<any>;
}
