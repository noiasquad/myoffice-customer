import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CData, PData, SData} from 'shared/model/common/data';
import {FilterCombination} from 'shared/model/common/filter';
import {SortCombination} from 'shared/model/common/sort';
import {FindService} from 'shared/repository/find.service';
import {HttpQueryBuilder} from './http-query';
import {PageSpec} from 'shared/model/common/pageable';

export class HttpFindService<I extends string, T> implements FindService<I, T> {

  constructor(private http: HttpClient, private baseUrl: string) {
  }

  findOne$(id: I): Observable<SData<T>> {
    return this.http.get<SData<T>>(`${this.baseUrl}/${id}`);
  }

  findOnes$(ids: I[]): Observable<CData<T>> {
    return this.http.get<CData<T>>(`${this.baseUrl}/ones`, {
      params: new HttpQueryBuilder().ones(ids).build()
    });
  }

  findAll$(page?: PageSpec, sorts?: SortCombination): Observable<PData<T>> {
    return this.http.get<PData<T>>(this.baseUrl, {
      params: new HttpQueryBuilder().page(page).sorts(sorts).build()
    });
  }

  findSome$(filters?: FilterCombination, page?: PageSpec, sorts?: SortCombination): Observable<PData<T>> {
    return this.http.get<PData<T>>(this.baseUrl, {
      params: new HttpQueryBuilder().page(page).sorts(sorts).filters(filters).build()
    });
  }

  search$(term: string, page?: PageSpec, sorts?: SortCombination): Observable<PData<T>> {
    return this.http.get<PData<T>>(`${this.baseUrl}/search`, {
      params: new HttpQueryBuilder().term(term).page(page).sorts(sorts).build()
    });
  }
}
