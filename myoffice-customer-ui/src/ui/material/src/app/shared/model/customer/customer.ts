import {Optional} from 'shared/util/functional';
import {Address, AddressType} from 'shared/model/address/address';
import {Person} from 'shared/model/person/person';
import { FolderId } from '../folder/folder';

export class CustomerState {
  person: Person;
  addresses: Address[] = [];
  profile: Profile;
  notes?: string;
  affiliations?: FolderId[];

  static from(state: CustomerState): CustomerState {
    return Object.assign(new CustomerState(), {
      ...state,
      person: Person.from(state.person),
      addresses: state.addresses ? state.addresses.map(a => Address.from(a)) : []
    });
  }

  postalAddress(): Optional<Address> {
    return this.addresses.find(a => AddressType.POSTAL === a.type);
  }

  address(): Optional<Address> {
    const postal = this.postalAddress();
    return postal ? postal : this.addresses.find(a => AddressType.POSTAL !== a.type);
  }
}

export interface Profile {
  hasMoved: boolean;
  isSubcontractor: boolean;
  sendInvitation: boolean;
}

export type CustomerId = string;
export type Customer = { id: CustomerId } & CustomerState;
export const customerOf = (id: CustomerId, state: CustomerState) => <Customer>{...state, id: id};
