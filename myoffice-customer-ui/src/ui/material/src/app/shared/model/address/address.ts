export type Street = string;
export type Zip = string;
export type City = string;
export type Country = string;
export type Region = string;
export type PhoneNumber = string;
export type Email = string;
export type WebUrl = string;

export enum AddressType {
  POSTAL = 'POSTAL',
  PHONE = 'PHONE',
  EMAIL = 'EMAIL',
  WEB = 'WEB'
}

export abstract class Address {
  type: AddressType;
  tags?: string;

  static from(address: Address): Address {
    switch (address.type) {
      case AddressType.POSTAL:
        return Object.assign(new PostalAddress(), address);
      case AddressType.PHONE:
        return Object.assign(new PhoneAddress(), address);
      case AddressType.EMAIL:
        return Object.assign(new EmailAddress(), address);
      case AddressType.WEB:
        return Object.assign(new WebAddress(), address);
    }
  }

  abstract format(): string;

  protected formatTags(): string {
    return this.tags ? ` (${this.tags})` : '';
  }
}

export class PostalAddress extends Address {
  street: Street;
  houseNbr: string;
  zip: Zip;
  city: City;
  country: Country;
  region?: Region;

  format(): string {
    const houseNbrFragment = this.houseNbr ? `${this.houseNbr}, ` : '';
    const regionFragment = this.region ? ` (${this.region})` : '';
    return `${houseNbrFragment}${this.street}, ${this.zip} ${this.city}${regionFragment}, ${this.country}` + this.formatTags();
  }
}

export class PhoneAddress extends Address {
  number: PhoneNumber;

  format(): string {
    return this.number + this.formatTags();
  }
}

export class EmailAddress extends Address {
  email: Email;

  format(): string {
    return this.email + this.formatTags();
  }
}

export class WebAddress extends Address {
  url: WebUrl;

  format(): string {
    return this.url + this.formatTags();
  }
}

export const postalOf = (s: Street, no: string, z: Zip, ci: City, co: Country, r?: string) =>
  <PostalAddress>{type: AddressType.POSTAL, street: s, houseNbr: no, zip: z, city: ci, country: co, region: r};
export const phoneOf = (s: PhoneNumber) => <PhoneAddress>{type: AddressType.PHONE, number: s};
export const emailOf = (s: Email) => <EmailAddress>{type: AddressType.EMAIL, email: s};
export const webOf = (s: WebUrl) => <WebAddress>{type: AddressType.WEB, url: s};

export const ADDRESS_TAGS: string[] = [
  'HOME', 'OFFICE', 'MOBILE', 'LINKEDIN', 'FACEBOOK', 'GOOGLEPLUS', 'TWITTER', 'INSTAGRAM', 'SKYPE', 'INTERNET'
];
