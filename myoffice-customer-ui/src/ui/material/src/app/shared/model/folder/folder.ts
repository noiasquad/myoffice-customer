import { CustomerId } from '../customer/customer';

export interface FolderState {
  name: string;
  notes?: string;
  affiliates: CustomerId[];
  primaryAffiliate: CustomerId;
}

export type FolderId = string;
export type Folder = { id: FolderId } & FolderState;
