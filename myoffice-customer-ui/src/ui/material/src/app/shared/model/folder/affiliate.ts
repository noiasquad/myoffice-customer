import { CustomerId } from '../customer/customer';
import { FolderId } from './folder';
import { Person } from '../person/person';

export class Affiliate {
  constructor(
    public readonly customerId: CustomerId,
    public readonly person?: Person) { }
}

export class Affiliation {
  constructor(
    public readonly folderId: FolderId,
    public readonly name?: string) { }
}
