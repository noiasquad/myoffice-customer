import {ifDefinedMap} from 'shared/util/functional';
import {Resource} from 'shared/model/common/resource';
import {Customer, CustomerId} from './customer';
import { resourceLinks, hasLink } from '../common/data';

const ID_FRAGMENT_REGEX = new RegExp(
  // https://emailregex.com/
  // tslint:disable-next-line:max-line-length
  /^.*\/customers\/(.*)$/);

export class CustomerResource {

  static selfId(resource: Resource<Customer>): CustomerId {
    const href = ifDefinedMap(resourceLinks(resource)['self'], self => self.href);
    const match = href && href.match(ID_FRAGMENT_REGEX);
    return match ? match[1] : '';
  }

  static canBeModified(resource: Resource<Customer>): boolean {
    return hasLink(resource, 'patch');
  }

  static canBeRemoved(resource: Resource<Customer>): boolean {
    return hasLink(resource, 'remove');
  }
}
