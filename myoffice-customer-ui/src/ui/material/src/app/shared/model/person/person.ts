export class Person {
  salutation?: Salutation;
  firstName?: string;
  lastName: string;
  gender?: Gender;
  birthDate?: string;

  static from(state: Person): Person {
    return Object.assign(new Person(), state);
  }

  name(): string {
    return this.nameFL();
  }

  nameFL(): string {
    return this.firstName ? `${this.firstName} ${this.lastName}` : this.lastName;
  }

  nameLF(): string {
    return this.firstName ? `${this.lastName} ${this.firstName}` : this.lastName;
  }
}

export enum Gender {
  MALE = 'MALE',
  FEMALE = 'FEMALE'
}

export enum Salutation {
  SIR = 'Monsieur',
  MISS = 'Madame'
}

export const salutationByGender: { [key: string]: Salutation } = {
  MALE: Salutation.SIR,
  FEMALE: Salutation.MISS
};

export const SALUTATIONS: string[] = [Salutation.SIR, Salutation.MISS];

export const personOf = (firstName: string, lastName: string, gender: Gender) => <Person>{
  firstName: firstName, lastName: lastName, gender: gender
};
