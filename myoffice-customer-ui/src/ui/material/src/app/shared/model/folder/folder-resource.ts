import {ifDefinedMap} from 'shared/util/functional';
import {Resource} from 'shared/model/common/resource';
import {Folder, FolderId} from './folder';
import { hasLink, resourceLinks } from '../common/data';

const ID_FRAGMENT_REGEX = new RegExp(
  // https://emailregex.com/
  // tslint:disable-next-line:max-line-length
  /^.*\/folders\/(.*)$/);

export class FolderResource {

  static selfId(resource: Resource<Folder>): FolderId {
    const href = ifDefinedMap(resourceLinks(resource)['self'], self => self.href);
    const match = href && href.match(ID_FRAGMENT_REGEX);
    return match ? match[1] : '';
  }

  static canBeModified(resource: Resource<Folder>): boolean {
    return hasLink(resource, 'patch');
  }

  static canBeRemoved(resource: Resource<Folder>): boolean {
    return hasLink(resource, 'remove');
  }
}
