import { OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UUID } from 'angular2-uuid';
import { Address } from 'shared/model/address/address';
import { EditableListItemComponent } from 'shared/component/list/editable-list-item/editable-list-item.component';

export abstract class AddressListItemEditComponent<T extends Address>
  extends EditableListItemComponent<T, UUID>
  implements OnInit {

  @Input() advancedEditingOn = false;
  fgAddress: FormGroup;

  ngOnInit(): void {
    this.buildForm(this.data);
    this.fgAddress.patchValue(this.data);
  }

  abstract buildForm(data: T): void;

  isValid(): boolean {
    return this.fgAddress.valid;
  }

  getData(): T {
    const base = this.data as T;
    return {type: base.type, ...this.fgAddress.value} as T;
  }
}
