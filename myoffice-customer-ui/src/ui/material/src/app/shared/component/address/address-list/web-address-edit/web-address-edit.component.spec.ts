import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {WebAddressEditComponent} from './web-address-edit.component';

describe('WebAddressEditComponent', () => {
  let component: WebAddressEditComponent;
  let fixture: ComponentFixture<WebAddressEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WebAddressEditComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebAddressEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
