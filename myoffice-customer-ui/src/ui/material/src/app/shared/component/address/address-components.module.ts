import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialComponentsModule} from 'shared/component/material/material-components.module';
import {AddressListEditComponent} from './address-list/address-list-edit/address-list-edit.component';
import {AddressListItemComponent} from './address-list/address-list-item/address-list-item.component';
import {EmailAddressEditComponent} from './address-list/email-address-edit/email-address-edit.component';
import {PhoneAddressEditComponent} from './address-list/phone-address-edit/phone-address-edit.component';
import {PostalAddressEditComponent} from './address-list/postal-address-edit/postal-address-edit.component';
import {WebAddressEditComponent} from './address-list/web-address-edit/web-address-edit.component';
import {AddressSelectorComponent} from './address-selector/address-selector.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialComponentsModule
  ],
  declarations: [
    AddressSelectorComponent,
    AddressListEditComponent,
    EmailAddressEditComponent,
    PhoneAddressEditComponent,
    WebAddressEditComponent,
    PostalAddressEditComponent,
    AddressListItemComponent
  ],
  exports: [
    AddressSelectorComponent,
    EmailAddressEditComponent,
    PhoneAddressEditComponent,
    WebAddressEditComponent,
    AddressListEditComponent,
    AddressListItemComponent
  ],
  providers: [],
})
export class AddressComponentsModule {
}
