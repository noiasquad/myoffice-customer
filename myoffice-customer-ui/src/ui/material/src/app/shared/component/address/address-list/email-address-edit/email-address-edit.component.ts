import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EmailAddress} from 'shared/model/address/address';
import {AddressListItemEditComponent} from '../address-list-item-edit/address-list-item-edit.component';

@Component({
  selector: 'app-email-address-edit',
  templateUrl: './email-address-edit.component.html',
  styleUrls: ['./email-address-edit.component.css']
})
export class EmailAddressEditComponent extends AddressListItemEditComponent<EmailAddress> {

  buildForm(address: EmailAddress): void {
    this.fgAddress = new FormGroup({
      email: new FormControl(address.email, {validators: [Validators.required, Validators.email]}),
      tags: new FormControl(address.tags)
    });
  }
}
