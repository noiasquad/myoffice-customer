import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {PhoneAddress} from 'shared/model/address/address';
import {AddressListItemEditComponent} from '../address-list-item-edit/address-list-item-edit.component';

@Component({
  selector: 'app-phone-address-edit',
  templateUrl: './phone-address-edit.component.html',
  styleUrls: ['./phone-address-edit.component.css']
})
export class PhoneAddressEditComponent extends AddressListItemEditComponent<PhoneAddress> {

  buildForm(address: PhoneAddress): void {
    this.fgAddress = new FormGroup({
      number: new FormControl(address.number, {validators: [Validators.required]}),
      tags: new FormControl(address.tags)
    });
  }
}
