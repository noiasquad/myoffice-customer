import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialComponentsModule} from 'shared/component/material/material-components.module';
import {PersonSelectorComponent} from './person-selector/person-selector.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialComponentsModule
  ],
  declarations: [
    PersonSelectorComponent
  ],
  exports: [
    PersonSelectorComponent
  ],
  providers: [],
})
export class PersonComponentsModule {
}
