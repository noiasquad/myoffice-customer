import {Component} from '@angular/core';
import {AddressListItemEditComponent} from '../address-list-item-edit/address-list-item-edit.component';
import {PostalAddress} from 'shared/model/address/address';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-postal-address-edit',
  templateUrl: './postal-address-edit.component.html',
  styleUrls: ['./postal-address-edit.component.css']
})
export class PostalAddressEditComponent extends AddressListItemEditComponent<PostalAddress> {

  buildForm(address: PostalAddress): void {
    this.fgAddress = new FormGroup({
      street: new FormControl(address.street, {validators: Validators.required}),
      houseNbr: new FormControl(address.houseNbr),
      zip: new FormControl(address.zip, {validators: Validators.required}),
      city: new FormControl(address.city, {validators: Validators.required}),
      country: new FormControl(address.country, {validators: Validators.required}),
      tags: new FormControl(address.tags)
    });
  }
}
