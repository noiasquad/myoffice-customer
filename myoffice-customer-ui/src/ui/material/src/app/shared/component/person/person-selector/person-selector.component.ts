import { AfterViewInit, Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { PersonClient } from 'core/api/person/person-client.service';
import { never, Observable, Subscription, NEVER } from 'rxjs';
import { switchMap, filter } from 'rxjs/operators';
import { Person } from 'shared/model/person/person';
import { isDefined } from '@angular/compiler/src/util';

@Component({
  selector: 'app-person-selector',
  templateUrl: './person-selector.component.html',
  styleUrls: ['./person-selector.component.css']
})
export class PersonSelectorComponent implements OnInit, AfterViewInit, OnDestroy {

  @Output()
  onSelect = new EventEmitter<Person>();
  fcPerson: FormControl;

  private subscriptions: Subscription[] = [];

  constructor(private personService: PersonClient) {
  }

  ngOnInit() {
    this.buildSelectorField();
  }

  ngAfterViewInit(): void {
    this.subscribeToNameChanges();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
  }

  private buildSelectorField(): void {
    this.fcPerson = new FormControl('', { updateOn: 'blur' });
  }

  private subscribeToNameChanges(): void {
    this.subscriptions.push(this.fcPerson.valueChanges
      .pipe(
        filter(term => term && term.length > 0),
        switchMap(name => this.parsePerson(name)))
      .subscribe(person => this.onPersonSelected(person)));
  }

  private parsePerson(name: string): Observable<Person> {
    return this.personService.identify(name);
  }

  private onPersonSelected(person: Person): void {
    this.onSelect.emit(person);
  }
}
