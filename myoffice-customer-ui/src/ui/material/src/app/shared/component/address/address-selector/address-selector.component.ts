import { AfterViewInit, Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PostalAddressClient } from 'core/api/address/postal-address-client.service';
import { Observable, Subscription } from 'rxjs';
import { filter, map, mergeMap } from 'rxjs/operators';
import { Address, AddressType, City, EmailAddress, PhoneAddress,
  PostalAddress, postalOf, WebAddress, Zip } from 'shared/model/address/address';
import { Consumer, ifDefinedDo, Optional } from 'shared/util/functional';
import { groupBy } from 'shared/util/groupBy';
import { AnyAddressParser } from 'shared/util/parser/address-parser';
import { isDefined } from '@angular/compiler/src/util';

@Component({
  selector: 'app-address-selector',
  templateUrl: './address-selector.component.html',
  styleUrls: ['./address-selector.component.css']
})
export class AddressSelectorComponent implements OnInit, AfterViewInit, OnDestroy {

  @Output() onSelect = new EventEmitter<Address>();

  fcAddress: FormControl;
  fcZipSelection: FormControl;
  fcPostalAddressSelection: FormControl;
  fgPostalAddress: FormGroup;
  fgPhoneAddress: FormGroup;
  fgEmailAddress: FormGroup;
  fgWebAddress: FormGroup;

  postalAddress: Optional<PostalAddress>;
  phoneAddress: Optional<PhoneAddress>;
  emailAddress: Optional<EmailAddress>;
  webAddress: Optional<WebAddress>;
  selectableZips: Optional<[Zip, City][]>;
  selectablePostalAddresses: Optional<PostalAddress[]>;

  private addressParser: AnyAddressParser;
  private streetsByZip: Optional<Map<Zip, PostalAddress[]>>;
  private subscriptions: Subscription[] = [];
  private handlers = new Map<AddressType, Consumer<Address>>()
    .set(AddressType.PHONE, a => this.onPhoneAddressRecognized(a as unknown as PhoneAddress))
    .set(AddressType.EMAIL, a => this.onEmailAddressRecognized(a as unknown as EmailAddress))
    .set(AddressType.WEB, a => this.onWebAddressRecognized(a as unknown as WebAddress))
    .set(AddressType.POSTAL, a => this.onPostalAddressRecognized(a as unknown as PostalAddress));

  constructor(private postalAddressService: PostalAddressClient) {
    this.addressParser = new AnyAddressParser();
  }

  ngOnInit(): void {
    this.buildForm();
  }

  ngAfterViewInit(): void {
    this.subscribeToAddressChanges();
    this.subscribeToZipSelections();
    this.subscribeToPostalAddressSelections();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
  }

  hasSeveralAddresses(): boolean {
    return !!(this.selectablePostalAddresses && this.selectablePostalAddresses.length > 1);
  }

  hasSeveralZips(): boolean {
    return !!(this.selectableZips && this.selectableZips.length > 1);
  }

  zipAndCities(): Array<[Zip, City]> {
    return this.selectableZips ? this.selectableZips : [];
  }

  private subscribeToAddressChanges(): void {
    this.subscriptions.push(
      this.fcAddress.valueChanges
        .pipe(
          filter(text => text && text.length > 0),
          map(text => this.addressParser.parse(text)),
          filter(isDefined))
        .subscribe(address =>
          address && ifDefinedDo(this.handlers.get(address.type), handler => handler(address))));
  }

  private subscribeToZipSelections(): void {
    this.subscriptions.push(
      this.fcZipSelection.valueChanges.subscribe(zip =>
        this.editPostalAddresses(this.streetsFromZip(zip), zip)));
  }

  private subscribeToPostalAddressSelections(): void {
    this.subscriptions.push(
      this.fcPostalAddressSelection.valueChanges.subscribe(address =>
        this.editPostalAddress(address)));
  }

  private buildForm(): void {
    this.fcAddress = new FormControl('', { updateOn: 'blur' });
    this.fcZipSelection = new FormControl('');
    this.fcPostalAddressSelection = new FormControl('');
  }

  private onPhoneAddressRecognized(address: PhoneAddress): void {
    this.onSelect.emit(address);
    this.fcAddress.reset();
  }

  private onEmailAddressRecognized(address: EmailAddress): void {
    this.onSelect.emit(address);
    this.fcAddress.reset();
  }

  private onWebAddressRecognized(address: WebAddress): void {
    this.onSelect.emit(address);
    this.fcAddress.reset();
  }

  private onPostalAddressRecognized(address: PostalAddress): void {
    this.findRegisteredStreets$(address).subscribe(streets => this.editPostalAddresses(streets));
  }

  private findRegisteredStreets$(streetOrZip: PostalAddress): Observable<PostalAddress[]> {
    const street = streetOrZip;
    const zip = postalOf('', '', streetOrZip.street, '', '');
    return this.postalAddressService.findStreets$(street)
      .pipe(mergeMap((streetsAnyZip: PostalAddress[]) =>
        this.postalAddressService.findStreets$(zip, 50)
          .pipe(map((streetsOfZip: PostalAddress[]) => [...streetsAnyZip, ...streetsOfZip]))));
  }

  private streetsFromZip(zip: Zip): PostalAddress[] {
    return this.streetsByZip && this.streetsByZip.get(zip) || [];
  }

  private editPostalAddresses(streets: PostalAddress[], selectedZip?: Zip): void {
    this.selectablePostalAddresses = undefined;
    this.selectableZips = undefined;

    if (streets.length > 1) {
      if (selectedZip) {
        this.selectablePostalAddresses = streets;
      } else {
        this.streetsByZip = groupBy(streets, a => a.zip);
        if (this.streetsByZip.size > 1) {
          this.selectableZips = Array.from(this.streetsByZip.entries()).map(t => [t[0], t[1][0].city]) as [Zip, City][];
        } else {
          this.selectablePostalAddresses = this.streetsByZip.get(streets[0].zip);
        }
      }
    } else if (streets.length === 1) {
      this.editPostalAddress(streets[0]);
    } else {
      this.editPostalAddress(postalOf('', '', '', '', ''));
    }
  }

  private editPostalAddress(address: PostalAddress): void {
    this.selectablePostalAddresses = undefined;
    this.onSelect.emit(address);
    this.fcAddress.reset();
  }
}
