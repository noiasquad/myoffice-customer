import { Component, OnInit } from '@angular/core';
import { BaseListItemComponent } from 'shared/component/list/base-list-item/base-list-item.component';
import { Address, AddressType } from 'shared/model/address/address';

interface Model {
  icon: string;
  value: string;
}

@Component({
  selector: 'app-address-list-item',
  templateUrl: './address-list-item.component.html',
  styleUrls: ['./address-list-item.component.css']
})
export class AddressListItemComponent extends BaseListItemComponent<Address, void> implements OnInit {

  model: Model;

  ngOnInit(): void {
    this.model = Mapper.toModel(this.data);
  }

  getData(): Address {
    return this.data;
  }
}

class Mapper {
  static toModel(data: Address): Model {
    return {
      icon: Iconifier.toIcon(data),
      value: data.format()
    } as Model;
  }
}

class Iconifier {
  static toIcon(data: Address): string {
    switch (data.type) {
      case AddressType.POSTAL:
        return 'home';
      case AddressType.EMAIL:
        return 'email';
      case AddressType.PHONE:
        return 'phone';
      case AddressType.WEB:
        return 'web';
    }
  }
}
