import { Component } from '@angular/core';
import { UUID } from 'angular2-uuid';
import { EditableListComponent } from 'shared/component/list/editable-list/editable-list.component';
import { Address, AddressType } from 'shared/model/address/address';
import { Holder } from 'shared/util/holder';
import { Identity } from 'shared/util/identity';
import { AddressListItemEditComponent } from '../address-list-item-edit/address-list-item-edit.component';
import { log } from 'shared/util/decorator/logging/log-decorator';

type Model = Holder<Address> & Identity<UUID>;

@Component({
  selector: 'app-address-list-edit',
  templateUrl: './address-list-edit.component.html',
  styleUrls: ['./address-list-edit.component.css']
})
export class AddressListEditComponent
extends EditableListComponent<Address, UUID, AddressListItemEditComponent<Address>> {

  advancedEditingOn = false;

  sort(): void {
    this.items = this.items.sort(ItemComparator.compare);
  }

  toModel(data: Address): Model {
    return {
      data: data,
      id: UUID.UUID()
    };
  }

  toggleAdvancedEditing(): void {
    this.advancedEditingOn = !this.advancedEditingOn;
    this.forEachComponent(it => it.advancedEditingOn = this.advancedEditingOn);
  }
}

class ItemComparator {
  private static addressOrder = new Map<AddressType, number>()
    .set(AddressType.POSTAL, 1)
    .set(AddressType.PHONE, 2)
    .set(AddressType.EMAIL, 3)
    .set(AddressType.WEB, 4);

  static compare(a: Holder<Address>, b: Holder<Address>): number {
    const aOrder = ItemComparator.addressOrder.get(a.data.type) || 1;
    const bOrder = ItemComparator.addressOrder.get(b.data.type) || 1;
    return aOrder < bOrder ? -1 : 1;
  }
}
