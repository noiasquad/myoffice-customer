import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddressListEditComponent} from './address-list-edit.component';

describe('AddressListEditComponent', () => {
  let component: AddressListEditComponent;
  let fixture: ComponentFixture<AddressListEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddressListEditComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressListEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
