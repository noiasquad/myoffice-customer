import {Component} from '@angular/core';
import {AddressListItemEditComponent} from '../address-list-item-edit/address-list-item-edit.component';
import {WebAddress} from 'shared/model/address/address';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-web-address-edit',
  templateUrl: './web-address-edit.component.html',
  styleUrls: ['./web-address-edit.component.css']
})
export class WebAddressEditComponent extends AddressListItemEditComponent<WebAddress> {

  buildForm(address: WebAddress): void {
    this.fgAddress = new FormGroup({
      url: new FormControl(address.url, {validators: [Validators.required]}),
      tags: new FormControl(address.tags)
    });
  }
}

