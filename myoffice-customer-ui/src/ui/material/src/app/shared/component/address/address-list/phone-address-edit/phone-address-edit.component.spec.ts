import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PhoneAddressEditComponent} from './phone-address-edit.component';

describe('PhoneAddressEditComponent', () => {
  let component: PhoneAddressEditComponent;
  let fixture: ComponentFixture<PhoneAddressEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PhoneAddressEditComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhoneAddressEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
