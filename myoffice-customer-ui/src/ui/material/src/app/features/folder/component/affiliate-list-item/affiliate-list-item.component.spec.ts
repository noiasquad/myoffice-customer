import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffiliateListItemComponent } from './affiliate-list-item.component';

describe('AffiliateListItemComponent', () => {
  let component: AffiliateListItemComponent;
  let fixture: ComponentFixture<AffiliateListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffiliateListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffiliateListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
