import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FolderService } from 'folder/service/folder.service';
import { BaseListComponent } from 'shared/component/list/base-list/base-list.component';
import { CustomerId } from 'shared/model/customer/customer';
import { Affiliate } from 'shared/model/folder/affiliate';
import { FolderId } from 'shared/model/folder/folder';
import { ifDefinedDo, ifDefinedMapElseDefault, Optional } from 'shared/util/functional';
import { Holder } from 'shared/util/holder';
import { Identity } from 'shared/util/identity';
import { AffiliateListItemComponent } from '../affiliate-list-item/affiliate-list-item.component';

type Model = Holder<Affiliate> & Identity<CustomerId> & {
  isPrimary: boolean;
};

@Component({
  selector: 'app-affiliate-list',
  templateUrl: './affiliate-list.component.html',
  styleUrls: ['./affiliate-list.component.css']
})
export class AffiliateListComponent
  extends BaseListComponent<Affiliate, CustomerId, AffiliateListItemComponent>
  implements OnChanges {

  @Input() folderId: Optional<FolderId>;
  @Input() primaryAffiliate: Optional<CustomerId>;
  @Input() readOnly = true;
  @Output() onPromote: EventEmitter<CustomerId> = new EventEmitter();

  constructor(private service: FolderService) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    ifDefinedDo(changes.folderId, change =>
      ifDefinedDo(change.currentValue, id =>
        this.service.loadAffiliates$(id).subscribe(as => this.setData(as))));
  }

  addData(data: Affiliate): void {
    ifDefinedDo(data.person,
      _ => super.addData(data),
      _ => this.service.loadAffiliate$(data.customerId).subscribe(a => super.addData(a)));
  }

  removeData(id: CustomerId): void {
    super.removeData(id);
    if (id === this.primaryAffiliate) {
      this.onPromote.emit(this.primaryAffiliate = undefined);
    }
  }

  sort(): void {
    this.items.sort(ItemComparator.compare);
  }

  toModel(affiliate: Affiliate): Model {
    return {
      data: affiliate,
      id: affiliate.customerId,
      isPrimary: ifDefinedMapElseDefault(this.primaryAffiliate, id => id === affiliate.customerId, () => false)
    };
  }

  promoteAffiliate(id: CustomerId): void {
    this.onPromote.emit(this.primaryAffiliate = id);
    const promoted = this.items.filter(it => it.id === id).map(it => this.toModel(it.data));
    this.items = this.items.filter(it => it.id !== id).concat(promoted);
  }
}

class ItemComparator {
  static compare(a: Model, b: Model): number {
    return a.isPrimary ? -1 : b.isPrimary ? 1 : 0;
  }
}
