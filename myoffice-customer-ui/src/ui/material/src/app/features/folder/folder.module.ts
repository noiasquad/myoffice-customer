import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ListModule } from 'shared/component/list/list.module';
import { MaterialComponentsModule } from 'shared/component/material/material-components.module';
import { AffiliateListItemComponent } from './component/affiliate-list-item/affiliate-list-item.component';
import { AffiliateListComponent } from './component/affiliate-list/affiliate-list.component';
import { CustomerSelectorComponent } from './component/customer-selector/customer-selector.component';
import { FolderEditComponent } from './component/folder-edit/folder-edit.component';
import { FolderListComponent } from './component/folder-list/folder-list.component';
import { FolderShowComponent } from './component/folder-show/folder-show.component';
import { FolderRoutingModule } from './folder-routing.module';
import { FolderService } from './service/folder.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    MaterialComponentsModule,
    CommonModule,
    FolderRoutingModule,
    ListModule
  ],
  declarations: [
    AffiliateListComponent,
    CustomerSelectorComponent,
    FolderListComponent,
    FolderEditComponent,
    FolderShowComponent,
    AffiliateListItemComponent,
  ],
  exports: [
    AffiliateListComponent,
    CustomerSelectorComponent,
    FolderListComponent,
    FolderEditComponent,
    FolderShowComponent,
  ],
  entryComponents: [
    FolderShowComponent,
  ],
  providers: [
    FolderService
  ],
})
export class FolderModule {
  constructor(folderService: FolderService) {
    folderService.initialize();
  }
}
