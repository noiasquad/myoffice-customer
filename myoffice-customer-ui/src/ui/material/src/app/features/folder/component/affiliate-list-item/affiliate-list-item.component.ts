import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BaseListItemComponent } from 'shared/component/list/base-list-item/base-list-item.component';
import { CustomerId } from 'shared/model/customer/customer';
import { Affiliate } from 'shared/model/folder/affiliate';

@Component({
  selector: 'app-affiliate-list-item',
  templateUrl: './affiliate-list-item.component.html',
  styleUrls: ['./affiliate-list-item.component.css']
})
export class AffiliateListItemComponent extends BaseListItemComponent<Affiliate, CustomerId> {

  @Input() isPrimary: boolean;
  @Input() readOnly = true;
  @Output() onPromote: EventEmitter<CustomerId> = new EventEmitter<CustomerId>();

  promote(): void {
    this.onPromote.emit(this.id);
  }
}
