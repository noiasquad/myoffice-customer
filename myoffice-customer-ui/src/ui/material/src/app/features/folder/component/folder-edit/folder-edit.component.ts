import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FolderService } from 'folder/service/folder.service';
import { CustomerId } from 'shared/model/customer/customer';
import { Affiliate } from 'shared/model/folder/affiliate';
import { FolderId, FolderState, Folder } from 'shared/model/folder/folder';
import { ifDefinedDo, Optional } from 'shared/util/functional';
import { AffiliateListComponent } from '../affiliate-list/affiliate-list.component';

@Component({
  selector: 'app-folder-edit',
  templateUrl: './folder-edit.component.html',
  styleUrls: ['./folder-edit.component.css']
})
export class FolderEditComponent implements OnInit {

  folderId: Optional<FolderId>;
  readonly model: Model;
  readonly form: Form;

  @ViewChild(AffiliateListComponent)
  private affiliateList: AffiliateListComponent;

  constructor(
    private service: FolderService,
    private route: ActivatedRoute,
    private router: Router) {
    this.form = new Form();
    this.model = new Model();
  }

  ngOnInit() {
    ifDefinedDo(this.folderId = this.route.snapshot.params['id'],
      id => this.loadFolder(id),
      _ => this.prepareNewFolder());
  }

  canBeSubmitted(): boolean {
    return this.form.isValid();
  }

  submitForm() {
    this.service.updateFolder({
      ...this.form.getData(),
      affiliates: this.affiliateList.getData().map(a => a.customerId),
      primaryAffiliate: this.model.primaryAffiliate
    } as FolderState, this.folderId);
  }

  cancelEdition() {
    this.router.navigate(['/folders']);
  }

  onAffiliateSelected(id: CustomerId): void {
    this.affiliateList.addData(new Affiliate(id));
  }

  onAffiliateRemoved(id: CustomerId): void {
    this.affiliateList.removeData(id);
  }

  onAffiliatePromoted(id: CustomerId): void {
    this.model.primaryAffiliate = id;
  }

  private loadFolder(id: FolderId) {
    this.service.loadFolder$(id).subscribe(data => this.onFolderLoaded(data));
  }

  private onFolderLoaded(data: FolderState): void {
    this.form.setData(data);
    this.model.name = data.name;
    this.model.primaryAffiliate = data.primaryAffiliate;
  }

  private prepareNewFolder() {
    this.service.createdFolderOnce$().subscribe(data => this.onFolderCreated(data));
  }

  private onFolderCreated(data: Folder): void {
    this.folderId = data.id;
    this.form.setData(data);
    this.model.name = data.name;
    this.model.primaryAffiliate = data.primaryAffiliate;
  }
}

class Form {
  readonly fgFolder: FormGroup;

  constructor() {
    this.fgFolder = new FormGroup({
      name: new FormControl('', { validators: Validators.required }),
      notes: new FormControl(''),
    });
  }

  isValid(): boolean {
    return this.fgFolder.valid;
  }

  setData(data: FolderState): void {
    this.fgFolder.patchValue({ ...data });
  }

  getData(): object {
    return { ...this.fgFolder.value };
  }

  resetNotes(): void {
    this.fgFolder.controls['notes'].setValue('');
  }
}

class Model {
  name: string;
  primaryAffiliate: Optional<CustomerId>;
}
