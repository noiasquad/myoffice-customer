import {AfterViewInit, Component, OnInit, Type, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Select, Store} from '@ngxs/store';
import {FindSomeFolders, SearchFolders} from 'core/features/command/folder-commands';
import {FolderListState} from 'core/features/state/folder-list-state';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {ListDataSource} from 'shared/component/list/mat-list/mat-list-datasource';
import {MatListComponent} from 'shared/component/list/mat-list/mat-list.component';
import {SData} from 'shared/model/common/data';
import {FilterCombination} from 'shared/model/common/filter';
import {DEFAULT_PAGE, PageInfo, PageSpec} from 'shared/model/common/pageable';
import {Operation} from 'shared/model/common/resource';
import {Term} from 'shared/model/common/search';
import {ascOf, SortCombination} from 'shared/model/common/sort';
import {Folder, FolderId} from 'shared/model/folder/folder';
import {isDefinedPredicate} from 'shared/util/empty';
import {FolderShowComponent} from '../folder-show/folder-show.component';

interface Model {
  data: FolderId;
  name: string;
}

@Component({
  selector: 'app-folder-list',
  template: `
    <app-list [columnNames]="['name']"
              [itemDataComponentTypeFn]="dataComponentType"
              [datasource]="datasource"
              [pageInfo$]="pageInfo$"
              [operations$]="operations$"
              (refresh)="refreshFolderList($event)"
              (page)="goToPageOfFolderList($event)"
              (filter)="filterFolderList($event)"
              (sort)="sortFolderList($event)"
              (search)="searchFolders($event)"
              (create)="navigateToFolderEdition()"
    >
    </app-list>`,
})
export class FolderListComponent implements OnInit, AfterViewInit {

  @ViewChild(MatListComponent)
  private listComponent: MatListComponent<Model>;
  private _pageInfo$: Observable<PageInfo>;
  @Select(FolderListState.operations)
  private _operations$: Observable<Operation[]>;

  get operations$(): Observable<Operation[]> {
    return this._operations$;
  }

  constructor(private store: Store, private router: Router) {
  }

  private _datasource: ListDataSource<Model>;

  get datasource(): ListDataSource<Model> {
    return this._datasource;
  }

  get pageInfo$(): Observable<PageInfo> {
    return this._pageInfo$;
  }

  ngOnInit(): void {
    this.initializePageInfoStream();
    this.initializeDataSource();
  }

  dataComponentType(data: FolderId): Type<any> {
    return FolderShowComponent;
  }

  private initializePageInfoStream(): void {
    this._pageInfo$ = this.store.select(FolderListState.page).pipe(filter(isDefinedPredicate));
  }

  private initializeDataSource(): void {
    this._datasource = new ListDataSource<Model>(
      this.store.select(FolderListState.data).pipe(
        filter(isDefinedPredicate),
        map(data => Mapper.toModel(data))));
  }

  refreshFolderList(event?: PageSpec): void {
    this.sortFolderList([event ? event : DEFAULT_PAGE, [], [ascOf('name')]]);
  }

  sortFolderList(event: [PageSpec, FilterCombination, SortCombination]): void {
    this.store.dispatch(new FindSomeFolders(Mapper.toFolderFilter(event[1]), event[0], Mapper.toFolderSort(event[2])));
  }

  ngAfterViewInit(): void {
    this.listComponent.sort('name', 'asc');
  }

  filterFolderList(event: [PageSpec, FilterCombination, SortCombination]): void {
    this.store.dispatch(new FindSomeFolders(Mapper.toFolderFilter(event[1]), event[0], Mapper.toFolderSort(event[2])));
  }

  searchFolders(event: [Term, PageSpec, SortCombination]): void {
    this.store.dispatch(new SearchFolders(event[0], event[1], Mapper.toFolderSort(event[2])));
  }

  goToPageOfFolderList(event: [PageSpec, FilterCombination, SortCombination]): void {
    this.store.dispatch(new FindSomeFolders(Mapper.toFolderFilter(event[1]), event[0], Mapper.toFolderSort(event[2])));
  }

  navigateToFolderEdition(): void {
    this.router.navigate(['/folders/edit']).catch(e => console.log(e));
  }
}

class Mapper {
  static toModel(data: SData<Folder>[]): Model[] {
    return data.map(folder => ({
      data: folder.id,
      name: folder.name
    }) as Model);
  }

  static toFolderFilter(fc: FilterCombination): FilterCombination {
    return fc;
  }

  static toFolderSort(sc: SortCombination): SortCombination {
    return sc;
  }
}
