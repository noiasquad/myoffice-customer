import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { CustomerRepository } from 'core/api/customer/customer-repository.service';
import { Observable, timer } from 'rxjs';
import { debounce, filter, map, startWith, switchMap } from 'rxjs/operators';
import { SData } from 'shared/model/common/data';
import { Customer, CustomerId, CustomerState } from 'shared/model/customer/customer';
import { isNotEmptyPredicate } from 'shared/util/empty';
import { ifDefinedDo, Optional, ifDefinedMapElseDefault } from 'shared/util/functional';

@Component({
  selector: 'app-customer-selector',
  templateUrl: './customer-selector.component.html',
  styleUrls: ['./customer-selector.component.css']
})
export class CustomerSelectorComponent implements OnInit {

  @Output()
  onSelect = new EventEmitter<CustomerId>();
  candidates$: Observable<SData<CustomerState>[]>;

  private fcCustomer: FormControl;

  constructor(private customerRepository: CustomerRepository) {
  }

  ngOnInit(): void {
    this.buildSelectorField();
    this.initializeCustomerStream();
  }

  reset(): void {
    this.fcCustomer.reset();
  }

  canBeApplied(): boolean {
    return !!this.fcCustomer.value;
  }

  displayFn(customer?: Customer): Optional<string> {
    return ifDefinedMapElseDefault(customer, c => `${c.person.lastName} ${c.person.firstName}`, () => '');
  }

  select(): void {
    ifDefinedDo(this.fcCustomer.value, value => {
      this.onSelect.emit(value.id);
      this.reset();
    });
  }

  private buildSelectorField(): void {
    this.fcCustomer = new FormControl('');
  }

  private initializeCustomerStream(): void {
    this.candidates$ = this.fcCustomer.valueChanges
      .pipe(
        startWith<string | CustomerState>(''),
        filter(isNotEmptyPredicate),
        map(value => typeof value === 'string' ? value : value.person && value.person.lastName),
        debounce(() => timer(1000)),
        switchMap(term => this.customerRepository.search$(term)),
        map(page => page.content));
  }
}
