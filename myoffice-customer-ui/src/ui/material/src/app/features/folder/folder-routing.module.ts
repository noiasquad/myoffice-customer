import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FolderListComponent} from './component/folder-list/folder-list.component';
import {FolderEditComponent} from './component/folder-edit/folder-edit.component';

const routes: Routes = [
  {
    path: '',
    component: FolderListComponent
  },
  {
    path: 'edit',
    component: FolderEditComponent
  },
  {
    path: 'edit/:id',
    component: FolderEditComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class FolderRoutingModule {
}
