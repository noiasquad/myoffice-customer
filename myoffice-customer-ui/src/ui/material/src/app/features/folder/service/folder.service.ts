import { isDefined } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { Actions, ofActionDispatched, Store } from '@ngxs/store';
import { LoadCustomer, LoadCustomers } from 'core/features/command/customer-commands';
import { LoadFolder, RemoveFolder, UpdateFolder } from 'core/features/command/folder-commands';
import { FolderCreated } from 'core/features/event/folder-events';
import { CustomerListState } from 'core/features/state/customer-list-state';
import { FolderListState } from 'core/features/state/folder-list-state';
import { Observable, of } from 'rxjs';
import { filter, map, switchMap, take } from 'rxjs/operators';
import { CustomerId } from 'shared/model/customer/customer';
import { Affiliate } from 'shared/model/folder/affiliate';
import { Folder, FolderId, FolderState } from 'shared/model/folder/folder';
import { ifDefinedMapElseDefault } from 'shared/util/functional';

@Injectable()
export class FolderService {

  constructor(private store: Store, private actions$: Actions) { }

  initialize(): void {
  }

  loadFolder$(id: FolderId): Observable<Folder> {
    this.store.dispatch(new LoadFolder(id));
    return this.folderOnce$(id);
  }

  loadAffiliate$(id: CustomerId): Observable<Affiliate> {
    this.store.dispatch(new LoadCustomer(id));
    return this.affiliateOnce$(id);
  }

  loadAffiliates$(id: FolderId): Observable<Affiliate[]> {
    const affiliates$ = this.folderOnce$(id).pipe(
      map(folder => ifDefinedMapElseDefault(folder.affiliates, a => a, () => [])));

    affiliates$.pipe(filter(as => as.length > 0))
      .subscribe(as => this.store.dispatch(new LoadCustomers(as)));

    return affiliates$.pipe(
      switchMap(as => as.length > 0 ?
        this.store.select(CustomerListState.byIds).pipe(
          map(fn => fn(as)),
          map(customers => customers.map(customer => new Affiliate(customer.id, customer.person)))) : of([])));
  }

  updateFolder(state: FolderState, id?: FolderId): void {
    this.store.dispatch(new UpdateFolder(state, id));
  }

  removeFolder(id: FolderId): void {
    this.store.dispatch(new RemoveFolder(id));
  }

  folderOnce$(id: FolderId): Observable<Folder> {
    return this.store.select(FolderListState.byId).pipe(
      map(byId => byId(id)),
      filter(isDefined),
      take(1));
  }

  createdFolderOnce$(): Observable<Folder> {
    return this.actions$.pipe(
      ofActionDispatched(FolderCreated),
      map(event => <Folder>{...event.state, id: event.id}),
      take(1));
  }

  affiliateOnce$(id: CustomerId): Observable<Affiliate> {
    return this.store.select(CustomerListState.byId).pipe(
      map(fn => fn(id)),
      filter(isDefined),
      map(customer => new Affiliate(customer.id, customer.person)),
      take(1));
  }

  affiliatesOnce$(id: CustomerId[]): Observable<Affiliate[]> {
    return this.store.select(CustomerListState.byIds).pipe(
      map(fn => fn(id)),
      filter(customers => customers.length > 0),
      map(customers => customers.map(customer => new Affiliate(customer.id, customer.person))),
      take(1));
  }
}
