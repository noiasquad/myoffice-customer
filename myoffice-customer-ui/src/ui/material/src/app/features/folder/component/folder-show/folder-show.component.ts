import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FolderService } from 'folder/service/folder.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SData } from 'shared/model/common/data';
import { Folder, FolderId, FolderState } from 'shared/model/folder/folder';
import { FolderResource } from 'shared/model/folder/folder-resource';
import { Optional } from 'shared/util/functional';
import { Holder } from 'shared/util/holder';

type Model = FolderState & {
  canBeModified: boolean;
  canBeRemoved: boolean;
};

@Component({
  selector: 'app-folder-show',
  templateUrl: './folder-show.component.html',
  styleUrls: ['./folder-show.component.css']
})
export class FolderShowComponent implements Holder<FolderId> {

  id: FolderId;
  model$: Optional<Observable<Model>>;

  constructor(private service: FolderService, private router: Router) {
  }

  set data(id: FolderId) {
    this.loadFolder(this.id = id);
  }

  modifyFolder(): void {
    this.router.navigate(['folders/edit', this.id]);
  }

  removeFolder(): void {
    this.service.removeFolder(this.id);
  }

  private loadFolder(id: FolderId): void {
    this.model$ = this.service.loadFolder$(id).pipe(map(Mapper.toModel));
  }
}

class Mapper {
  static toModel(data: SData<Folder>): Model {
    return {
      ...data,
      canBeModified: FolderResource.canBeModified(data),
      canBeRemoved: FolderResource.canBeRemoved(data)
    };
  }
}
