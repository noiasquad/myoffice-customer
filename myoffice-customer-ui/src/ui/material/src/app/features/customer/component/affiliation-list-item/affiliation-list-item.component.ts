import { Component, Input } from '@angular/core';
import { BaseListItemComponent } from 'shared/component/list/base-list-item/base-list-item.component';
import { Affiliation } from 'shared/model/folder/affiliate';
import { FolderId } from 'shared/model/folder/folder';

@Component({
  selector: 'app-affiliation-list-item',
  templateUrl: './affiliation-list-item.component.html',
  styleUrls: ['./affiliation-list-item.component.css']
})
export class AffiliationListItemComponent extends BaseListItemComponent<Affiliation, FolderId> {
  @Input() readOnly = true;
}
