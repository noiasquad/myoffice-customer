import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from 'customer/service/customer.service';
import { AddressListEditComponent } from 'shared/component/address/address-list/address-list-edit/address-list-edit.component';
import { Address } from 'shared/model/address/address';
import { CustomerId, CustomerState, Customer } from 'shared/model/customer/customer';
import { Affiliation } from 'shared/model/folder/affiliate';
import { FolderId } from 'shared/model/folder/folder';
import { Person } from 'shared/model/person/person';
import { IsoDate } from 'shared/util/converter/iso-date-converter';
import { ifDefinedDo, Optional } from 'shared/util/functional';
import { AffiliationListComponent } from '../affiliation-list/affiliation-list.component';
import { log } from 'shared/util/decorator/logging/log-decorator';

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.css']
})
export class CustomerEditComponent implements OnInit {

  customerId: Optional<CustomerId>;
  readonly model: Model;
  readonly form: Form;

  @ViewChild(AffiliationListComponent)
  private affiliationList: AffiliationListComponent;
  @ViewChild(AddressListEditComponent)
  private addressList: AddressListEditComponent;

  constructor(
    private service: CustomerService,
    private route: ActivatedRoute,
    private router: Router) {
      this.form = new Form();
      this.model = new Model();
  }

  ngOnInit() {
    ifDefinedDo(this.customerId = this.route.snapshot.params['id'],
      id => this.loadCustomer(id),
      _ => this.prepareNewCustomer());
  }

  submitForm() {
    this.service.updateCustomer({
      ...this.form.getData(),
      addresses: this.addressList.getData(),
      affiliations: this.affiliationList.getData().map(a => a.folderId)
    } as CustomerState,
    this.customerId);
  }

  canBeSubmitted(): boolean {
    return this.form.isValid() && this.model.addressListIsValid;
  }

  cancelEdition() {
    this.router.navigate(['/customers']);
  }

  onAffiliationSelected(id: FolderId): void {
    this.affiliationList.addData(new Affiliation(id));
  }

  onPersonSelected(person: Person): void {
    this.form.setPerson(this.model.person = person);
  }

  onAddressSelected(address: Address): void {
    this.addressList.addData(address);
  }

  onAddressAdded(address: Address): void {
    this.model.addressListIsValid = this.addressList.isValid();
  }

  onAddressRemoved(address: Address): void {
    this.model.addressListIsValid = !this.addressList.isEmpty();
  }

  togglePersonSearch(): void {
    this.model.togglePersonSearchOn = !this.model.togglePersonSearchOn;
  }

  toggleAddressSearch(): void {
    this.model.toggleAddressSearchOn = !this.model.toggleAddressSearchOn;
  }

  togglePersonAdvancedEditing(): void {
    this.model.personAdvancedEditingOn = !this.model.personAdvancedEditingOn;
  }

  toggleAddressAdvancedEditing(): void {
    this.addressList.toggleAdvancedEditing();
  }

  private loadCustomer(id: CustomerId) {
    this.service.loadCustomer$(id).subscribe(data => this.onCustomerLoaded(data));
  }

  private onCustomerLoaded(data: CustomerState): void {
    this.form.setData(data);
    this.addressList.setData(data.addresses ? data.addresses : []);
    this.model.person = Person.from(data.person);
    this.model.personName = this.model.person.name();
    this.model.addressListIsValid = this.addressList.isValid();
    this.model.togglePersonSearchOn = false;
  }

  private prepareNewCustomer() {
    this.model.togglePersonSearchOn = true;
    this.model.personTabExpanded = true;
    this.model.addressTabExpanded = false;
    this.service.createdCustomerOnce$().subscribe(data => this.onCustomerCreated(data));
  }

  private onCustomerCreated(data: Customer): void {
    this.customerId = data.id;
    this.model.togglePersonSearchOn = false;
    this.model.person = data.person;
  }
}

class Form {
  readonly fgPerson: FormGroup;
  readonly fgProfile: FormGroup;

  constructor() {
    this.fgPerson = new FormGroup({
      lastName: new FormControl('', {validators: Validators.required}),
      firstName: new FormControl(''),
      salutation: new FormControl(''),
      birthDate: new FormControl(''),
    });

    this.fgProfile = new FormGroup({
      hasMoved: new FormControl(''),
      isSubcontractor: new FormControl(''),
      sendInvitation: new FormControl(''),
      notes: new FormControl(''),
    });
  }

  isValid(): boolean {
    return this.fgPerson.valid && this.fgProfile.valid;
  }

  setData(data: CustomerState): void {
    this.fgPerson.patchValue(data.person);
    this.fgProfile.patchValue({...data.profile, notes: data.notes});
  }

  setPerson(person: Person): void {
    this.fgPerson.patchValue(person);
  }

  getData(): object {
    return {
      person: {
        ...this.fgPerson.value,
        birthDate: IsoDate.toString(this.fgPerson.value.birthDate)
      },
      profile: {...this.fgProfile.value},
      notes: this.fgProfile.value.notes
    };
  }

  resetNotes(): void {
    this.fgProfile.controls['notes'].setValue('');
  }
}

class Model {
  person: Optional<Person>;
  personName: string;
  personAdvancedEditingOn = false;
  togglePersonSearchOn = true;
  toggleAddressSearchOn = true;
  personTabExpanded = false;
  addressTabExpanded = false;
  addressListIsValid = false;
}
