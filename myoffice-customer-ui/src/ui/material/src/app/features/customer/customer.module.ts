import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ListModule} from 'shared/component/list/list.module';
import {MaterialComponentsModule} from 'shared/component/material/material-components.module';
import {AffiliationListComponent} from './component/affiliation-list/affiliation-list.component';
import {CustomerEditComponent} from './component/customer-edit/customer-edit.component';
import {CustomerListComponent} from './component/customer-list/customer-list.component';
import {CustomerShowComponent} from './component/customer-show/customer-show.component';
import {FolderSelectorComponent} from './component/folder-selector/folder-selector.component';
import {CustomerRoutingModule} from './customer-routing.module';
import {PersonComponentsModule} from 'shared/component/person/person-components.module';
import {AddressComponentsModule} from 'shared/component/address/address-components.module';
import { AffiliationListItemComponent } from './component/affiliation-list-item/affiliation-list-item.component';
import { CustomerService } from './service/customer.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialComponentsModule,
    CustomerRoutingModule,
    ListModule,
    PersonComponentsModule,
    AddressComponentsModule,
  ],
  declarations: [
    AffiliationListComponent,
    FolderSelectorComponent,
    CustomerListComponent,
    CustomerShowComponent,
    CustomerEditComponent,
    AffiliationListItemComponent,
  ],
  exports: [
    AffiliationListComponent,
    FolderSelectorComponent,
    CustomerListComponent,
    CustomerShowComponent,
    CustomerEditComponent,
  ],
  entryComponents: [
    CustomerShowComponent,
  ],
  providers: [
    CustomerService
  ],
})
export class CustomerModule {
  constructor(customerService: CustomerService) {
    customerService.initialize();
  }
}
