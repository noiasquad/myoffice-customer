import { isDefined } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { Actions, ofActionDispatched, Store } from '@ngxs/store';
import { LoadAffiliations, LoadCustomer, RemoveCustomer, UpdateCustomer } from 'core/features/command/customer-commands';
import { LoadFolder } from 'core/features/command/folder-commands';
import { CustomerCreated } from 'core/features/event/customer-events';
import { CustomerListState } from 'core/features/state/customer-list-state';
import { FolderListState } from 'core/features/state/folder-list-state';
import { Observable } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { Customer, CustomerId, CustomerState } from 'shared/model/customer/customer';
import { Affiliation } from 'shared/model/folder/affiliate';
import { FolderId } from 'shared/model/folder/folder';

@Injectable()
export class CustomerService {

  constructor(private store: Store, private actions$: Actions) { }

  initialize(): void {
  }

  loadCustomer$(id: CustomerId): Observable<Customer> {
    this.store.dispatch(new LoadCustomer(id));
    return this.customerOnce$(id);
  }

  loadAffiliation$(id: FolderId): Observable<Affiliation> {
    this.store.dispatch(new LoadFolder(id));
    return this.affiliationOnce$(id);
  }

  loadAffiliations$(id: CustomerId): Observable<Affiliation[]> {
    this.store.dispatch(new LoadAffiliations(id));
    return this.affiliationsOnce$(id);
  }

  updateCustomer(state: CustomerState, id?: CustomerId, folderIds?: FolderId[]): void {
    this.store.dispatch(new UpdateCustomer(state, id, folderIds));
  }

  removeCustomer(id: CustomerId): void {
    this.store.dispatch(new RemoveCustomer(id));
  }

  customerOnce$(id: CustomerId): Observable<Customer> {
    return this.store.select(CustomerListState.byId).pipe(
      map(byId => byId(id)),
      filter(isDefined),
      take(1));
  }

  createdCustomerOnce$(): Observable<Customer> {
    return this.actions$.pipe(
      ofActionDispatched(CustomerCreated),
      map(event => <Customer>{...event.state, id: event.id}),
      take(1));
  }

  affiliationOnce$(id: FolderId): Observable<Affiliation> {
    return this.store.select(FolderListState.byId).pipe(
      map(fn => fn(id)),
      filter(isDefined),
      map(folder => new Affiliation(folder.id, folder.name)),
      take(1));
  }

  affiliationsOnce$(id: CustomerId): Observable<Affiliation[]> {
    return this.store.select(FolderListState.byCustomerId).pipe(
      map(fn => fn(id)),
      map(folders => folders.map(folder => new Affiliation(folder.id, folder.name))),
      take(1));
  }
}
