import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffiliationListItemComponent } from './affiliation-list-item.component';

describe('AffiliationListItemComponent', () => {
  let component: AffiliationListItemComponent;
  let fixture: ComponentFixture<AffiliationListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffiliationListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffiliationListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
