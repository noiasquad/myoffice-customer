import {AfterViewInit, Component, OnInit, Type, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Select, Store} from '@ngxs/store';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {ListDataSource} from 'shared/component/list/mat-list/mat-list-datasource';
import {MatListComponent} from 'shared/component/list/mat-list/mat-list.component';
import {SData} from 'shared/model/common/data';
import {FilterCombination, SingleFilter} from 'shared/model/common/filter';
import {DEFAULT_PAGE, PageInfo, PageSpec} from 'shared/model/common/pageable';
import {Term} from 'shared/model/common/search';
import {ascOf, SingleSort, SortCombination} from 'shared/model/common/sort';
import {Customer, CustomerId} from 'shared/model/customer/customer';
import {Operation} from 'shared/model/common/resource';
import {CustomerShowComponent} from '../customer-show/customer-show.component';
import { isDefined } from '@angular/compiler/src/util';
import { CustomerListState } from 'core/features/state/customer-list-state';
import { FindSomeCustomers, SearchCustomers } from 'core/features/command/customer-commands';

interface Model {
  data: CustomerId;
  firstName: string;
  lastName: string;
}

@Component({
  selector: 'app-customer-list',
  template: `
    <app-list [columnNames]="['lastName', 'firstName']"
              [itemDataComponentTypeFn]="dataComponentTypeFn"
              [datasource]="datasource"
              [pageInfo$]="pageInfo$"
              [operations$]="operations$"
              (refresh)="refreshCustomerList($event)"
              (page)="goToPageOfCustomerList($event)"
              (filter)="filterCustomerList($event)"
              (sort)="sortCustomerList($event)"
              (search)="searchCustomers($event)"
              (create)="navigateToCustomerEdition()"
    >
    </app-list>`,
})
export class CustomerListComponent implements OnInit, AfterViewInit {

  @ViewChild(MatListComponent)
  private listComponent: MatListComponent<Model>;
  private _pageInfo$: Observable<PageInfo>;
  @Select(CustomerListState.operations)
  private _operations$: Observable<Operation[]>;

  get operations$(): Observable<Operation[]> {
    return this._operations$;
  }

  constructor(private store: Store, private router: Router) {
  }

  private _datasource: ListDataSource<Model>;

  get datasource(): ListDataSource<Model> {
    return this._datasource;
  }

  ngOnInit(): void {
    this.initializePageInfoStream();
    this.initializeDataSource();
  }

  dataComponentTypeFn(data: CustomerId): Type<any> {
    return CustomerShowComponent;
  }

  private initializePageInfoStream(): void {
    this._pageInfo$ = this.store.select(CustomerListState.page).pipe(filter(isDefined));
  }

  get pageInfo$(): Observable<PageInfo> {
    return this._pageInfo$;
  }

  private initializeDataSource(): void {
    this._datasource = new ListDataSource<Model>(
      this.store.select(CustomerListState.data).pipe(filter(isDefined), map(Mapper.toModel)));
  }

  sortCustomerList(event: [PageSpec, FilterCombination, SortCombination]): void {
    this.store.dispatch(new FindSomeCustomers(Mapper.toCustomerFilter(event[1]), event[0], Mapper.toCustomerSort(event[2])));
  }

  filterCustomerList(event: [PageSpec, FilterCombination, SortCombination]): void {
    this.store.dispatch(new FindSomeCustomers(Mapper.toCustomerFilter(event[1]), event[0], Mapper.toCustomerSort(event[2])));
  }

  ngAfterViewInit(): void {
    this.listComponent.sort('lastName', 'asc');
  }

  refreshCustomerList(event?: PageSpec): void {
    this.sortCustomerList([event ? event : DEFAULT_PAGE, [], [ascOf('lastName')]]);
  }

  searchCustomers(event: [Term, PageSpec, SortCombination]): void {
    this.store.dispatch(new SearchCustomers(event[0], event[1], Mapper.toCustomerSort(event[2])));
  }

  goToPageOfCustomerList(event: [PageSpec, FilterCombination, SortCombination]): void {
    this.store.dispatch(new FindSomeCustomers(Mapper.toCustomerFilter(event[1]), event[0], Mapper.toCustomerSort(event[2])));
  }

  navigateToCustomerEdition(): void {
    this.router.navigate(['/customers/edit']).catch(e => console.log(e));
  }
}

class Mapper {
  static toModel(data: SData<Customer>[]): Model[] {
    return data.map(customer => ({
      data: customer.id,
      firstName: customer.person.firstName,
      lastName: customer.person.lastName
    }) as Model);
  }

  static toCustomerFilter(fc: FilterCombination): FilterCombination {
    return fc.map(f => ({...f, attribute: `person.${f.attribute}`}) as SingleFilter);
  }

  static toCustomerSort(sc: SortCombination): SortCombination {
    return sc.map(sort => ({...sort, attribute: `person.${sort.attribute}`}) as SingleSort);
  }
}
