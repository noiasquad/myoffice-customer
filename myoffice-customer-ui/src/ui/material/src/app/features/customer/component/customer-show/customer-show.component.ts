import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from 'customer/service/customer.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SData } from 'shared/model/common/data';
import { Customer, CustomerId, CustomerState } from 'shared/model/customer/customer';
import { CustomerResource } from 'shared/model/customer/customer-resource';
import { Optional } from 'shared/util/functional';
import { Holder } from 'shared/util/holder';

type Model = CustomerState & {
  canBeModified: boolean;
  canBeRemoved: boolean;
};

@Component({
  selector: 'app-customer-show',
  templateUrl: './customer-show.component.html',
  styleUrls: ['./customer-show.component.css']
})
export class CustomerShowComponent implements Holder<CustomerId> {

  id: CustomerId;
  model$: Optional<Observable<Model>>;

  constructor(private service: CustomerService, private router: Router) {
  }

  set data(id: CustomerId) {
    this.loadCustomer(this.id = id);
  }

  modifyCustomer(): void {
    this.router.navigate(['customers/edit', this.id]);
  }

  removeCustomer(): void {
    this.service.removeCustomer(this.id);
  }

  private loadCustomer(id: CustomerId): void {
    this.model$ = this.service.loadCustomer$(id).pipe(map(Mapper.toModel));
  }
}

class Mapper {
  static toModel(data: SData<Customer>): Model {
    return {
      ...CustomerState.from(data),
      canBeModified: CustomerResource.canBeModified(data),
      canBeRemoved: CustomerResource.canBeRemoved(data)
    } as Model;
  }
}
