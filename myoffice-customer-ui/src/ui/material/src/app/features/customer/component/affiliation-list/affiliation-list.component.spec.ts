import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AffiliationListComponent } from './affiliation-list.component';

describe('AffiliateListComponent', () => {
  let component: AffiliationListComponent;
  let fixture: ComponentFixture<AffiliationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AffiliationListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffiliationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
