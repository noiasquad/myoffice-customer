import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CustomerService } from 'customer/service/customer.service';
import { BaseListComponent } from 'shared/component/list/base-list/base-list.component';
import { CustomerId } from 'shared/model/customer/customer';
import { Affiliation } from 'shared/model/folder/affiliate';
import { FolderId } from 'shared/model/folder/folder';
import { ifDefinedDo, ifDefinedMapElseDefault, Optional } from 'shared/util/functional';
import { Holder } from 'shared/util/holder';
import { Identity } from 'shared/util/identity';
import { AffiliationListItemComponent } from '../affiliation-list-item/affiliation-list-item.component';

type Model = Holder<Affiliation> & Identity<FolderId>;

@Component({
  selector: 'app-affiliation-list',
  templateUrl: './affiliation-list.component.html',
  styleUrls: ['./affiliation-list.component.css']
})
export class AffiliationListComponent
  extends BaseListComponent<Affiliation, FolderId, AffiliationListItemComponent>
  implements OnChanges {

  @Input() customerId: Optional<CustomerId>;
  @Input() readOnly = true;

  constructor(private service: CustomerService) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    ifDefinedDo(changes.customerId, change =>
      ifDefinedDo(change.currentValue, id =>
        this.service.loadAffiliations$(id).subscribe(as => this.setData(as))));
  }

  addData(data: Affiliation): void {
    ifDefinedDo(data.name,
      _ => super.addData(data),
      _ => this.service.loadAffiliation$(data.folderId).subscribe(a => super.addData(a)));
  }

  sort(): void {
    this.items.sort(ItemComparator.compare);
  }

  toModel(data: Affiliation): Model {
    return {
      data: data,
      id: data.folderId
    };
  }
}

class ItemComparator {
  static compare(a: Model, b: Model): number {
    return ifDefinedMapElseDefault(a.data.name, n => n, () => '').localeCompare(
      ifDefinedMapElseDefault(b.data.name, n => n, () => ''));
  }
}
