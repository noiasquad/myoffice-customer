import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Affiliate, Affiliation} from 'shared/model/folder/affiliate';
import {CustomerId} from 'shared/model/customer/customer';
import {FolderId, FolderState} from 'shared/model/folder/folder';
import {BaseCrudService} from 'shared/repository/crud.service';
import {HttpFindService} from 'shared/repository/http/http-find.service';
import {HttpUpdateService} from 'shared/repository/http/http-update.service';
import { CData } from 'shared/model/common/data';

const ENDPOINT_URL = '/api/customer/v1/folders';
const CUSTOMER_ENDPOINT_URL = '/api/customer/v1/customers';

@Injectable()
export class FolderRepository extends BaseCrudService<FolderId, FolderState> {

  constructor(protected http: HttpClient) {
    super(new HttpFindService(http, ENDPOINT_URL), new HttpUpdateService(http, ENDPOINT_URL));
  }

  findAffiliates$(id: FolderId): Observable<CData<Affiliate>> {
    return this.http.get<CData<Affiliate>>(`${ENDPOINT_URL}/${id}/affiliates`);
  }

  putAffiliate$(id: FolderId, affiliate: Affiliate): Observable<Affiliation> {
    return this.http.put<Affiliation>(`${ENDPOINT_URL}/${id}/affiliates`, affiliate);
  }

  removeAffiliate$(id: FolderId, affiliate: CustomerId): Observable<any> {
    return this.http.delete<any>(`${ENDPOINT_URL}/${id}/affiliates/${affiliate}`);
  }

  findAffiliations$(id: CustomerId): Observable<CData<Affiliation>> {
    return this.http.get<CData<Affiliation>>(`${CUSTOMER_ENDPOINT_URL}/${id}/affiliations`);
  }
}
