import {Injectable} from '@angular/core';
import {PostalAddress} from 'shared/model/address/address';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {HttpQueryBuilder} from 'shared/repository/http/http-query';

const ENDPOINT_URL = '/api/address/v1';

@Injectable()
export class PostalAddressClient {

  constructor(private http: HttpClient) {
  }

  public verifyOccupation$(address: PostalAddress, personName: string): Observable<PostalAddress[]> {
    return this.http.get<PostalAddress[]>(`${ENDPOINT_URL}/addresses/occupied`, {
      params: new HttpQueryBuilder()
        .param('street', address.street)
        .param('houseNbr', address.houseNbr)
        .param('zip', address.zip)
        .param('city', address.city)
        .param('personName', personName)
        .build()
    });
  }

  public findStreets$(address: PostalAddress, limit?: number): Observable<PostalAddress[]> {
    return this.http.get<PostalAddress[]>(`${ENDPOINT_URL}/streets/search`, {
      params: new HttpQueryBuilder()
        .param('street', address.street)
        .param('zip', address.zip)
        .param('city', address.city)
        .param('limit', limit)
        .build()
    });
  }

  public findAddresses$(address: PostalAddress, limit?: number): Observable<PostalAddress[]> {
    return this.http.get<PostalAddress[]>(`${ENDPOINT_URL}/addresses/search`, {
      params: new HttpQueryBuilder()
        .param('street', address.street)
        .param('houseNbr', address.houseNbr)
        .param('zip', address.zip)
        .param('city', address.city)
        .param('limit', limit)
        .build()
    });
  }
}
