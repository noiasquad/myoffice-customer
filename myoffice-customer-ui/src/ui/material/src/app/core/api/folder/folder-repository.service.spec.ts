import {inject, TestBed} from '@angular/core/testing';

import {FolderRepository} from './folder-repository.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('FolderRepository', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [FolderRepository]
    });
  });

  it('should be created', inject([FolderRepository], (service: FolderRepository) => {
    expect(service).toBeTruthy();
  }));
});
