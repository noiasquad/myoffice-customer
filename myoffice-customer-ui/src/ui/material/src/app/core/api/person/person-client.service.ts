import {Injectable} from '@angular/core';
import {Gender, Person, Salutation, salutationByGender} from 'shared/model/person/person';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Optional} from 'shared/util/functional';

const GENDER_API_ENDPOINT_URL = '/api/person/v1/gender';

@Injectable()
export class PersonClient {

  constructor(private http: HttpClient) {
  }

  public identify(fullName: string): Observable<Person> {
    return this.http.get<Person>(`${GENDER_API_ENDPOINT_URL}?fullName=${fullName}`).pipe(
      map(person => <Person>{...person, salutation: toSalutation(person.gender)}));
  }
}

function toSalutation(gender?: Gender): Optional<Salutation> {
  return gender && salutationByGender[gender];
}
