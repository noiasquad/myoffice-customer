import {inject, TestBed} from '@angular/core/testing';

import {CustomerRepository} from './customer-repository.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('CustomerRepository', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CustomerRepository]
    });
  });

  it('should be created', inject([CustomerRepository], (service: CustomerRepository) => {
    expect(service).toBeTruthy();
  }));
});
