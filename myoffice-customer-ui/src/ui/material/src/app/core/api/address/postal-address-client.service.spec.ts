import {TestBed} from '@angular/core/testing';

import {PostalAddressClient} from './postal-address-client.service';

describe('PostalAddressService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostalAddressClient = TestBed.get(PostalAddressClient);
    expect(service).toBeTruthy();
  });
});
