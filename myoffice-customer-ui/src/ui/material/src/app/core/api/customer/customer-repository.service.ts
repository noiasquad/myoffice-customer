import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CustomerId, CustomerState } from 'shared/model/customer/customer';
import { BaseCrudService } from 'shared/repository/crud.service';
import { HttpFindService } from 'shared/repository/http/http-find.service';
import { HttpUpdateService } from 'shared/repository/http/http-update.service';

const ENDPOINT_URL = '/api/customer/v1/customers';

@Injectable()
export class CustomerRepository extends BaseCrudService<CustomerId, CustomerState> {

  constructor(protected http: HttpClient) {
    super(new HttpFindService(http, ENDPOINT_URL), new HttpUpdateService(http, ENDPOINT_URL));
  }
}

