import {TestBed} from '@angular/core/testing';

import {PersonClient} from './person.service';

describe('PersonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PersonClient = TestBed.get(PersonClient);
    expect(service).toBeTruthy();
  });
});
