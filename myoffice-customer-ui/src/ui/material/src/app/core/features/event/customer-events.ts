import {CustomerId, Customer, CustomerState} from 'shared/model/customer/customer';

export class CustomerLoaded {
  static readonly type = 'Event.CustomerLoaded';
  constructor(public readonly customer: Customer) {
  }
}

export class CustomerCreated {
  static readonly type = 'Event.CustomerCreated';
  constructor(public readonly state: CustomerState, readonly id: CustomerId) {
  }
}

export class CustomerModified {
  static readonly type = 'Event.CustomerModified';
  constructor(public readonly state: CustomerState, readonly id: CustomerId) {
  }
}

export class CustomerRemoved {
  static readonly type = 'Event.CustomerRemoved';
  constructor(public readonly id: CustomerId) {
  }
}
