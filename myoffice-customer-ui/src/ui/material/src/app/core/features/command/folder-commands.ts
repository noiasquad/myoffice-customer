import {CustomerId} from 'shared/model/customer/customer';
import {FilterCombination} from 'shared/model/common/filter';
import {PageSpec} from 'shared/model/common/pageable';
import {Term} from 'shared/model/common/search';
import {SortCombination} from 'shared/model/common/sort';
import {FolderId, FolderState} from 'shared/model/folder/folder';

export class FindAllFolders {
  static readonly type = 'Command.FindAllFolders';
  constructor(
    public page?: PageSpec,
    public sorts?: SortCombination) {
  }
}

export class FindSomeFolders {
  static readonly type = 'Command.FindSomeFolders';
  constructor(
    public filters?: FilterCombination,
    public page?: PageSpec,
    public sorts?: SortCombination) {
  }
}

export class SearchFolders {
  static readonly type = 'Command.SearchFolders';
  constructor(
    public term: Term,
    public page?: PageSpec,
    public sorts?: SortCombination) {
  }
}

export class LoadFolder {
  static readonly type = 'Command.LoadFolder';
  constructor(public readonly id: FolderId) {
  }
}

export class LoadFolders {
  static readonly type = 'Command.LoadFolders';
  constructor(public readonly ids: FolderId[]) {
  }
}

export class UpdateFolder {
  static readonly type = 'Command.UpdateFolder';
  constructor(public readonly state: FolderState, public readonly id?: FolderId) {
  }
}

export class RemoveFolder {
  static readonly type = 'Command.RemoveFolder';
  constructor(public readonly id: FolderId) {
  }
}

export class CreateAffiliation {
  static readonly type = 'Command.CreateAffiliation';
  constructor(public readonly id: FolderId, readonly affiliate: CustomerId) {
  }
}

export class CancelAffiliation {
  static readonly type = 'Command.CancelAffiliation';
  constructor(public readonly id: FolderId, public readonly customerId: CustomerId) {
  }
}

export class PromoteAffiliate {
  static readonly type = 'Command.PromoteAffiliate';
  constructor(public readonly id: FolderId, public readonly customerId: CustomerId) {
  }
}
