import { FilterCombination } from 'shared/model/common/filter';
import { PageSpec } from 'shared/model/common/pageable';
import { Term } from 'shared/model/common/search';
import { SortCombination } from 'shared/model/common/sort';
import { CustomerId, CustomerState } from 'shared/model/customer/customer';

export class FindAllCustomers {
  static readonly type = 'Command.FindAllCustomers';
  constructor(
    public readonly page?: PageSpec,
    public readonly sorts?: SortCombination) {
  }
}

export class FindSomeCustomers {
  static readonly type = 'Command.FindSomeCustomers';
  constructor(
    public readonly filters?: FilterCombination,
    public readonly page?: PageSpec,
    public readonly sorts?: SortCombination) {
  }
}

export class SearchCustomers {
  static readonly type = 'Command.SearchCustomers';
  constructor(
    public readonly term: Term,
    public readonly page?: PageSpec,
    public readonly sorts?: SortCombination) {
  }
}

export class LoadCustomer {
  static readonly type = 'Command.LoadCustomer';
  constructor(public readonly id: CustomerId) {
  }
}

export class LoadAffiliations {
  static readonly type = 'Command.LoadAffiliations';
  constructor(public readonly id: CustomerId) {
  }
}

export class LoadCustomers {
  static readonly type = 'Command.LoadCustomers';
  constructor(public readonly ids: CustomerId[]) {
  }
}

export class UpdateCustomer {
  static readonly type = 'Command.UpdateCustomer';
  constructor(
    public readonly state: CustomerState,
    public readonly id?: CustomerId) {
  }
}

export class RemoveCustomer {
  static readonly type = 'Command.RemoveCustomer';
  constructor(public readonly id: CustomerId) {
  }
}
