import { Folder, FolderId, FolderState } from 'shared/model/folder/folder';

export class FolderLoaded {
  static readonly type = 'Event.FolderLoaded';
  constructor(public readonly folder: Folder) {
  }
}

export class FolderCreated {
  static readonly type = 'Event.FolderCreated';
  constructor(public readonly state: FolderState, readonly id: FolderId) {
  }
}

export class FolderModified {
  static readonly type = 'Event.FolderModified';
  constructor(public readonly state: FolderState, readonly id: FolderId) {
  }
}

export class FolderRemoved {
  static readonly type = 'Event.FolderRemoved';
  constructor(public readonly id: FolderId) {
  }
}
