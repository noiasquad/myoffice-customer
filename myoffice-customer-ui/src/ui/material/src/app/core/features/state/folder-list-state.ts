import { Action, Selector, State, StateContext } from '@ngxs/store';
import { FolderRepository } from 'core/api/folder/folder-repository.service';
import { CancelAffiliation, FindAllFolders, FindSomeFolders, LoadFolder,
  LoadFolders, PromoteAffiliate, RemoveFolder, SearchFolders, UpdateFolder } from 'core/features/command/folder-commands';
import { FolderCreated, FolderLoaded, FolderModified, FolderRemoved } from 'core/features/event/folder-events';
import { of } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';
import { OperationPerformed } from 'shared/event/events';
import { CData, PData, resourceLinks, SData, embedded } from 'shared/model/common/data';
import { EMPTY_PAGE, PageInfo } from 'shared/model/common/pageable';
import { CustomerId } from 'shared/model/customer/customer';
import { Affiliation } from 'shared/model/folder/affiliate';
import { Folder, FolderId } from 'shared/model/folder/folder';
import { FolderResource } from 'shared/model/folder/folder-resource';
import { ifDefinedDo, ifDefinedMapElseDefault } from 'shared/util/functional';
import { LoadAffiliations } from '../command/customer-commands';

@State<PData<Folder>>({
  name: 'folderList',
  defaults: {
    content: [],
    ...EMPTY_PAGE
  }
})
export class FolderListState {

  constructor(private repository: FolderRepository) {
  }

  @Selector()
  static data(state: PData<Folder>) {
    return state.content;
  }

  @Selector()
  static page(state: PData<Folder>) {
    return state as PageInfo;
  }

  @Selector()
  static links(state: PData<Folder>) {
    return resourceLinks(state);
  }

  @Selector()
  static operations(state: PData<Folder>) {
    return Object.keys(resourceLinks(state));
  }

  @Selector()
  static byId(state: PData<Folder>) {
    return (id: FolderId) => (state.content as Folder[]).find(folder => folder.id === id);
  }

  @Selector()
  static byIds(state: PData<Folder>) {
    return (ids: FolderId[]) => (state.content as Folder[])
      .filter(folder => ids.some(it => it === folder.id));
  }

  @Selector()
  static byCustomerId(state: PData<Folder>) {
    return (id: CustomerId) => (state.content as Folder[])
      .filter(folder => folder.affiliates && folder.affiliates.some(it => it === id));
  }

  @Action(FindAllFolders)
  public findAll(ctx: StateContext<PData<Folder>>, action: FindAllFolders) {
    return this.repository.findAll$(action.page, action.sorts).pipe(
      tap((result: PData<Folder>) => ctx.setState(result)));
  }

  @Action(FindSomeFolders)
  public findSome(ctx: StateContext<PData<Folder>>, action: FindSomeFolders) {
    return this.repository.findSome$(action.filters, action.page, action.sorts).pipe(
      tap((result: PData<Folder>) => ctx.setState(result)));
  }

  @Action(SearchFolders)
  public search(ctx: StateContext<PData<Folder>>, action: SearchFolders) {
    return this.repository.search$(action.term, action.page, action.sorts).pipe(
      tap((result: PData<Folder>) => ctx.setState(result)));
  }

  @Action(LoadFolder)
  public loadOne(ctx: StateContext<PData<Folder>>, { id }: LoadFolder) {
    const content = ctx.getState().content as SData<Folder>[];
    return ifDefinedMapElseDefault(
      content.find(it => it.id === id),
      folder => of(folder),
      () => this.repository.findOne$(id).pipe(
        tap((result: SData<Folder>) => ctx.patchState({ content: content.concat(result) })))
    ).pipe(mergeMap(folder => ctx.dispatch(new FolderLoaded(folder))));
  }

  @Action(LoadFolders)
  public loadOnes(ctx: StateContext<PData<Folder>>, { ids }: LoadFolders) {
    const content = ctx.getState().content as SData<Folder>[];
    const notLoadedIds = ids.filter(it => content.every(folder => folder.id !== it));
    return notLoadedIds.length > 0 ?
      this.repository.findOnes$(notLoadedIds).pipe(
        tap((result: CData<Folder>) => ctx.patchState({ content: content.concat(embedded(result)) }))) : of([]);
  }

  @Action(LoadAffiliations)
  public loadAffiliations(ctx: StateContext<PData<Folder>>, { id }: LoadAffiliations) {
    const content = ctx.getState().content as SData<Folder>[];
    return this.repository.findAffiliations$(id).pipe(
      map((result: CData<Affiliation>) => embedded(result).map(a => a.folderId)),
      map(ids => ids.filter(it => content.every(folder => folder.id !== it))),
      mergeMap(notLoadedIds => notLoadedIds.length > 0 ?
        this.repository.findOnes$(notLoadedIds).pipe(
          tap((result: CData<Folder>) => ctx.patchState({ content: content.concat(embedded(result)) }))) : of([])));
  }

  @Action(UpdateFolder)
  public update(ctx: StateContext<PData<Folder>>, { state, id }: UpdateFolder) {
    const content = ctx.getState().content as SData<Folder>[];
    return this.repository.put$(state, id).pipe(
      tap((result: SData<Folder>) => ctx.patchState({ content: content.filter(it => it.id !== id).concat(result) })),
      map(result => id ?
        new FolderModified(result, id) :
        new FolderCreated(result, FolderResource.selfId(result))),
      mergeMap(event => ctx.dispatch([event, OperationPerformed.of(UpdateFolder, event)])));
  }

  @Action(RemoveFolder)
  public remove(ctx: StateContext<PData<Folder>>, { id }: RemoveFolder) {
    const content = ctx.getState().content as SData<Folder>[];
    return this.repository.remove$(id).pipe(
      tap(_ => ctx.patchState({ content: content.filter(it => it.id !== id) })),
      map(_ => new FolderRemoved(id)),
      mergeMap(event => ctx.dispatch([event, OperationPerformed.of(RemoveFolder, event)])));
  }

  @Action(PromoteAffiliate)
  public promote(ctx: StateContext<PData<Folder>>, { id, customerId }: PromoteAffiliate) {
    const content = ctx.getState().content as SData<Folder>[];
    ifDefinedDo(content.find(it => it.id === id), folder => {
      ctx.patchState({
        content: content.filter(it => it.id !== id).concat({
          ...folder,
          primaryAffiliate: customerId
        })
      });
    });
  }

  @Action(CancelAffiliation)
  public cancelAffiliation(ctx: StateContext<PData<Folder>>, { id, customerId }: CancelAffiliation) {
    const content = ctx.getState().content as SData<Folder>[];
    ifDefinedDo(content.find(it => it.id === id), folder => {
      ctx.patchState({
        content: content.filter(it => it.id !== id).concat({
          ...folder,
          affiliates: ifDefinedMapElseDefault(folder.affiliates, as => as.filter(it => it !== customerId), () => [])
        })
      });
    });
  }
}
