import { Action, Selector, State, StateContext } from '@ngxs/store';
import { CustomerRepository } from 'core/api/customer/customer-repository.service';
import { FindAllCustomers, FindSomeCustomers, LoadCustomer, LoadCustomers, RemoveCustomer,
  SearchCustomers, UpdateCustomer } from 'core/features/command/customer-commands';
import { CustomerCreated, CustomerLoaded, CustomerModified, CustomerRemoved } from 'core/features/event/customer-events';
import { of } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';
import { OperationPerformed } from 'shared/event/events';
import { CData, PData, resourceLinks, SData, embedded } from 'shared/model/common/data';
import { EMPTY_PAGE, PageInfo } from 'shared/model/common/pageable';
import { Customer, CustomerId } from 'shared/model/customer/customer';
import { CustomerResource } from 'shared/model/customer/customer-resource';
import { ifDefinedMapElseDefault } from 'shared/util/functional';
import { toJson } from 'shared/util/debug';

@State<PData<Customer>>({
  name: 'customerList',
  defaults: {
    content: [] as Customer[],
    ...EMPTY_PAGE
  }
})
export class CustomerListState {

  constructor(private repository: CustomerRepository) {
  }

  @Selector()
  static data(state: PData<Customer>) {
    return state.content;
  }

  @Selector()
  static page(state: PData<Customer>) {
    return state as PageInfo;
  }

  @Selector()
  static links(state: PData<Customer>) {
    return resourceLinks(state);
  }

  @Selector()
  static operations(state: PData<Customer>) {
    return Object.keys(resourceLinks(state));
  }

  @Selector()
  static byId(state: PData<Customer>) {
    return (id: CustomerId) => (state.content as Customer[]).find(customer => customer.id === id);
  }

  @Selector()
  static byIds(state: PData<Customer>) {
    return (ids: CustomerId[]) => (state.content as Customer[]).filter(customer => ids.some(it => it === customer.id));
  }

  @Action(FindAllCustomers)
  public findAll(ctx: StateContext<PData<Customer>>, { page, sorts }: FindAllCustomers) {
    return this.repository.findAll$(page, sorts).pipe(
      tap((result: PData<Customer>) => ctx.setState(result)));
  }

  @Action(FindSomeCustomers)
  public findSome(ctx: StateContext<PData<Customer>>, { filters, page, sorts }: FindSomeCustomers) {
    return this.repository.findSome$(filters, page, sorts).pipe(
      tap((result: PData<Customer>) => ctx.setState(result)));
  }

  @Action(SearchCustomers)
  public search(ctx: StateContext<PData<Customer>>, { term, page, sorts }: SearchCustomers) {
    return this.repository.search$(term, page, sorts).pipe(
      tap((result: PData<Customer>) => ctx.setState(result)));
  }

  @Action(LoadCustomer)
  public loadOne(ctx: StateContext<PData<Customer>>, { id }: LoadCustomer) {
    const content = ctx.getState().content as SData<Customer>[];
    return ifDefinedMapElseDefault(
      content.find(it => it.id === id),
      customer => of(customer).pipe(
        tap((result: SData<Customer>) =>
          ctx.dispatch(new CustomerLoaded(result)))),
      () => this.repository.findOne$(id).pipe(
        tap((result: SData<Customer>) => {
          ctx.patchState({ content: content.concat(result) });
          ctx.dispatch(new CustomerLoaded(result));
        })));
  }

  @Action(LoadCustomers)
  public loadOnes(ctx: StateContext<PData<Customer>>, { ids }: LoadCustomers) {
    const content = ctx.getState().content as SData<Customer>[];
    const notLoadedIds = ids.filter(it => content.every(customer => customer.id !== it));
    return notLoadedIds.length > 0 ?
      this.repository.findOnes$(notLoadedIds).pipe(
        tap((result: CData<Customer>) => ctx.patchState({ content: content.concat(embedded(result)) }))) : of([]);
  }

  @Action(UpdateCustomer)
  public update(ctx: StateContext<PData<Customer>>, { state, id }: UpdateCustomer) {
    const content = ctx.getState().content as SData<Customer>[];
    return this.repository.put$(state, id).pipe(
      tap((result: SData<Customer>) => ctx.patchState({ content: content.filter(it => it.id !== id).concat(result) })),
      map(result => id ?
        new CustomerModified(result, id) :
        new CustomerCreated(result, CustomerResource.selfId(result))),
      mergeMap(event => ctx.dispatch([event, OperationPerformed.of(UpdateCustomer, event)])));
  }

  @Action(RemoveCustomer)
  public remove(ctx: StateContext<PData<Customer>>, { id }: RemoveCustomer) {
    const content = ctx.getState().content as SData<Customer>[];
    return this.repository.remove$(id).pipe(
      tap(_ => ctx.patchState({ content: content.filter(it => it.id !== id) })),
      map(_ => new CustomerRemoved(id)),
      mergeMap(event => ctx.dispatch([event, OperationPerformed.of(CustomerRemoved, event)])));
  }
}
