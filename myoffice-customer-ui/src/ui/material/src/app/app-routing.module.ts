import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from 'core/auth/component/login/login.component';
import {AuthGuard} from 'core/auth/guard/auth-guard.service';

const routes: Routes = [
  {
    path: 'customers',
    loadChildren: './features/customer/customer.module#CustomerModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'folders',
    loadChildren: './features/folder/folder.module#FolderModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'login', component: LoginComponent
  },
  {path: '**', redirectTo: 'folders'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: false, useHash: false})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
