package edu.noia.myoffice.customer.domain.vo;

import edu.noia.myoffice.common.domain.vo.Identity;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;

import java.util.UUID;

@EqualsAndHashCode(of = "id", callSuper = false, doNotUseGetters = true)
@Value(staticConstructor = "of")
public final class CustomerId implements Identity {

    @NonNull
    final UUID id;

    public static CustomerId random() {
        return CustomerId.of(UUID.randomUUID());
    }
}
