package edu.noia.myoffice.customer.domain.vo;

import edu.noia.myoffice.customer.domain.entity.FolderState;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.HashSet;
import java.util.Set;

import static edu.noia.myoffice.common.util.exception.ExceptionSuppliers.notFound;

@Getter
@Setter
@EqualsAndHashCode(of = "name", callSuper = false, doNotUseGetters = true)
@Accessors(chain = true)
@RequiredArgsConstructor(staticName = "of")
@NoArgsConstructor
public final class FolderSample implements FolderState {

    @NonNull
    String name;
    String notes;
    Set<CustomerId> affiliates = new HashSet<>();
    CustomerId primaryAffiliate;

    public static FolderSample from(FolderState state) {
        return (FolderSample)FolderSample.of(state.getName()).modify(state);
    }

    @Override
    public FolderState addAffiliate(CustomerId affiliate) {
        affiliates.add(affiliate);
        return this;
    }

    public FolderSample removeAffiliate(CustomerId affiliate) {
        if (!getAffiliates().remove(affiliate)) {
            throw notFound(Affiliate.class, affiliate).get();
        }
        return this;
    }
}
