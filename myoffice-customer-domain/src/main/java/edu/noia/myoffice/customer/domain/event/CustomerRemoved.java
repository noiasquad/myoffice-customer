package edu.noia.myoffice.customer.domain.event;

import edu.noia.myoffice.common.domain.event.DomainEvent;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import lombok.NonNull;
import lombok.Value;

@Value(staticConstructor = "of")
public class CustomerRemoved implements DomainEvent {

    @NonNull
    CustomerId customerId;
}
