package edu.noia.myoffice.customer.domain.service;

import edu.noia.myoffice.common.domain.event.EventPublisher;
import edu.noia.myoffice.customer.domain.entity.Customer;
import edu.noia.myoffice.customer.domain.entity.Folder;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.event.FolderRemoved;
import edu.noia.myoffice.customer.domain.factory.FolderFactory;
import edu.noia.myoffice.customer.domain.repository.CustomerRepository;
import edu.noia.myoffice.customer.domain.repository.FolderRepository;
import edu.noia.myoffice.customer.domain.util.finder.CustomerFinder;
import edu.noia.myoffice.customer.domain.util.finder.FolderFinder;
import edu.noia.myoffice.customer.domain.vo.Affiliation;
import edu.noia.myoffice.customer.domain.vo.AffiliationId;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class FolderService {

    FolderRepository folderRepository;
    EventPublisher eventPublisher;
    FolderFactory folderFactory;
    FolderFinder folderFinder;
    CustomerFinder customerFinder;

    public FolderService(@NonNull CustomerRepository customerRepository,
                         @NonNull FolderRepository folderRepository,
                         @NonNull EventPublisher eventPublisher) {

        this.folderRepository = folderRepository;
        this.eventPublisher = eventPublisher;
        this.folderFactory = new FolderFactory();
        this.folderFinder = new FolderFinder(folderRepository);
        this.customerFinder = new CustomerFinder(customerRepository);
    }

    public Folder create(FolderState data) {
        return folderFactory.create(data)
                .publish(eventPublisher)
                .persist(folderRepository);
    }

    public Folder create(Customer customer) {
        return folderFactory.create(customer)
                .publish(eventPublisher)
                .persist(folderRepository);
    }

    public Folder modify(FolderId folderId, FolderState modifier) {
        return folderFinder.find(folderId)
                .modify(modifier)
                .publish(eventPublisher)
                .persist(folderRepository);
    }

    public Affiliation affiliate(FolderId folderId, CustomerId customerId) {
        customerFinder.check(customerId);
        folderFinder.find(folderId)
                .affiliate(customerId)
                .publish(eventPublisher)
                .persist(folderRepository);
        return Affiliation.of(folderId, customerId);
    }

    public Folder promote(FolderId folderId, CustomerId customerId) {
        customerFinder.check(customerId);
        return folderFinder.find(folderId)
                .promote(customerId)
                .publish(eventPublisher)
                .persist(folderRepository);
    }

    public void remove(FolderId folderId) {
        folderFinder.check(folderId);
        folderRepository.remove(folderId);
        eventPublisher.publish(FolderRemoved.of(folderId));
    }

    public Folder remove(AffiliationId affiliationId) {
        return folderFinder.find(affiliationId.getFolderId())
                .remove(affiliationId.getCustomerId())
                .publish(eventPublisher)
                .persist(folderRepository);
    }
}