package edu.noia.myoffice.customer.domain.entity;

import edu.noia.myoffice.customer.domain.vo.Profile;
import edu.noia.myoffice.address.domain.vo.Address;
import edu.noia.myoffice.person.domain.vo.Person;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

public interface CustomerState {

    @Valid
    Person getPerson();

    CustomerState setPerson(Person person);

    @Valid
    List<Address> getAddresses();

    CustomerState setAddresses(List<Address> addresses);

    @Valid
    Profile getProfile();

    CustomerState setProfile(Profile profile);

    String getNotes();

    CustomerState setNotes(String text);

    default CustomerState modify(CustomerState modifier) {
        return setPerson(modifier.getPerson())
                .setProfile(modifier.getProfile())
                .setNotes(modifier.getNotes())
                .setAddresses(modifier.getAddresses());
    }

}
