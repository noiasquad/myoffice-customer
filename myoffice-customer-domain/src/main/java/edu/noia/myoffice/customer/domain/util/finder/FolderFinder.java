package edu.noia.myoffice.customer.domain.util.finder;

import edu.noia.myoffice.common.util.finder.Finder;
import edu.noia.myoffice.customer.domain.entity.Folder;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.repository.FolderRepository;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FolderFinder {

    @NonNull
    FolderRepository repository;

    public Folder find(FolderId id) {
        return Finder.find(Folder.class, id, i -> repository.findOne(i).map(s -> Folder.ofValid(i, s)));
    }

    public void check(FolderId id) {
        Finder.find(FolderState.class, id, repository::findOne);
    }
}
