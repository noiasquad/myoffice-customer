package edu.noia.myoffice.customer.domain.repository;

import edu.noia.myoffice.customer.domain.vo.Affiliation;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import edu.noia.myoffice.common.domain.repository.DomainRepository;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import org.javatuples.Pair;

import java.util.stream.Stream;

public interface FolderRepository extends DomainRepository<FolderId, FolderState> {

    Stream<Affiliation> findAffiliationsById(CustomerId customerId);

    Stream<Pair<FolderId, FolderState>> findByCustomerId(CustomerId customerId);
}
