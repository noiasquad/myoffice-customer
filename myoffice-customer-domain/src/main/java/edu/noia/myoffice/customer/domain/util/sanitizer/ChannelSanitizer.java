package edu.noia.myoffice.customer.domain.util.sanitizer;

import edu.noia.myoffice.common.util.sanitizer.EmailAddressSanitizer;
import edu.noia.myoffice.common.util.sanitizer.Sanitizer;
import edu.noia.myoffice.customer.domain.vo.Channel;
import edu.noia.myoffice.customer.domain.vo.PhoneNumber;

public interface ChannelSanitizer {

    Sanitizer<PhoneNumber> phoneNumber = new PhoneNumberGoogleSanitizer();
    Sanitizer<String> email = new EmailAddressSanitizer();

    Sanitizer<Channel> phoneChannel = cha -> phoneNumber
            .sanitize(PhoneNumber.of(cha.getValue(), cha.getType()))
            .map(pn -> Channel.of(pn.getChannel(), pn.getNumber(), cha.getComment()));

    Sanitizer<Channel> emailChannel = cha -> email
            .sanitize(cha.getValue())
            .map(value -> Channel.of(cha.getType(), value, cha.getComment()));
}
