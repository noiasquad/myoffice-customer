package edu.noia.myoffice.customer.domain.vo;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@EqualsAndHashCode(of = {"customerId"}, doNotUseGetters = true, callSuper = false)
@RequiredArgsConstructor(staticName = "of")
public class Affiliate {

    @NotNull
    @NonNull
    CustomerId customerId;
}
