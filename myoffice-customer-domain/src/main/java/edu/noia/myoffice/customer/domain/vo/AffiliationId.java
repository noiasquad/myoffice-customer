package edu.noia.myoffice.customer.domain.vo;

import lombok.*;

@EqualsAndHashCode(of={"folderId", "customerId"}, callSuper = false, doNotUseGetters = true)
@Value(staticConstructor = "of")
public class AffiliationId {

    @NonNull
    FolderId folderId;
    @NonNull
    CustomerId customerId;
}
