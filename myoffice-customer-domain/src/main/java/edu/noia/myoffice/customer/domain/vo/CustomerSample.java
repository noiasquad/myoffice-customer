package edu.noia.myoffice.customer.domain.vo;

import edu.noia.myoffice.address.domain.vo.Address;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.person.domain.vo.Person;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(of = {"person"}, callSuper = false, doNotUseGetters = true)
@Accessors(chain = true)
@RequiredArgsConstructor(staticName = "of")
@NoArgsConstructor
public final class CustomerSample implements CustomerState {

    @NonNull
    Person person;
    List<Address> addresses = new ArrayList<>();
    Profile profile;
    String notes;

    public static CustomerSample from(CustomerState state) {
        return (CustomerSample)CustomerSample.of(state.getPerson()).modify(state);
    }
}
