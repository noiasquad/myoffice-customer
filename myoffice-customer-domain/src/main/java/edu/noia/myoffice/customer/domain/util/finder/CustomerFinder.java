package edu.noia.myoffice.customer.domain.util.finder;

import edu.noia.myoffice.common.util.finder.Finder;
import edu.noia.myoffice.customer.domain.entity.Customer;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.repository.CustomerRepository;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CustomerFinder {

    @NonNull
    CustomerRepository repository;

    public Customer find(CustomerId id) {
        return Finder.find(Customer.class, id, i -> repository.findOne(i).map(s -> Customer.ofValid(i, s)));
    }

    public void check(CustomerId id) {
        Finder.find(CustomerState.class, id, repository::findOne);
    }
}
