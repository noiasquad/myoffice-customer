package edu.noia.myoffice.customer.domain.vo;

import lombok.Value;

@Value(staticConstructor = "of")
public class PhoneNumber {

//    @Pattern(regexp = PHONE_INTERNATIONAL, message = "'${validatedValue}' does not follow " + PHONE_INTERNATIONAL)

    private String number;
    private Channel.Type channel;
}
