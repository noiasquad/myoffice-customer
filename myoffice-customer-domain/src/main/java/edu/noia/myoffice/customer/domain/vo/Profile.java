package edu.noia.myoffice.customer.domain.vo;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Getter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
public class Profile {

    Boolean hasMoved = false;
    Boolean isSubcontractor = false;
    Boolean sendInvitation = false;
}