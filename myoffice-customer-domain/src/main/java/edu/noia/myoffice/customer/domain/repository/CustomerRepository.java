package edu.noia.myoffice.customer.domain.repository;

import edu.noia.myoffice.common.domain.repository.DomainRepository;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;

public interface CustomerRepository extends DomainRepository<CustomerId, CustomerState> {
}

