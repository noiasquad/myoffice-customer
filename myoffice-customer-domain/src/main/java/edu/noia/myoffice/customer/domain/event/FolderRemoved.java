package edu.noia.myoffice.customer.domain.event;

import edu.noia.myoffice.common.domain.event.DomainEvent;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import lombok.NonNull;
import lombok.Value;

@Value(staticConstructor = "of")
public class FolderRemoved implements DomainEvent {

    @NonNull
    FolderId folderId;
}
