package edu.noia.myoffice.customer.domain.util.sanitizer;

import edu.noia.myoffice.common.util.sanitizer.Sanitizer;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.person.domain.util.sanitizer.PersonSanitizer;

public interface CustomerSanitizer {

    Sanitizer<CustomerState> person = state ->
            PersonSanitizer.person.sanitize(state.getPerson()).map(state::setPerson);
}
