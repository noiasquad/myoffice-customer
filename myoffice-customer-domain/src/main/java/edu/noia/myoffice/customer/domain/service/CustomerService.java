package edu.noia.myoffice.customer.domain.service;

import edu.noia.myoffice.common.domain.event.EventPublisher;
import edu.noia.myoffice.customer.domain.entity.Customer;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.entity.Folder;
import edu.noia.myoffice.customer.domain.event.CustomerRemoved;
import edu.noia.myoffice.customer.domain.factory.FolderFactory;
import edu.noia.myoffice.customer.domain.repository.CustomerRepository;
import edu.noia.myoffice.customer.domain.repository.FolderRepository;
import edu.noia.myoffice.customer.domain.util.finder.CustomerFinder;
import edu.noia.myoffice.customer.domain.util.finder.FolderFinder;
import edu.noia.myoffice.customer.domain.vo.Affiliation;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;

import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CustomerService {

    CustomerRepository customerRepository;
    FolderRepository folderRepository;
    EventPublisher eventPublisher;
    FolderFactory folderFactory;
    FolderFinder folderFinder;
    CustomerFinder customerFinder;

    public CustomerService(@NonNull CustomerRepository customerRepository,
                           @NonNull FolderRepository folderRepository,
                           @NonNull EventPublisher eventPublisher) {

        this.customerRepository = customerRepository;
        this.folderRepository = folderRepository;
        this.eventPublisher = eventPublisher;
        this.folderFactory = new FolderFactory();
        this.folderFinder = new FolderFinder(folderRepository);
        this.customerFinder = new CustomerFinder(customerRepository);
    }

    public Customer create(CustomerState data) {
        Customer customer = Customer.of(data)
                .publish(eventPublisher)
                .persist(customerRepository);
        affiliate(customer);
        return customer;
    }

    public Customer create(CustomerState data, List<FolderId> affiliations) {
        Customer customer = Customer.of(data)
                .publish(eventPublisher)
                .persist(customerRepository);
        affiliations.forEach(folderId -> affiliate(customer, folderFinder.find(folderId)));
        return customer;
    }

    private Affiliation affiliate(Customer customer, Folder folder) {
        folder.affiliate(customer.getId())
                .publish(eventPublisher)
                .persist(folderRepository);
        return Affiliation.of(folder.getId(), customer.getId());
    }

    private Affiliation affiliate(Customer customer) {
        return affiliate(customer, folderFactory.create(customer));
    }

    public Customer modify(CustomerId customerId, CustomerState modifier) {
        return customerFinder.find(customerId)
                .modify(modifier)
                .publish(eventPublisher)
                .persist(customerRepository);
    }

    public void remove(CustomerId customerId) {
        customerFinder.check(customerId);
        folderRepository.findByCustomerId(customerId).forEach(folder ->
                Folder.ofValid(folder.getValue0(), folder.getValue1())
                        .remove(customerId)
                        .publish(eventPublisher)
                        .persist(folderRepository));
        customerRepository.remove(customerId);
        eventPublisher.publish(CustomerRemoved.of(customerId));
    }
}