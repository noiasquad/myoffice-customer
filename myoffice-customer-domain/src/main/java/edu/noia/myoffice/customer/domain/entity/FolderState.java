package edu.noia.myoffice.customer.domain.entity;

import edu.noia.myoffice.customer.domain.vo.CustomerId;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Set;

public interface FolderState {

    @NotNull
    String getName();

    FolderState setName(String value);

    String getNotes();

    FolderState setNotes(String value);

    CustomerId getPrimaryAffiliate();

    FolderState setPrimaryAffiliate(CustomerId affiliateId);

    @Valid
    Set<CustomerId> getAffiliates();

    FolderState setAffiliates(Set<CustomerId> affiliates);

    default FolderState modify(FolderState modifier) {
        return setName(modifier.getName())
                .setNotes(modifier.getNotes())
                .setAffiliates(modifier.getAffiliates())
                .setPrimaryAffiliate(getAffiliates().isEmpty() ? null : modifier.getPrimaryAffiliate());
    }

    FolderState addAffiliate(CustomerId affiliateId);

    FolderState removeAffiliate(CustomerId affiliateId);
}
