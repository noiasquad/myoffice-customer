package edu.noia.myoffice.customer.domain.entity;

import edu.noia.myoffice.common.domain.entity.BaseDomainEntity;
import edu.noia.myoffice.common.util.sanitizer.Sanitizer;
import edu.noia.myoffice.common.util.validation.BeanValidator;
import edu.noia.myoffice.customer.domain.event.CustomerCreated;
import edu.noia.myoffice.customer.domain.event.CustomerModified;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.CustomerSample;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.stream.Stream;

@Slf4j
@EqualsAndHashCode(callSuper = true)
public class Customer extends BaseDomainEntity<Customer, CustomerId, CustomerState> {

    protected Customer(CustomerId id, CustomerState state) {
        super(id, state);
    }

    /**
     * Create a customer entity from a given state
     *
     * @param state a given set of attributes
     * @return a {@link Customer} instance with a generated identity
     */
    public static Customer of(@NonNull CustomerState state) {
        return of(CustomerId.random(), state);
    }

    /**
     * Create a customer entity from a given state
     *
     * @param id    a given identity
     * @param state a given set of attributes
     * @return a {@link Customer} instance with a generated id
     */
    public static Customer of(@NonNull CustomerId id, @NonNull CustomerState state) {
        return ofValid(id, validate(state));
    }

    /**
     * Create a customer entity from a given state declared as valid, and registers a edu.noia.myoffice.customer.edu.noia.myoffice.customer.domain event
     *
     * @param id    a given identity
     * @param state a given set of attributes
     * @return a {@link Customer} instance with a generated id
     */
    public static Customer ofValid(@NonNull CustomerId id, @NonNull CustomerState state) {
        Customer customer = new Customer(id, state);
        return customer.andEvent(CustomerCreated.of(id, CustomerSample.from(customer.state)));
    }

    /**
     * Validate a given state
     *
     * @param state a given set of attributes
     * @return the given state
     * @throws {@link javax.validation.ValidationException}
     */
    private static CustomerState validate(CustomerState state) {
        return BeanValidator.validate(state);
    }

    /**
     * Modify the state of the entity
     *
     * @param modifier a set of non-null attributes
     * @return the entity
     */
    public Customer modify(CustomerState modifier) {
        return ((Customer) setState(validate(state.modify(modifier))))
                .andEvent(CustomerModified.of(id, CustomerSample.from(state)));
    }

    /**
     * Apply a set of cleanup actions to be applied on the current state of the entity
     *
     * @param sanitizers a set of cleanup actions
     * @return the entity
     */
    public Customer sanitize(Sanitizer<CustomerState>... sanitizers) {
        Stream.of(sanitizers).forEach(sanitizer -> setState(sanitizer.sanitize(state).orElse(state)));
        return this;
    }

    @Override
    protected CustomerState cloneState() {
        return CustomerSample.from(state);
    }
}