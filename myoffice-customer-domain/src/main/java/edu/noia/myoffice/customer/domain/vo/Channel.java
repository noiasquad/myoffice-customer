package edu.noia.myoffice.customer.domain.vo;

import lombok.NonNull;
import lombok.Value;

import javax.validation.constraints.NotNull;

@Value(staticConstructor = "of")
public class Channel {

    @NotNull
    @NonNull
    private Type type;
    @NotNull
    @NonNull
    private String value;

    private String comment;

    public enum Type {
        PHONE,
        PHONE_HOME,
        PHONE_OFFICE,
        MOBILE,
        FAX,
        EMAIL,
        EMAIL_HOME,
        EMAIL_OFFICE,
        LINKEDIN,
        FACEBOOK,
        GOOGLEPLUS,
        TWITTER,
        INSTAGRAM,
        SKYPE,
        WEB;
    }
}
