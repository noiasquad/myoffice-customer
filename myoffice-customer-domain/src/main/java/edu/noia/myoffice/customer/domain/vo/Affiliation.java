package edu.noia.myoffice.customer.domain.vo;

import lombok.*;

@Getter
@EqualsAndHashCode(of = {"folderId"}, doNotUseGetters = true, callSuper = false)
@AllArgsConstructor(staticName = "of")
@RequiredArgsConstructor(staticName = "of")
public class Affiliation {

    @NonNull
    FolderId folderId;
    @NonNull
    CustomerId customerId;
    Boolean isPrimaryDebtor;
}
