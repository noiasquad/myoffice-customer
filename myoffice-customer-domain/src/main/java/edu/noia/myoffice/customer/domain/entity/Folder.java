package edu.noia.myoffice.customer.domain.entity;

import edu.noia.myoffice.common.domain.entity.BaseDomainEntity;
import edu.noia.myoffice.common.util.validation.BeanValidator;
import edu.noia.myoffice.customer.domain.event.*;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import edu.noia.myoffice.customer.domain.vo.FolderSample;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.Set;

import static edu.noia.myoffice.common.util.exception.ExceptionSuppliers.itemNotFound;
import static java.util.Collections.unmodifiableSet;

@Slf4j
@EqualsAndHashCode(callSuper = true)
public class Folder extends BaseDomainEntity<Folder, FolderId, FolderState> {

    protected Folder(FolderId id, FolderState state) {
        super(id, state);
    }

    /**
     * Create a folder entity from a given state
     *
     * @param state a given set of attributes
     * @return a {@link Folder} instance with a generated identity
     */
    public static Folder of(@NonNull FolderState state) {
        return of(FolderId.random(), state);
    }

    /**
     * Create a folder entity from a given state
     *
     * @param id    a given identity
     * @param state a given set of attributes
     * @return a {@link Folder} instance with a generated id
     */
    public static Folder of(@NonNull FolderId id, @NonNull FolderState state) {
        return ofValid(id, FolderSample.from(validate(state)));
    }

    /**
     * Create a folder entity from a given state declared as valid, and registers a domain event
     *
     * @param id    a given identity
     * @param state a given set of attributes
     * @return a {@link Folder} instance with a generated id
     */
    public static Folder ofValid(@NonNull FolderId id, @NonNull FolderState state) {
        Folder folder = new Folder(id, state);
        return folder.andEvent(FolderCreated.of(id, FolderSample.from(folder.state)));
    }

    /**
     * Validate a given state
     *
     * @param state a given set of attributes
     * @return the given state
     * @throws javax.validation.ValidationException
     */
    private static <T> T validate(T state) {
        return BeanValidator.validate(state);
    }

    /**
     * Modify the state of the entity
     *
     * @param modifier a set of non-null attributes
     * @return the entity
     */
    public Folder modify(FolderState modifier) {
        boolean hasPromotion = Optional.ofNullable(modifier.getPrimaryAffiliate())
                .map(affiliate -> !affiliate.equals(state.getPrimaryAffiliate()))
                .orElse(false);

        Folder folder = ((Folder)setState(validate(state.modify(modifier))))
                .andEvent(FolderModified.of(id, FolderSample.from(state)));

        return hasPromotion ? folder.promote(modifier.getPrimaryAffiliate()) : folder;
    }

    /**
     * @return the set of affiliates
     */
    public Set<CustomerId> getAffiliates() {
        return unmodifiableSet(state.getAffiliates());
    }

    /**
     * Add a new affiliate identified by its identity and register a domain event
     *
     * @param customerId a given identity
     * @return a {@link Folder} instance
     */
    public Folder affiliate(CustomerId customerId) {
        state.addAffiliate(customerId);
        return state.getAffiliates().size() == 1 ?
                andEvent(AffiliateCreated.of(getId(), customerId))
                        .andEvent(AffiliatePromoted.of(getId(), customerId)) :
                andEvent(AffiliateCreated.of(getId(), customerId));
    }

    /**
     * Promote an affiliate identified by its identity as primary and register a domain event
     *
     * @param customerId a given identity
     * @return a {@link Folder} instance
     */
    public Folder promote(CustomerId customerId) {
        if (state.getAffiliates().contains(customerId)) {
            state.setPrimaryAffiliate(customerId);
            return andEvent(AffiliatePromoted.of(getId(), customerId));
        } else {
            throw itemNotFound(Customer.class, customerId, Folder.class, getId()).get();
        }
    }

    /**
     * Remove an affiliate
     *
     * @param customerId the identity of the affiliate
     * @return the entity
     */
    public Folder remove(CustomerId customerId) {
        state.removeAffiliate(customerId);
        return andEvent(AffiliateRemoved.of(getId(), customerId));
    }

    @Override
    protected FolderState cloneState() {
        return FolderSample.from(state);
    }
}
