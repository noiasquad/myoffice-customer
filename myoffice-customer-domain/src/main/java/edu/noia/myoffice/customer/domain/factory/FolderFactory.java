package edu.noia.myoffice.customer.domain.factory;

import edu.noia.myoffice.customer.domain.entity.Customer;
import edu.noia.myoffice.customer.domain.entity.Folder;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.vo.FolderSample;

public class FolderFactory {

    public Folder create(FolderState data) {
        return Folder.of(data);
    }

    public Folder create(Customer customer) {
        return Folder.of(FolderSample.of(customer.getState().getPerson().getFullname()));
    }
}
