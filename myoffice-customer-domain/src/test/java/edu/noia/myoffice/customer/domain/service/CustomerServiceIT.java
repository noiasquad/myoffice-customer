package edu.noia.myoffice.customer.domain.service;

import edu.noia.myoffice.customer.domain.entity.Customer;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.repository.CustomerRepository;
import edu.noia.myoffice.customer.domain.repository.FolderRepository;
import edu.noia.myoffice.customer.domain.test.config.ITConfiguration;
import edu.noia.myoffice.customer.domain.test.util.TestCustomer;
import edu.noia.myoffice.customer.domain.test.util.TestFolder;
import edu.noia.myoffice.customer.domain.vo.Affiliation;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import org.javatuples.Pair;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = {ITConfiguration.class})
@RunWith(SpringRunner.class)
public class CustomerServiceIT {

    @Autowired
    private CustomerService service;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private FolderRepository folderRepository;

    @After
    public void rollback() {
        customerRepository
                .findAll()
                .collect(toList())
                .forEach(customer -> customerRepository.remove(customer.getValue0()));
        folderRepository
                .findAll()
                .collect(toList())
                .forEach(folder -> folderRepository.remove(folder.getValue0()));
    }

    @Test
    public void create_should_return_an_affiliation_of_the_customer_and_a_new_folder() {
        // Given
        CustomerState anyCustomer = TestCustomer.randomValid();
        // When
        Customer customer = service.create(anyCustomer);
        // Then
        assertThat(customer.getId()).isNotNull();
        List<Affiliation> affiliations = folderRepository.findAffiliationsById(customer.getId()).collect(toList());
        assertThat(affiliations).hasSize(1);
    }

    @Test
    public void create_in_an_existing_folder_should_return_an_affiliation_of_the_customer_and_this_folder() {
        // Given
        Pair<FolderId, FolderState> anyFolder = Pair.with(FolderId.random(), TestFolder.randomValid());
        folderRepository.put(anyFolder.getValue0(), anyFolder.getValue1());
        CustomerState anyCustomer = TestCustomer.randomValid();
        // When
        Customer customer = service.create(anyCustomer, singletonList(anyFolder.getValue0()));
        // Then
        assertThat(customer.getId()).isNotNull();
        List<Affiliation> affiliations = folderRepository.findAffiliationsById(customer.getId()).collect(toList());
        assertThat(affiliations).hasSize(1);

        Affiliation affiliation = affiliations.get(0);
        assertThat(affiliation.getCustomerId()).isNotNull();
        assertThat(affiliation.getFolderId()).isEqualTo(anyFolder.getValue0());
        assertThat(customerRepository.findOne(affiliation.getCustomerId()).isPresent()).isTrue();
        assertThat(folderRepository.findOne(affiliation.getFolderId()).isPresent()).isTrue();
        assertThat(folderRepository.findOne(affiliation.getFolderId()).get().getAffiliates())
                .contains(affiliation.getCustomerId());
    }
}
