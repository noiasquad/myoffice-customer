package edu.noia.myoffice.customer.domain.service;

import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.repository.CustomerRepository;
import edu.noia.myoffice.customer.domain.repository.FolderRepository;
import edu.noia.myoffice.customer.domain.test.config.ITConfiguration;
import edu.noia.myoffice.customer.domain.test.util.TestCustomer;
import edu.noia.myoffice.customer.domain.test.util.TestFolder;
import edu.noia.myoffice.customer.domain.vo.Affiliation;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import org.javatuples.Pair;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = {ITConfiguration.class})
@RunWith(SpringRunner.class)
public class FolderServiceIT {

    @Autowired
    private FolderService service;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private FolderRepository folderRepository;

    @After
    public void rollback() {
        customerRepository
                .findAll()
                .collect(toList())
                .forEach(customer -> customerRepository.remove(customer.getValue0()));
        folderRepository
                .findAll()
                .collect(toList())
                .forEach(folder -> folderRepository.remove(folder.getValue0()));
    }

    @Test
    public void affiliate_an_existing_customer_into_an_existing_folder_should_return_the_right_affiliation() {
        // Given
        Pair<CustomerId, CustomerState> anyCustomer = Pair.with(CustomerId.random(), TestCustomer.randomValid());
        Pair<FolderId, FolderState> anyFolder = Pair.with(FolderId.random(), TestFolder.randomValid());

        folderRepository.put(anyFolder.getValue0(), anyFolder.getValue1());
        customerRepository.put(anyCustomer.getValue0(), anyCustomer.getValue1());
        // When
        Affiliation affiliation = service.affiliate(anyFolder.getValue0(), anyCustomer.getValue0());
        // Then
        assertThat(affiliation).isNotNull();
        assertThat(affiliation.getCustomerId()).isNotNull();
        assertThat(affiliation.getCustomerId()).isEqualTo(anyCustomer.getValue0());
        assertThat(affiliation.getFolderId()).isEqualTo(anyFolder.getValue0());
        assertThat(folderRepository.findOne(affiliation.getFolderId()).isPresent()).isTrue();
        assertThat(folderRepository.findOne(affiliation.getFolderId()).get().getAffiliates())
                .contains(affiliation.getCustomerId());
    }
}
