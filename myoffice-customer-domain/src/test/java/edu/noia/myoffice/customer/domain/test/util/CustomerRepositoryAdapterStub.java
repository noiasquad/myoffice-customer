package edu.noia.myoffice.customer.domain.test.util;

import edu.noia.myoffice.common.domain.repository.InMemoryKeyValueRepository;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.repository.CustomerRepository;
import edu.noia.myoffice.customer.domain.vo.CustomerId;

public class CustomerRepositoryAdapterStub
        extends InMemoryKeyValueRepository<CustomerId, CustomerState>
        implements CustomerRepository {
}
