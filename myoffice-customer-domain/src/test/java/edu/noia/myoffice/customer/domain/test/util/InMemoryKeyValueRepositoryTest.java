package edu.noia.myoffice.customer.domain.test.util;

import edu.noia.myoffice.address.domain.vo.PostalAddress;
import edu.noia.myoffice.common.domain.repository.InMemoryKeyValueRepository;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.CustomerSample;
import edu.noia.myoffice.person.domain.vo.Person;
import org.javatuples.Pair;
import org.junit.Before;
import org.junit.Test;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

public class InMemoryKeyValueRepositoryTest {

    private InMemoryKeyValueRepository<CustomerId, CustomerState> repository;

    @Before
    public void setup() {
        repository = new InMemoryKeyValueRepository();
    }

    @Test
    public void should_put_find_delete_as_expected() {
        // given
        CustomerId anyId = CustomerId.random();
        CustomerState anyState = TestCustomer.randomValid();
        // when
        repository.put(anyId, anyState);
        // then
        assertThat(repository.findOne(anyId).isPresent()).isTrue();
        assertThat(repository.findAll()).containsExactly(Pair.with(anyId, anyState));

        // given
        CustomerId otherId = CustomerId.random();
        CustomerState otherState = TestCustomer.randomValid();
        // when
        repository.put(otherId, otherState);
        // then
        assertThat(repository.findOne(otherId).isPresent()).isTrue();
        assertThat(repository.findAll())
                .containsExactlyInAnyOrder(Pair.with(anyId, anyState), Pair.with(otherId, otherState));

        // given
        CustomerState modifiedState = anyState.modify(
                CustomerSample.of(new Person()
                        .setLastName("anyName"))
                        .setAddresses(singletonList(new PostalAddress()
                                .setStreet("one street")
                                .setHouseNbr("one house")
                                .setZip("one zip")
                                .setCity("one city")
                                .setRegion("one region")
                                .setCountry("one country"))));
        // when
        repository.put(otherId, modifiedState);
        // then
        assertThat(repository.findOne(otherId).isPresent()).isTrue();
        assertThat(repository.findAll())
                .containsExactlyInAnyOrder(Pair.with(anyId, anyState), Pair.with(otherId, modifiedState));

        // when
        repository.remove(anyId);
        // then
        assertThat(repository.findAll()).containsExactly(Pair.with(otherId, modifiedState));

        // when
        repository.remove(otherId);
        // then
        assertThat(repository.findAll()).isEmpty();
    }
}
