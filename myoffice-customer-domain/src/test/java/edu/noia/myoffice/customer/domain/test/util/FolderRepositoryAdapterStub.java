package edu.noia.myoffice.customer.domain.test.util;

import edu.noia.myoffice.common.domain.repository.InMemoryKeyValueRepository;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.repository.FolderRepository;
import edu.noia.myoffice.customer.domain.vo.Affiliation;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import org.javatuples.Pair;

import java.util.stream.Stream;

public class FolderRepositoryAdapterStub
        extends InMemoryKeyValueRepository<FolderId, FolderState>
        implements FolderRepository {

    @Override
    public Stream<Affiliation> findAffiliationsById(CustomerId customerId) {
        return store.entrySet()
                .stream()
                .filter(e -> e.getValue().getAffiliates().contains(customerId))
                .map(e -> Affiliation.of(e.getKey(), customerId));
    }

    @Override
    public Stream<Pair<FolderId, FolderState>> findByCustomerId(CustomerId customerId) {
        return store.entrySet()
                .stream()
                .filter(e -> e.getValue().getAffiliates().contains(customerId))
                .map(e -> Pair.with(e.getKey(), e.getValue()));
    }
}
