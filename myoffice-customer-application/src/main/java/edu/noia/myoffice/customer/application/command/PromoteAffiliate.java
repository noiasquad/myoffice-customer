package edu.noia.myoffice.customer.application.command;

import edu.noia.myoffice.common.application.command.Command;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import lombok.NonNull;
import lombok.Value;

@Value(staticConstructor = "of")
public class PromoteAffiliate implements Command {

    @NonNull
    FolderId folderId;
    @NonNull
    CustomerId customerId;
}
