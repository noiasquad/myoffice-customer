package edu.noia.myoffice.customer.application.config;

import edu.noia.myoffice.customer.application.query.CustomerReadRepository;
import edu.noia.myoffice.customer.application.query.DefaultCustomerReadRepository;
import edu.noia.myoffice.customer.application.query.DefaultFolderReadRepository;
import edu.noia.myoffice.customer.application.query.FolderReadRepository;
import edu.noia.myoffice.customer.application.service.ManageCustomerUseCase;
import edu.noia.myoffice.customer.application.service.ManageFolderUseCase;
import edu.noia.myoffice.customer.application.service.SanitizeCustomerUseCase;
import edu.noia.myoffice.customer.domain.repository.CustomerRepository;
import edu.noia.myoffice.customer.domain.repository.FolderRepository;
import edu.noia.myoffice.customer.domain.service.CustomerService;
import edu.noia.myoffice.customer.domain.service.FolderService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomerApplicationConfiguration {

    @Bean
    public ManageCustomerUseCase manageCustomerUseCase(CustomerService customerService,
                                                       FolderService folderService) {
        return new ManageCustomerUseCase(customerService, folderService);
    }

    @Bean
    public ManageFolderUseCase manageFolderUseCase(FolderService folderService) {
        return new ManageFolderUseCase(folderService);
    }

    @Bean
    public SanitizeCustomerUseCase sanitizeCustomerUseCase(FolderService folderService,
                                                           FolderRepository folderRepository,
                                                           CustomerRepository customerRepository) {
        return new SanitizeCustomerUseCase(folderService, folderRepository, customerRepository);
    }

    @Bean("application.customer.read.repository")
    public CustomerReadRepository readCustomerRepository(
            @Qualifier("data.customer.read.repository") CustomerReadRepository repository) {
        return new DefaultCustomerReadRepository(repository);
    }

    @Bean("application.folder.read.repository")
    public FolderReadRepository readFolderRepository(
            @Qualifier("data.folder.read.repository") FolderReadRepository repository) {
        return new DefaultFolderReadRepository(repository);
    }

}
