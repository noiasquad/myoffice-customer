package edu.noia.myoffice.customer.application.service;

import edu.noia.myoffice.customer.application.command.AffiliateCustomer;
import edu.noia.myoffice.customer.application.command.CreateCustomer;
import edu.noia.myoffice.customer.application.command.ModifyCustomer;
import edu.noia.myoffice.customer.application.command.RemoveCustomer;
import edu.noia.myoffice.customer.domain.entity.Customer;
import edu.noia.myoffice.customer.domain.service.CustomerService;
import edu.noia.myoffice.customer.domain.service.FolderService;
import edu.noia.myoffice.customer.domain.vo.Affiliation;
import edu.noia.myoffice.customer.domain.vo.CustomerSample;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ManageCustomerUseCase {

    @NonNull
    CustomerService customerService;
    @NonNull
    FolderService folderService;

    public Customer handle(CreateCustomer command) {
        return customerService.create(CustomerSample.of(command.getPerson())
                .setAddresses(command.getAddresses())
                .setProfile(command.getProfile())
                .setNotes(command.getNotes()), command.getAffiliations());
    }

    public Customer handle(ModifyCustomer command) {
        return customerService.modify(command.getId(), CustomerSample.of(command.getPerson())
                .setAddresses(command.getAddresses())
                .setProfile(command.getProfile())
                .setNotes(command.getNotes()));
    }

    public Affiliation handle(AffiliateCustomer command) {
        return folderService.affiliate(command.getFolderId(), command.getCustomerId());
    }

    public void handle(RemoveCustomer command) {
        customerService.remove(command.getId());
    }
}
