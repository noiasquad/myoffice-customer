package edu.noia.myoffice.customer.application.service;

import edu.noia.myoffice.customer.application.command.*;
import edu.noia.myoffice.customer.domain.entity.Folder;
import edu.noia.myoffice.customer.domain.service.FolderService;
import edu.noia.myoffice.customer.domain.vo.AffiliationId;
import edu.noia.myoffice.customer.domain.vo.FolderSample;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ManageFolderUseCase {

    @NonNull
    FolderService folderService;

    public Folder handle(CreateFolder command) {
        return folderService.create(FolderSample.of(command.getName())
                .setNotes(command.getNotes())
                .setAffiliates(command.getAffiliates())
                .setPrimaryAffiliate(command.getPrimaryAffiliate()));
    }

    public Folder handle(ModifyFolder command) {
        return folderService.modify(command.getId(), FolderSample.of(command.getName())
                .setNotes(command.getNotes())
                .setAffiliates(command.getAffiliates())
                .setPrimaryAffiliate(command.getPrimaryAffiliate()));
    }

    public void handle(AffiliateCustomer command) {
        folderService.affiliate(command.getFolderId(), command.getCustomerId());
    }

    public void handle(PromoteAffiliate command) {
        folderService.promote(command.getFolderId(), command.getCustomerId());
    }

    public void handle(CancelAffiliation command) {
        folderService.remove(AffiliationId.of(command.getFolderId(), command.getCustomerId()));
    }

    public void handle(RemoveFolder command) {
        folderService.remove(command.getId());
    }
}
