package edu.noia.myoffice.customer.application.command;

import edu.noia.myoffice.common.application.command.Command;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateFolder implements Command {

    @NotNull
    String name;
    String notes;
    Set<CustomerId> affiliates = new HashSet<>();
    CustomerId primaryAffiliate;
}
