package edu.noia.myoffice.customer.application.query;

import edu.noia.myoffice.common.application.repository.ApplicationReadRepository;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.vo.Affiliation;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import org.javatuples.Pair;

import java.util.stream.Stream;

public interface FolderReadRepository extends ApplicationReadRepository<FolderId, FolderState> {

    Stream<Affiliation> findAffiliationsById(CustomerId customerId);

    Stream<Pair<FolderId, FolderState>> findByCustomerId(CustomerId customerId);
}
