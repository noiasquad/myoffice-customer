package edu.noia.myoffice.customer.application.query;

import edu.noia.myoffice.common.util.search.FindCriteria;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.javatuples.Pair;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Transactional(readOnly = true)
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DefaultCustomerReadRepository implements CustomerReadRepository {

    @NonNull
    CustomerReadRepository repository;

    @Override
    public Optional<CustomerState> findOne(CustomerId id) {
        return repository.findOne(id);
    }

    @Override
    public Stream<Pair<CustomerId, CustomerState>> findOnes(CustomerId... id) {
        return repository.findOnes(id).collect(Collectors.toList()).stream();
    }

    @Override
    public Page<Pair<CustomerId, CustomerState>> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public Stream<Pair<CustomerId, CustomerState>> findSome(List<FindCriteria> criteria) {
        return repository.findSome(criteria).collect(Collectors.toList()).stream();
    }

    @Override
    public Page<Pair<CustomerId, CustomerState>> findSome(List<FindCriteria> criteria, Pageable pageable) {
        return repository.findSome(criteria, pageable);
    }

    @Override
    public Page<Pair<CustomerId, CustomerState>> search(Object value, Pageable pageable) {
        return repository.search(value, pageable);
    }
}
