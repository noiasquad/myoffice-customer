package edu.noia.myoffice.customer.application.command;

import edu.noia.myoffice.common.application.command.Command;
import edu.noia.myoffice.customer.domain.entity.Customer;
import lombok.NonNull;
import lombok.Value;

@Value(staticConstructor = "of")
public class SanitizeCustomer implements Command {

    @NonNull
    Customer customer;
}
