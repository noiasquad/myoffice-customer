package edu.noia.myoffice.customer.application.query;

import edu.noia.myoffice.common.util.search.FindCriteria;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.vo.Affiliation;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.javatuples.Pair;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Transactional(readOnly = true)
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DefaultFolderReadRepository implements FolderReadRepository {

    @NonNull
    FolderReadRepository repository;

    @Override
    public Optional<FolderState> findOne(FolderId id) {
        return repository.findOne(id);
    }

    @Override
    public Stream<Pair<FolderId, FolderState>> findOnes(FolderId... ids) {
        return repository.findOnes(ids).collect(Collectors.toList()).stream();
    }

    @Override
    public Page<Pair<FolderId, FolderState>> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public Stream<Pair<FolderId, FolderState>> findSome(List<FindCriteria> criteria) {
        return repository.findSome(criteria).collect(Collectors.toList()).stream();
    }

    @Override
    public Page<Pair<FolderId, FolderState>> findSome(List<FindCriteria> criteria, Pageable pageable) {
        return repository.findSome(criteria, pageable);
    }

    @Override
    public Page<Pair<FolderId, FolderState>> search(Object value, Pageable pageable) {
        return repository.search(value, pageable);
    }

    @Override
    public Stream<Affiliation> findAffiliationsById(CustomerId customerId) {
        return repository.findAffiliationsById(customerId).collect(Collectors.toList()).stream();
    }

    @Override
    public Stream<Pair<FolderId, FolderState>> findByCustomerId(CustomerId customerId) {
        return repository.findByCustomerId(customerId);
    }
}
