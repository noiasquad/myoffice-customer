package edu.noia.myoffice.customer.application.service;

import edu.noia.myoffice.customer.application.command.SanitizeCustomer;
import edu.noia.myoffice.customer.domain.repository.CustomerRepository;
import edu.noia.myoffice.customer.domain.repository.FolderRepository;
import edu.noia.myoffice.customer.domain.service.FolderService;
import edu.noia.myoffice.customer.domain.util.sanitizer.CustomerSanitizer;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SanitizeCustomerUseCase {

    @NonNull
    FolderService folderService;
    @NonNull
    FolderRepository folderRepository;
    @NonNull
    CustomerRepository customerRepository;

    public void handle(SanitizeCustomer command) {
        command.getCustomer().sanitize(CustomerSanitizer.person).persist(customerRepository);
        if (folderRepository.findAffiliationsById(command.getCustomer().getId()).count() > 0) {
            folderService.create(command.getCustomer());
        }
    }
}
