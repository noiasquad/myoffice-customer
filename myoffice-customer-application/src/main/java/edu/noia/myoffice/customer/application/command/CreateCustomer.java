package edu.noia.myoffice.customer.application.command;

import edu.noia.myoffice.common.application.command.Command;
import edu.noia.myoffice.address.domain.vo.Address;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import edu.noia.myoffice.customer.domain.vo.Profile;
import edu.noia.myoffice.person.domain.vo.Person;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateCustomer implements Command {

    List<FolderId> affiliations = new ArrayList<>();
    @NotNull
    Person person;
    List<Address> addresses = new ArrayList<>();
    Profile profile;
    String notes;
}
