package edu.noia.myoffice.customer.application.command;

import edu.noia.myoffice.common.application.command.Command;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.Set;

@Accessors(chain = true)
@Getter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ModifyFolder implements Command {

    @Setter
    FolderId id;
    String name;
    String notes;
    Set<CustomerId> affiliates = new HashSet<>();
    CustomerId primaryAffiliate;
}
