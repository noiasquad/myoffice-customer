package edu.noia.myoffice.customer.application.query;

import edu.noia.myoffice.common.application.repository.ApplicationReadRepository;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;

public interface CustomerReadRepository extends ApplicationReadRepository<CustomerId, CustomerState> {
}
