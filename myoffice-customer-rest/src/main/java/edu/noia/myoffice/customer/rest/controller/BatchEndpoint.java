package edu.noia.myoffice.customer.rest.controller;

import edu.noia.myoffice.customer.batch.job.SanityJob;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/customer/v1/batches")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class BatchEndpoint {

    @NonNull
    SanityJob sanityJob;

    @PostMapping("/sanity/jobs")
    public ResponseEntity sanitize(Pageable pageable) {
        sanityJob.execute();
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
