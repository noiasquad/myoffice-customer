package edu.noia.myoffice.customer.rest.mapper.hateoas;

import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import edu.noia.myoffice.customer.rest.controller.FolderEndpoint;
import edu.noia.myoffice.customer.rest.security.Authority;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.javatuples.Pair;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.LinkRelation;
import org.springframework.hateoas.server.core.EmbeddedWrapper;
import org.springframework.hateoas.server.core.EmbeddedWrappers;

import java.util.function.Predicate;
import java.util.stream.Stream;

import static edu.noia.myoffice.customer.rest.security.Authority.*;
import static java.util.stream.Collectors.toList;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class FolderResourceMapper {

    @NonNull
    Predicate<Authority> hasAuthority;

    public CollectionModel<EmbeddedWrapper> toResource(Stream<Pair<FolderId, FolderState>> content) {

        EmbeddedWrappers wrappers = new EmbeddedWrappers(true);
        CollectionModel<EmbeddedWrapper> resource = new CollectionModel<>(
                content.map(p -> wrappers.wrap(toResource(p.getValue0(), p.getValue1()), LinkRelation.of("content"))).collect(toList()));

        if (hasAuthority.test(CREATE_FOLDER)) {
            resource.add(linkTo(FolderEndpoint.class).withRel("create"));
        }
        return resource;
    }

    public EntityModel<Page<EntityModel<FolderState>>> toResource(Page<Pair<FolderId, FolderState>> content) {
        EntityModel<Page<EntityModel<FolderState>>> resource =
                new EntityModel<>(content.map(p -> toResource(p.getValue0(), p.getValue1())));

        if (hasAuthority.test(CREATE_FOLDER)) {
            resource.add(linkTo(FolderEndpoint.class).withRel("create"));
        }
        return resource;
    }

    public EntityModel<FolderState> toResource(FolderId id, FolderState state) {
        EntityModel resource = new EntityModel<>(state);

        if (hasAuthority.test(VIEW_FOLDER)) {
            resource.add(linkTo(FolderEndpoint.class)
                    .slash(id.getId())
                    .withSelfRel());
        }
        if (hasAuthority.test(MODIFY_FOLDER)) {
            resource.add(linkTo(FolderEndpoint.class)
                    .slash(id.getId())
                    .withRel("patch"));
        }
        if (hasAuthority.test(REMOVE_FOLDER)) {
            resource.add(linkTo(FolderEndpoint.class)
                    .slash(id.getId())
                    .withRel("remove"));
        }
        if (hasAuthority.test(AFFILIATE)) {
            resource.add(linkTo(FolderEndpoint.class)
                    .slash(id.getId())
                    .slash("affiliates")
                    .slash("{id}")
                    .withRel("affiliate"));
        }
        if (hasAuthority.test(UNAFFILIATE)) {
            resource.add(linkTo(FolderEndpoint.class)
                    .slash(id.getId())
                    .slash("affiliates")
                    .slash("{id}")
                    .withRel("unaffiliate"));
        }
        return resource;
    }
}
