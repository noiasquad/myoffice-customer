package edu.noia.myoffice.customer.rest.util;

import edu.noia.myoffice.common.application.repository.ApplicationReadRepository;
import edu.noia.myoffice.common.domain.vo.Identity;
import edu.noia.myoffice.common.util.search.FindCriteria;
import org.javatuples.Pair;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public class Search {

    public static <I extends Identity, S> Page<Pair<I, S>> execute(String term,
                                                                   String[] fields,
                                                                   String[] filters,
                                                                   Pageable pageable,
                                                                   ApplicationReadRepository<I, S> repository) {
        return (term != null ? fields != null ?
                repository.findSome(FindCriteria.from(term, fields), pageable) :
                repository.search(term, pageable) :
                filters != null ?
                        repository.findSome(FindCriteria.from(filters), pageable) :
                        repository.findAll(pageable));
    }
}
