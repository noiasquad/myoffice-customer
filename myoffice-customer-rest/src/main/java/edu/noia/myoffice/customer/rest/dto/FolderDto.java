package edu.noia.myoffice.customer.rest.dto;

import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public final class FolderDto implements FolderState {

    String name;
    String notes;
    Set<CustomerId> affiliates = new HashSet<>();
    CustomerId primaryAffiliate;

    @Override
    public FolderState addAffiliate(CustomerId affiliate) {
        affiliates.add(affiliate);
        return this;
    }

    @Override
    public FolderState removeAffiliate(CustomerId affiliate) {
        affiliates.remove(affiliate);
        return this;
    }
}
