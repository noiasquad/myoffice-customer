package edu.noia.myoffice.customer.rest.mapper.hateoas;

import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import edu.noia.myoffice.customer.rest.controller.CustomerEndpoint;
import edu.noia.myoffice.customer.rest.controller.FolderEndpoint;
import edu.noia.myoffice.customer.rest.security.Authority;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;

import java.util.Set;
import java.util.function.Predicate;

import static edu.noia.myoffice.customer.rest.security.Authority.*;
import static java.util.stream.Collectors.toList;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AffiliateResourceMapper {

    @NonNull
    Predicate<Authority> hasAuthority;

    public CollectionModel<EntityModel<CustomerId>> toResource(FolderId folderId, Set<CustomerId> content) {
        CollectionModel<EntityModel<CustomerId>> resource = new CollectionModel<>(
                content.stream().map(affiliate -> toResource(folderId, affiliate)).collect(toList()));

        if (hasAuthority.test(AFFILIATE)) {
            resource.add(linkTo(FolderEndpoint.class)
                    .slash(folderId.getId())
                    .slash("affiliates")
                    .slash("{id}")
                    .withRel("affiliate"));
        }
        return resource;
    }

    public EntityModel<CustomerId> toResource(FolderId folderId, CustomerId affiliate) {
        EntityModel<CustomerId> resource = new EntityModel<>(affiliate);

        if (hasAuthority.test(VIEW_CUSTOMER)) {
            resource.add(linkTo(CustomerEndpoint.class)
                    .slash(affiliate.getId())
                    .withRel("customer"));
        }
        if (hasAuthority.test(VIEW_FOLDER)) {
            resource.add(linkTo(FolderEndpoint.class)
                    .slash(folderId.getId())
                    .withRel("folder"));
        }
        if (hasAuthority.test(UNAFFILIATE)) {
            resource.add(linkTo(FolderEndpoint.class)
                    .slash(folderId.getId())
                    .slash("affiliates")
                    .slash(affiliate.getId())
                    .withRel("remove"));
        }
        return resource;
    }
}
