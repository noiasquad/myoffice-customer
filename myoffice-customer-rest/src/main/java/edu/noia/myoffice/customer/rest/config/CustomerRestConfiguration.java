package edu.noia.myoffice.customer.rest.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import edu.noia.myoffice.address.rest.serializer.AddressSerializers;
import edu.noia.myoffice.common.rest.exception.EndpointExceptionHandler;
import edu.noia.myoffice.common.serializer.CommonSerializers;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.rest.mixin.CustomerMixin;
import edu.noia.myoffice.customer.rest.mixin.FolderMixin;
import edu.noia.myoffice.customer.rest.serializer.CustomerSerializers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

@ComponentScan(basePackageClasses = {
        EndpointExceptionHandler.class
})
@Configuration
public class CustomerRestConfiguration {

    @Bean
    public Jackson2ObjectMapperBuilder jacksonBuilder() {
        return new Jackson2ObjectMapperBuilder()
                .serializationInclusion(JsonInclude.Include.NON_EMPTY)
                .mixIn(FolderState.class, FolderMixin.class)
                .mixIn(CustomerState.class, CustomerMixin.class)
                .modules(
                        CommonSerializers.getModule(),
                        CustomerSerializers.getModule(),
                        AddressSerializers.getModule());
    }
}