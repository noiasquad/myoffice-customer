package edu.noia.myoffice.customer.rest.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleModule;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CustomerSerializers {

    public static Module getModule() {
        SimpleModule module = new SimpleModule();
        module.addDeserializer(CustomerId.class, new CustomerIdDeserializer());
        module.addDeserializer(FolderId.class, new FolderIdDeserializer());
        module.addSerializer(CustomerId.class, new CustomerIdSerializer());
        module.addSerializer(FolderId.class, new FolderIdSerializer());
        return module;
    }

    public static class CustomerIdSerializer extends JsonSerializer<CustomerId> {
        @Override
        public void serialize(CustomerId customerId, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            if (customerId != null) {
                gen.writeString(customerId.getId().toString());
            }
        }
    }

    public static class CustomerIdDeserializer extends JsonDeserializer<CustomerId> {
        @Override
        public CustomerId deserialize(JsonParser parser, DeserializationContext ctx) throws IOException {
            return Optional.ofNullable(parser.readValueAs(String.class))
                    .filter(StringUtils::hasText)
                    .map(UUID::fromString)
                    .map(CustomerId::of)
                    .orElse(null);
        }
    }

    public static class FolderIdSerializer extends JsonSerializer<FolderId> {
        @Override
        public void serialize(FolderId folderId, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            if (folderId != null) {
                gen.writeString(folderId.getId().toString());
            }
        }
    }

    public static class FolderIdDeserializer extends JsonDeserializer<FolderId> {
        @Override
        public FolderId deserialize(JsonParser parser, DeserializationContext ctx) throws IOException {
            return Optional.ofNullable(parser.readValueAs(String.class))
                    .filter(StringUtils::hasText)
                    .map(UUID::fromString)
                    .map(FolderId::of)
                    .orElse(null);
        }
    }
}
