package edu.noia.myoffice.customer.rest.controller;

import edu.noia.myoffice.common.rest.util.IdentifiantPropertyEditorSupport;
import edu.noia.myoffice.common.util.finder.Finder;
import edu.noia.myoffice.customer.application.command.CreateCustomer;
import edu.noia.myoffice.customer.application.command.ModifyCustomer;
import edu.noia.myoffice.customer.application.command.RemoveCustomer;
import edu.noia.myoffice.customer.application.query.CustomerReadRepository;
import edu.noia.myoffice.customer.application.query.FolderReadRepository;
import edu.noia.myoffice.customer.application.service.ManageCustomerUseCase;
import edu.noia.myoffice.customer.domain.entity.Customer;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.rest.mapper.hateoas.AffiliationResourceMapper;
import edu.noia.myoffice.customer.rest.mapper.hateoas.CustomerResourceMapper;
import edu.noia.myoffice.customer.rest.security.Authority;
import edu.noia.myoffice.customer.rest.util.Search;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.core.EmbeddedWrapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static edu.noia.myoffice.customer.rest.mapper.CustomerMapper.toDto;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping(path = "/api/customer/v1/customers")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CustomerEndpoint {

    ManageCustomerUseCase customerUseCase;
    CustomerReadRepository customerRepository;
    FolderReadRepository folderRepository;

    AffiliationResourceMapper affiliationResourceMapper = new AffiliationResourceMapper(Authority.check);
    CustomerResourceMapper customerResourceMapper = new CustomerResourceMapper(Authority.check);


    CustomerEndpoint(
            ManageCustomerUseCase customerUseCase,
            @Qualifier("application.customer.read.repository") CustomerReadRepository customerRepository,
            @Qualifier("application.folder.read.repository") FolderReadRepository folderRepository) {

        this.customerUseCase = customerUseCase;
        this.customerRepository = customerRepository;
        this.folderRepository = folderRepository;
    }

    @PostMapping
    public ResponseEntity<EntityModel<CustomerState>> create(@RequestBody CreateCustomer command) {
        Customer customer = customerUseCase.handle(command);
        return status(CREATED).body(customerResourceMapper.toResource(customer.getId(), customer.getState()));
    }

    @PutMapping("/{id}")
    public ResponseEntity<EntityModel<CustomerState>> patch(
            @PathVariable("id") CustomerId customerId,
            @RequestBody ModifyCustomer command) {
        Customer customer = customerUseCase.handle(command.setId(customerId));
        return ok(customerResourceMapper.toResource(customer.getId(), customer.getState()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<EntityModel<CustomerState>> findOne(@PathVariable("id") CustomerId customerId) {
        return ok(customerResourceMapper.toResource(customerId,
                Finder.find(CustomerState.class, customerId, customerRepository::findOne)));
    }

    @GetMapping("/ones")
    public ResponseEntity<CollectionModel<EmbeddedWrapper>> findOnes(@RequestParam CustomerId[] id) {
        return ok(customerResourceMapper.toResource(
                customerRepository.findOnes(id)
                        .map(p -> Pair.with(p.getValue0(), toDto.map(p.getValue1())))));
    }

    @GetMapping
    public ResponseEntity<EntityModel<Page<EntityModel<CustomerState>>>> findAll(Pageable pageable) {
        return ok(customerResourceMapper.toResource(
                customerRepository.findAll(pageable)
                        .map(p -> Pair.with(p.getValue0(), toDto.map(p.getValue1())))));
    }

    @GetMapping("/{id}/affiliations")
    public ResponseEntity<CollectionModel<EmbeddedWrapper>> findAffiliations(@PathVariable("id") CustomerId customerId) {
        return ok(affiliationResourceMapper.toResource(folderRepository.findAffiliationsById(customerId)));
    }

    @GetMapping("/search")
    public ResponseEntity<EntityModel<Page<EntityModel<CustomerState>>>> search(
            @RequestParam(required = false) String term,
            @RequestParam(required = false) String[] fields,
            @RequestParam(required = false) String[] filters,
            Pageable pageable) {

        return ok(customerResourceMapper.toResource(
                Search.execute(term, fields, filters, pageable, customerRepository)
                        .map(p -> Pair.with(p.getValue0(), toDto.map(p.getValue1())))));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") CustomerId customerId) {
        customerUseCase.handle(RemoveCustomer.of(customerId));
        return ResponseEntity.ok().build();
    }

    @InitBinder
    public void dataBinding(WebDataBinder binder) {
        binder.registerCustomEditor(CustomerId.class,
                new IdentifiantPropertyEditorSupport<>(s -> CustomerId.of(UUID.fromString(s))));
    }
}
