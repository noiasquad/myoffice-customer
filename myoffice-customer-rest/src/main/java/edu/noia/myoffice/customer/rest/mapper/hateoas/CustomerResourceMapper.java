package edu.noia.myoffice.customer.rest.mapper.hateoas;

import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.rest.controller.CustomerEndpoint;
import edu.noia.myoffice.customer.rest.security.Authority;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.javatuples.Pair;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.LinkRelation;
import org.springframework.hateoas.server.core.EmbeddedWrapper;
import org.springframework.hateoas.server.core.EmbeddedWrappers;

import java.util.function.Predicate;
import java.util.stream.Stream;

import static edu.noia.myoffice.customer.rest.security.Authority.*;
import static java.util.stream.Collectors.toList;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CustomerResourceMapper {

    @NonNull
    Predicate<Authority> hasAuthority;

    public CollectionModel<EmbeddedWrapper> toResource(Stream<Pair<CustomerId, CustomerState>> content) {

        EmbeddedWrappers wrappers = new EmbeddedWrappers(true);
        CollectionModel<EmbeddedWrapper> resource = new CollectionModel<>(
                content.map(p -> wrappers.wrap(toResource(p.getValue0(), p.getValue1()), LinkRelation.of("content"))).collect(toList()));

        if (hasAuthority.test(CREATE_CUSTOMER)) {
            resource.add(linkTo(CustomerEndpoint.class).withRel("create"));
        }
        return resource;
    }

    public EntityModel<Page<EntityModel<CustomerState>>> toResource(Page<Pair<CustomerId, CustomerState>> content) {
        EntityModel<Page<EntityModel<CustomerState>>> resource =
                new EntityModel<>(content.map(p -> toResource(p.getValue0(), p.getValue1())));

        if (hasAuthority.test(CREATE_CUSTOMER)) {
            resource.add(linkTo(CustomerEndpoint.class).withRel("create"));
        }
        return resource;
    }

    public EntityModel<CustomerState> toResource(CustomerId id, CustomerState state) {
        EntityModel resource = new EntityModel<>(state);

        if (hasAuthority.test(VIEW_CUSTOMER)) {
            resource.add(linkTo(CustomerEndpoint.class).slash(id.getId()).withSelfRel());
        }
        if (hasAuthority.test(MODIFY_CUSTOMER)) {
            resource.add(linkTo(CustomerEndpoint.class).slash(id.getId()).withRel("patch"));
        }
        if (hasAuthority.test(REMOVE_CUSTOMER)) {
            resource.add(linkTo(CustomerEndpoint.class).slash(id.getId()).withRel("remove"));
        }
        return resource;
    }
}
