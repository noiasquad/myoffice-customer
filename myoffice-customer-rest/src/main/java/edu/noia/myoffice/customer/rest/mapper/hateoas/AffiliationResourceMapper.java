package edu.noia.myoffice.customer.rest.mapper.hateoas;

import edu.noia.myoffice.customer.domain.vo.Affiliation;
import edu.noia.myoffice.customer.rest.controller.CustomerEndpoint;
import edu.noia.myoffice.customer.rest.controller.FolderEndpoint;
import edu.noia.myoffice.customer.rest.security.Authority;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.LinkRelation;
import org.springframework.hateoas.server.core.EmbeddedWrapper;
import org.springframework.hateoas.server.core.EmbeddedWrappers;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static edu.noia.myoffice.customer.rest.security.Authority.*;
import static java.util.stream.Collectors.toList;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AffiliationResourceMapper {

    @NonNull
    Predicate<Authority> hasAuthority;

    public CollectionModel<EmbeddedWrapper> toResource(Stream<Affiliation> content) {
        EmbeddedWrappers wrappers = new EmbeddedWrappers(true);
        CollectionModel<EmbeddedWrapper> resource = new CollectionModel<>(
                content.map(p -> wrappers.wrap(toResource(p), LinkRelation.of("content"))).collect(toList()));

        if (hasAuthority.test(AFFILIATE)) {
            resource.add(linkTo(CustomerEndpoint.class).withRel("affiliate"));
        }
        return resource;
    }

    public CollectionModel<EmbeddedWrapper> toResource(List<Affiliation> content) {
        return toResource(content.stream());
    }

    public EntityModel<Affiliation> toResource(Affiliation affiliation) {
        EntityModel<Affiliation> resource = new EntityModel<>(affiliation);

        if (hasAuthority.test(VIEW_CUSTOMER)) {
            resource.add(linkTo(CustomerEndpoint.class)
                    .slash(affiliation.getCustomerId().getId())
                    .withRel("customer"));
        }
        if (hasAuthority.test(VIEW_FOLDER)) {
            resource.add(linkTo(FolderEndpoint.class)
                    .slash(affiliation.getFolderId().getId())
                    .withRel("folder"));
        }
        if (hasAuthority.test(UNAFFILIATE)) {
            resource.add(linkTo(FolderEndpoint.class)
                    .slash(affiliation.getFolderId().getId())
                    .slash("affiliates")
                    .slash(affiliation.getCustomerId().getId())
                    .withRel("remove"));
        }
        return resource;
    }
}
