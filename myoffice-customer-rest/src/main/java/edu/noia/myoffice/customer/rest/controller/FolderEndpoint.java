package edu.noia.myoffice.customer.rest.controller;

import edu.noia.myoffice.common.rest.util.IdentifiantPropertyEditorSupport;
import edu.noia.myoffice.common.util.finder.Finder;
import edu.noia.myoffice.customer.application.command.*;
import edu.noia.myoffice.customer.application.query.FolderReadRepository;
import edu.noia.myoffice.customer.application.service.ManageFolderUseCase;
import edu.noia.myoffice.customer.domain.entity.Folder;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import edu.noia.myoffice.customer.rest.mapper.hateoas.FolderResourceMapper;
import edu.noia.myoffice.customer.rest.security.Authority;
import edu.noia.myoffice.customer.rest.util.Search;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.core.EmbeddedWrapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static edu.noia.myoffice.customer.rest.mapper.FolderMapper.toDto;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping(path = "/api/customer/v1/folders")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class FolderEndpoint {

    ManageFolderUseCase folderUseCase;
    FolderReadRepository folderRepository;
    FolderResourceMapper folderResourceMapper;

    FolderEndpoint(
            ManageFolderUseCase folderUseCase,
            @Qualifier("application.folder.read.repository") FolderReadRepository folderRepository) {

        this.folderUseCase = folderUseCase;
        this.folderRepository = folderRepository;
        this.folderResourceMapper = new FolderResourceMapper(Authority.check);
    }

    @PostMapping
    public ResponseEntity<EntityModel<FolderState>> create(@RequestBody CreateFolder command) {
        Folder folder = folderUseCase.handle(command);
        return status(CREATED).body(folderResourceMapper.toResource(folder.getId(), folder.getState()));
    }

    @PutMapping("/{id}")
    public ResponseEntity<EntityModel<FolderState>> patch(
            @PathVariable("id") FolderId folderId,
            @RequestBody ModifyFolder command) {
        Folder folder = folderUseCase.handle(command.setId(folderId));
        return ok(folderResourceMapper.toResource(folder.getId(), folder.getState()));
    }

    @PutMapping("/{id}/affiliates/{customerId}")
    public ResponseEntity affiliate(
            @PathVariable("id") FolderId folderId,
            @PathVariable CustomerId affiliateId) {
        folderUseCase.handle(AffiliateCustomer.of(folderId, affiliateId));
        return status(NO_CONTENT).build();
    }

    @DeleteMapping("/{id}/affiliates/{customerId}")
    public ResponseEntity unaffiliate(
            @PathVariable("id") FolderId folderId,
            @PathVariable CustomerId affiliateId) {
        folderUseCase.handle(CancelAffiliation.of(folderId, affiliateId));
        return status(NO_CONTENT).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<EntityModel<FolderState>> findOne(@PathVariable("id") FolderId folderId) {
        return ok(folderResourceMapper.toResource(folderId,
                Finder.find(FolderState.class, folderId, folderRepository::findOne)));
    }

    @GetMapping("/ones")
    public ResponseEntity<CollectionModel<EmbeddedWrapper>> findOnes(@RequestParam FolderId[] id) {
        return ok(folderResourceMapper.toResource(
                folderRepository.findOnes(id)
                        .map(p -> Pair.with(p.getValue0(), toDto.map(p.getValue1())))));
    }

    @GetMapping
    public ResponseEntity<EntityModel<Page<EntityModel<FolderState>>>> findAll(Pageable pageable) {
        return ok(folderResourceMapper.toResource(
                folderRepository.findAll(pageable)
                        .map(p -> Pair.with(p.getValue0(), toDto.map(p.getValue1())))));
    }

    @GetMapping("/search")
    public ResponseEntity<EntityModel<Page<EntityModel<FolderState>>>> search(
            @RequestParam(required = false) String term,
            @RequestParam(required = false) String[] fields,
            @RequestParam(required = false) String[] filters,
            Pageable pageable) {

        return ok(folderResourceMapper.toResource(
                Search.execute(term, fields, filters, pageable, folderRepository)
                        .map(p -> Pair.with(p.getValue0(), toDto.map(p.getValue1())))));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") FolderId folderId) {
        folderUseCase.handle(RemoveFolder.of(folderId));
        return status(NO_CONTENT).build();
    }

    @InitBinder
    public void dataBinding(WebDataBinder binder) {
        binder.registerCustomEditor(FolderId.class,
                new IdentifiantPropertyEditorSupport<>(s -> FolderId.of(UUID.fromString(s))));

        binder.registerCustomEditor(CustomerId.class,
                new IdentifiantPropertyEditorSupport<>(s -> CustomerId.of(UUID.fromString(s))));
    }

}
