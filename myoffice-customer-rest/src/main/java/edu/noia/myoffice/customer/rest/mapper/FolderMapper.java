package edu.noia.myoffice.customer.rest.mapper;

import edu.noia.myoffice.common.util.mapper.Mapper;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.rest.dto.FolderDto;

public interface FolderMapper {

    // Optimized edu.noia.myoffice.customer.edu.noia.myoffice.customer.domain to dto mapping thank to the use of interfaces
    Mapper<FolderState, FolderState> toDto = state -> state;

    // Optimized dto to edu.noia.myoffice.customer.edu.noia.myoffice.customer.domain mapping thank to the use of interfaces
    Mapper<FolderDto, FolderState> toDomainEntity = state -> state;

    // Dto to JSON mapping delegated to Jackson library
    // JSON to dto mapping delegated to Jackson library
}
