package edu.noia.myoffice.customer.rest.security;

import java.util.function.Predicate;

public enum Authority {
    ADMIN, USER,
    VIEW_CUSTOMER, MODIFY_CUSTOMER, CREATE_CUSTOMER, REMOVE_CUSTOMER,
    VIEW_FOLDER, MODIFY_FOLDER, CREATE_FOLDER, REMOVE_FOLDER,
    AFFILIATE, UNAFFILIATE;

    public static final Predicate<Authority> check = Authority::check;

    public boolean check() {
        return true;
    }
}
