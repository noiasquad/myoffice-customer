package edu.noia.myoffice.customer.rest.dto;

import edu.noia.myoffice.address.domain.vo.Address;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.vo.Profile;
import edu.noia.myoffice.person.domain.vo.Person;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public final class CustomerDto implements CustomerState {

    Person person;
    List<Address> addresses = new ArrayList<>();
    Profile profile;
    String notes;
}
