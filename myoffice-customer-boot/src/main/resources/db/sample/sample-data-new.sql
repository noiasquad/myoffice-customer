INSERT INTO t_customer (pk_id, id, salutation, first_name, last_name)
VALUES (2, 'a3708a6d-66d4-460a-8e5b-912bfbeb80cd', 'Mister', 'John', 'DOE');
INSERT INTO t_customer_address (fk_customer, type, a1, a2, a3, a4, a5, a6)
VALUES (2, 'POSTAL', 'Rue du test 99', '', '9000', 'Test', 'NE', 'Suisse');
INSERT INTO t_customer_address (fk_customer, type, a1, a2)
VALUES (2, 'PHONE', '032 999 99 99', 'PRIVATE');
INSERT INTO t_customer_address (fk_customer, type, a1, a2)
VALUES (2, 'PHONE', '032 999 99 99', 'FAX');
INSERT INTO t_customer_address (fk_customer, type, a1, a2)
VALUES (2, 'EMAIL', 'info@test.ch', 'OFFICE');
INSERT INTO t_customer_address (fk_customer, type, a1, a2)
VALUES (2, 'EMAIL', 'test@test.ch', 'PRIVATE');
INSERT INTO t_customer_address (fk_customer, type, a1)
VALUES (2, 'WEB', 'www.test.ch');

INSERT INTO t_folder (pk_id, id, name)
VALUES (1, 'a3708a6d-66d4-460a-8e5b-912bfbeb80cd', 'test');
