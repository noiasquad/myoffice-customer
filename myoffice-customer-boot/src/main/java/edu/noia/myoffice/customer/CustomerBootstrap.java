package edu.noia.myoffice.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(CustomerBootstrap.class, args);
    }
}
