package edu.noia.myoffice.customer.config;

import edu.noia.myoffice.person.data.rest.adapter.com.genderapi.GenderApiClientConfiguration;
import edu.noia.myoffice.person.rest.controller.PersonGenderEndpoint;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

@Profile("gender_api")
@ComponentScan(basePackageClasses = PersonGenderEndpoint.class)
@Import(value = GenderApiClientConfiguration.class)
@Configuration
public class PersonGenderConfiguration {
}
