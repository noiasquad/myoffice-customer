package edu.noia.myoffice.customer.config;

import edu.noia.myoffice.common.domain.event.EventPublisher;
import edu.noia.myoffice.customer.domain.repository.CustomerRepository;
import edu.noia.myoffice.customer.domain.repository.FolderRepository;
import edu.noia.myoffice.customer.domain.service.CustomerService;
import edu.noia.myoffice.customer.domain.service.FolderService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomerDomainConfiguration {

    @Bean
    public CustomerService customerDomainService(CustomerRepository customerRepository,
                                                 FolderRepository folderRepository,
                                                 EventPublisher eventPublisher) {
        return new CustomerService(customerRepository, folderRepository, eventPublisher);
    }

    @Bean
    public FolderService folderDomainService(CustomerRepository customerRepository,
                                             FolderRepository folderRepository,
                                             EventPublisher eventPublisher) {
        return new FolderService(customerRepository, folderRepository, eventPublisher);
    }
}
