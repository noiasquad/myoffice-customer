package edu.noia.myoffice.customer.event.listener;

import edu.noia.myoffice.common.domain.event.DomainEvent;
import edu.noia.myoffice.common.event.store.EventStore;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;

import java.time.Instant;
import java.util.function.Consumer;

@RequiredArgsConstructor
public class DomainEventListener implements Consumer<DomainEvent> {

    @NonNull
    private EventStore eventStore;

    @EventListener(value = {DomainEvent.class})
    public void accept(DomainEvent event) {
        eventStore.register(event, Instant.now());
    }
}
