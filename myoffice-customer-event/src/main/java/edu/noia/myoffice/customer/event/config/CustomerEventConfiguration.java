package edu.noia.myoffice.customer.event.config;

import edu.noia.myoffice.common.domain.event.EventPublisher;
import edu.noia.myoffice.common.event.adapter.jpa.EventStoreJpaAdapter;
import edu.noia.myoffice.common.event.adapter.jpa.JpaEventPublication;
import edu.noia.myoffice.common.event.adapter.jpa.JpaEventStore;
import edu.noia.myoffice.common.event.store.EventStore;
import edu.noia.myoffice.customer.event.adapter.EventPublisherAdapter;
import edu.noia.myoffice.customer.event.listener.DomainEventListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Slf4j
@EnableJpaRepositories(basePackageClasses = {JpaEventStore.class})
@EntityScan(basePackageClasses = {JpaEventPublication.class})
@Configuration
public class CustomerEventConfiguration {

    @Bean
    public EventPublisher domainEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        return new EventPublisherAdapter(applicationEventPublisher);
    }

    @Bean
    public EventStore domainEventStore(JpaEventStore repository) {
        return new EventStoreJpaAdapter(repository);
    }

    @Bean
    public DomainEventListener domainEventListener(EventStore eventStore) {
        return new DomainEventListener(eventStore);
    }
}
