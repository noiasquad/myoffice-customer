package edu.noia.myoffice.customer.batch.job;

import edu.noia.myoffice.customer.application.command.SanitizeCustomer;
import edu.noia.myoffice.customer.application.query.CustomerReadRepository;
import edu.noia.myoffice.customer.application.service.SanitizeCustomerUseCase;
import edu.noia.myoffice.customer.domain.entity.Customer;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SanityChunk {

    @NonNull
    CustomerReadRepository repository;
    @NonNull
    SanitizeCustomerUseCase usecase;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Optional<Pageable> execute(Pageable pageable) {
        Page<Customer> page = repository.findAll(pageable).map(p -> Customer.ofValid(p.getValue0(), p.getValue1()));
        page.forEach(customer -> usecase.handle(SanitizeCustomer.of(customer)));
        return page.isLast() ? Optional.empty() : pageable.next().toOptional();
    }
}
