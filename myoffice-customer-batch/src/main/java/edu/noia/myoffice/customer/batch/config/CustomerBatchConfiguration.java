package edu.noia.myoffice.customer.batch.config;

import edu.noia.myoffice.customer.application.query.CustomerReadRepository;
import edu.noia.myoffice.customer.application.service.SanitizeCustomerUseCase;
import edu.noia.myoffice.customer.batch.job.SanityChunk;
import edu.noia.myoffice.customer.batch.job.SanityJob;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomerBatchConfiguration {

    @Bean
    public SanityJob sanityJob(
            @Qualifier("application.customer.read.repository") CustomerReadRepository repository,
            SanitizeCustomerUseCase useCase) {
        return new SanityJob(new SanityChunk(repository, useCase));
    }

}