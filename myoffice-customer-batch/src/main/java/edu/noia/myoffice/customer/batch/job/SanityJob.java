package edu.noia.myoffice.customer.batch.job;

import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;

@Slf4j
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SanityJob {

    @NonNull
    SanityChunk chunk;

    @Async("batchingTaskExecutor")
    public void execute() {
        LOG.debug("Execution of foldering job has been started");
        Pageable pageable = PageRequest.of(0, 10);
        do {
            LOG.debug("Execution of next foldering job chunk is on going: {}", pageable.toString());
            pageable = chunk.execute(pageable).orElse(null);
        }
        while (pageable != null);
        LOG.debug("Execution of foldering job is terminated");
    }

}
