package data.test.util;

import edu.noia.myoffice.address.domain.vo.EmailAddress;
import edu.noia.myoffice.address.domain.vo.PhoneAddress;
import edu.noia.myoffice.address.domain.vo.PostalAddress;
import edu.noia.myoffice.address.domain.vo.WebAddress;
import edu.noia.myoffice.customer.domain.entity.Customer;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.CustomerSample;
import edu.noia.myoffice.customer.domain.vo.Profile;
import edu.noia.myoffice.person.domain.vo.Gender;
import edu.noia.myoffice.person.domain.vo.Person;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TestCustomer {

    private static RandomString randomString8 = new RandomString(10, ThreadLocalRandom.current());

    public static CustomerState randomValid() {
        return CustomerSample.of(randomPerson())
                .setAddresses(Arrays.asList(
                        randomPostalAddress(),
                        randomSwissPhoneNumber(),
                        randomEmailAddress(),
                        randomWebsiteUrl()))
                .setProfile(Profile.of(false, false, false))
                .setNotes(randomString8.nextString());
    }

    public static Customer random() {
        return Customer.ofValid(CustomerId.random(), randomValid());
    }

    private static Person randomPerson() {
        return Person.of(
                randomString8.nextString(),
                randomString8.nextString(),
                Gender.MALE,
                randomString8.nextString(),
                LocalDate.now().minusYears(randomInt(80)));
    }

    private static PostalAddress randomPostalAddress() {
        return new PostalAddress()
                .setStreet(randomString8.nextString())
                .setHouseNbr(String.valueOf(randomInt(10)))
                .setZip(randomString8.nextString())
                .setCity(randomString8.nextString())
                .setRegion(randomString8.nextString())
                .setCountry(randomString8.nextString());
    }

    private static PhoneAddress randomSwissPhoneNumber() {
        return new PhoneAddress().setNumber(String.format("+4179%d", randomInt(1000000, 9999999)));
    }

    private static EmailAddress randomEmailAddress() {
        return new EmailAddress().setEmail(String.format("%s@%s.com", randomString8.nextString(), randomString8.nextString()));
    }

    private static WebAddress randomWebsiteUrl() {
        return new WebAddress().setUrl(String.format("http://www.%s.com", randomString8.nextString()));
    }

    private static int randomInt(int limit) {
        return ThreadLocalRandom.current().nextInt(limit);
    }

    private static int randomInt(int from, int to) {
        return ThreadLocalRandom.current().nextInt(from, to);
    }
}
