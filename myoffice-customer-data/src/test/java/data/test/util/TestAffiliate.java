package data.test.util;

import edu.noia.myoffice.customer.domain.vo.CustomerId;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TestAffiliate {

    public static CustomerId random() {
        return CustomerId.of(UUID.randomUUID());
    }
}
