package data.test.config;

import edu.noia.myoffice.customer.data.jpa.entity.JpaCustomerState;
import edu.noia.myoffice.customer.data.jpa.entity.JpaFolderState;
import edu.noia.myoffice.customer.data.jpa.hibernate.converter.CustomerIdConverter;
import edu.noia.myoffice.customer.data.jpa.hibernate.converter.FolderIdConverter;
import edu.noia.myoffice.customer.data.jpa.hibernate.type.JpaProfileType;
import edu.noia.myoffice.customer.data.jpa.repository.JpaCustomerStateRepository;
import edu.noia.myoffice.customer.data.jpa.repository.JpaFolderStateRepository;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(
        basePackageClasses = {
                JpaCustomerStateRepository.class,
                JpaFolderStateRepository.class
        }
)
@EntityScan(
        basePackageClasses = {
                JpaCustomerState.class,
                JpaFolderState.class,
                CustomerIdConverter.class,
                FolderIdConverter.class,
                JpaProfileType.class
        }
)
@Configuration
public class ConfigurationIT {
}
