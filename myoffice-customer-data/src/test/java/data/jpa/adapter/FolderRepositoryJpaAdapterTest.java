package data.jpa.adapter;

import data.test.util.TestFolder;
import edu.noia.myoffice.customer.data.jpa.adapter.FolderRepositoryJpaAdapter;
import edu.noia.myoffice.customer.data.jpa.entity.JpaFolderState;
import edu.noia.myoffice.customer.data.jpa.repository.JpaFolderStateRepository;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class FolderRepositoryJpaAdapterTest {

    @Mock
    private JpaFolderStateRepository jpaRepository;
    @InjectMocks
    private FolderRepositoryJpaAdapter repositoryAdapter;

    @Test
    public void put_should_call_jpa_persistence() {
        // Given
        FolderId anyId = FolderId.random();
        FolderState anyState = TestFolder.randomValid();
        given(jpaRepository.save(any(JpaFolderState.class))).willAnswer(invocation -> invocation.getArgument(0));
        // When
        repositoryAdapter.put(anyId, anyState);
        // Then
        verify(jpaRepository).save(any(JpaFolderState.class));
    }

}
