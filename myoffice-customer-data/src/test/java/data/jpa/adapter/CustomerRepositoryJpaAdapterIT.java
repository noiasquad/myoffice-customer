package data.jpa.adapter;

import edu.noia.myoffice.customer.data.jpa.adapter.CustomerRepositoryJpaAdapter;
import edu.noia.myoffice.customer.data.jpa.entity.JpaCustomerState;
import edu.noia.myoffice.customer.data.jpa.repository.JpaCustomerStateRepository;
import data.test.config.ConfigurationIT;
import data.test.util.TestCustomer;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ConfigurationIT.class})
@DataJpaTest
public class CustomerRepositoryJpaAdapterIT {

    private CustomerRepositoryJpaAdapter repositoryAdapter;
    @Autowired
    private JpaCustomerStateRepository jpaRepository;
    @PersistenceContext
    private EntityManager em;

    @Before
    public void setup() {
        repositoryAdapter = new CustomerRepositoryJpaAdapter(jpaRepository);
    }

    @Test
    public void put_should_persist_the_customer() {
        // Given
        CustomerId anyId = CustomerId.random();
        CustomerState anyState = TestCustomer.randomValid();
        // When
        repositoryAdapter.put(anyId, anyState);
        flushClear();
        // Then
        JpaCustomerState jpaCustomer = jpaRepository.findById(anyId).orElse(null);
        assertThat(jpaCustomer).isNotNull();
        assertThat(jpaCustomer.getPerson()).isEqualToComparingOnlyGivenFields(anyState.getPerson(),
                "lastName");
        assertThat(jpaCustomer.getAddresses().size()).isEqualTo(anyState.getAddresses().size());
        assertThat(jpaCustomer.getAddresses())
                .usingElementComparatorOnFields("type")
                .containsOnlyElementsOf(anyState.getAddresses());
    }

    private void flushClear() {
        em.flush();
        em.clear();
    }
}
