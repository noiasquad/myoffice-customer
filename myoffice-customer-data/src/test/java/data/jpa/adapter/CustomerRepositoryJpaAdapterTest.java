package data.jpa.adapter;

import edu.noia.myoffice.customer.data.jpa.adapter.CustomerRepositoryJpaAdapter;
import edu.noia.myoffice.customer.data.jpa.entity.JpaCustomerState;
import edu.noia.myoffice.customer.data.jpa.repository.JpaCustomerStateRepository;
import data.test.util.TestCustomer;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CustomerRepositoryJpaAdapterTest {

    @Mock
    private JpaCustomerStateRepository jpaRepository;
    @InjectMocks
    private CustomerRepositoryJpaAdapter repositoryAdapter;

    @Test
    public void put_should_call_jpa_persistence() {
        // Given
        CustomerId anyId = CustomerId.random();
        CustomerState anyState = TestCustomer.randomValid();
        given(jpaRepository.save(any(JpaCustomerState.class)))
                .willAnswer(invocation -> invocation.getArgument(0));
        // When
        repositoryAdapter.put(anyId, anyState);
        // Then
        verify(jpaRepository).save(any(JpaCustomerState.class));
    }

}
