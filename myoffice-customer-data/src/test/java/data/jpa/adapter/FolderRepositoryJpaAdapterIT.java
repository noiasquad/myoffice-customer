package data.jpa.adapter;

import data.test.config.ConfigurationIT;
import data.test.util.TestAffiliate;
import data.test.util.TestFolder;
import edu.noia.myoffice.customer.data.jpa.adapter.FolderRepositoryJpaAdapter;
import edu.noia.myoffice.customer.data.jpa.entity.JpaFolderState;
import edu.noia.myoffice.customer.data.jpa.repository.JpaFolderStateRepository;
import edu.noia.myoffice.customer.domain.entity.Folder;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.vo.Affiliation;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ConfigurationIT.class})
@DataJpaTest
public class FolderRepositoryJpaAdapterIT {

    private FolderRepositoryJpaAdapter repositoryAdapter;
    @Autowired
    private JpaFolderStateRepository jpaRepository;
    @PersistenceContext
    private EntityManager em;

    @Before
    public void setup() {
        repositoryAdapter = new FolderRepositoryJpaAdapter(jpaRepository);
    }

    @Test
    public void put_should_persist_the_folder_state() {
        // Given
        FolderId anyId = FolderId.random();
        FolderState anyState = TestFolder.randomValid();
        // When
        repositoryAdapter.put(anyId, anyState);
        flushClear();
        // Then
        JpaFolderState jpaFolder = jpaRepository.findById(anyId).orElse(null);
        assertThat(jpaFolder).isNotNull();
        assertThat(jpaFolder).isEqualToComparingOnlyGivenFields(anyState, "name", "notes");
    }

    @Test
    public void put_should_persist_the_folder_state_with_two_affiliates() {
        // Given
        CustomerId affiliate1 = TestAffiliate.random();
        CustomerId affiliate2 = TestAffiliate.random();
        FolderId anyId = FolderId.random();
        FolderState anyState = TestFolder.randomValid().addAffiliate(affiliate1).addAffiliate(affiliate2);
        // When
        repositoryAdapter.put(anyId, anyState);
        flushClear();
        // Then
        JpaFolderState jpaFolder = jpaRepository.findById(anyId).orElse(null);
        assertThat(jpaFolder).isNotNull();
        assertThat(jpaFolder).isEqualToComparingOnlyGivenFields(anyState, "name", "notes");
        assertThat(jpaFolder.getAffiliates()).containsExactlyInAnyOrder(affiliate1, affiliate2);
    }

    @Test
    public void find_by_customer_should_return_the_right_affiliations() {
        // Given
        new FolderRepositoryJpaAdapter(jpaRepository);
        CustomerId customerId = CustomerId.random();
        Folder anyFolder1 = TestFolder.random(customerId);
        Folder anyFolder2 = TestFolder.random(customerId);
        // When
        Folder folder1 = anyFolder1.persist(repositoryAdapter);
        Folder folder2 = anyFolder2.persist(repositoryAdapter);
        flushClear();
        // Then
        Stream<Affiliation> affiliations = repositoryAdapter.findAffiliationsById(customerId);
        assertThat(affiliations
                .map(Affiliation::getFolderId)
                .collect(toList()))
                .containsExactlyInAnyOrder(folder1.getId(), folder2.getId());
    }

    private void flushClear() {
        em.flush();
        em.clear();
    }
}
