package edu.noia.myoffice.customer.data.jpa.entity;

import edu.noia.myoffice.person.domain.vo.Gender;
import edu.noia.myoffice.person.domain.vo.Person;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;

@Getter
@Setter
@Accessors(chain = true)
@Embeddable
public class JpaPerson {

    String firstName;
    String lastName;
    @Enumerated(EnumType.STRING)
    Gender gender;
    String salutation;
    LocalDate birthDate;

    public static JpaPerson from(Person person) {
        return new JpaPerson()
                .setFirstName(person.getFirstName())
                .setLastName(person.getLastName())
                .setSalutation(person.getSalutation())
                .setBirthDate(person.getBirthDate())
                .setGender(person.getGender());
    }
}
