package edu.noia.myoffice.customer.data.jpa.entity;

import edu.noia.myoffice.address.domain.vo.Address;
import edu.noia.myoffice.common.data.jpa.JpaBaseEntity;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.Profile;
import edu.noia.myoffice.person.domain.vo.Person;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "customer")
@Entity
@EqualsAndHashCode(of = "id", callSuper = false)
@Accessors(chain = true)
@Getter
@Setter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JpaCustomerState extends JpaBaseEntity implements CustomerState {

    CustomerId id;

    @Embedded
    JpaPerson person;

    @Type(type = "edu.noia.myoffice.customer.data.jpa.hibernate.type.JpaAddressType")
    @Columns(columns = {
            @Column(name = "type"),
            @Column(name = "tags"),
            @Column(name = "a1"),
            @Column(name = "a2"),
            @Column(name = "a3"),
            @Column(name = "a4"),
            @Column(name = "a5"),
            @Column(name = "a6")
    })
    @ElementCollection
    @CollectionTable(name = "customer_address", joinColumns = @JoinColumn(name = "fk_customer"))
    List<Address> addresses = new ArrayList<>();

    @Type(type = "edu.noia.myoffice.customer.data.jpa.hibernate.type.JpaProfileType")
    @Columns(columns = {
            @Column(name = "hasMoved"),
            @Column(name = "isSubcontractor"),
            @Column(name = "sendInvitation")
    })
    Profile profile;
    String notes;

    public static JpaCustomerState of(CustomerState state) {
        return (JpaCustomerState) new JpaCustomerState().modify(state);
    }

    @Override
    public Person getPerson() {
        return Person.of(
                person.getFirstName(),
                person.getLastName(),
                person.getGender(),
                person.getSalutation(),
                person.getBirthDate());
    }

    @Override
    public JpaCustomerState setPerson(Person person) {
        this.person = JpaPerson.from(person);
        return this;
    }
}
