package edu.noia.myoffice.customer.data.jpa.mapper;

import edu.noia.myoffice.common.util.mapper.Mapper;
import edu.noia.myoffice.customer.data.jpa.entity.JpaFolderState;
import edu.noia.myoffice.customer.domain.entity.FolderState;

public interface FolderMapper {

    // Optimized mapping thank to the use of interfaces
    Mapper<FolderState, JpaFolderState> toJpaEntity = state ->
            state instanceof JpaFolderState ? (JpaFolderState) state : JpaFolderState.of(state);

    // Optimized mapping thank to the use of interfaces
    Mapper<JpaFolderState, FolderState> toDomainEntity = state -> state;
}
