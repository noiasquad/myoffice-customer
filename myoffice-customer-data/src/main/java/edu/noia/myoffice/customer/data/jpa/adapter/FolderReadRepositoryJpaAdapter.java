package edu.noia.myoffice.customer.data.jpa.adapter;

import edu.noia.myoffice.customer.data.jpa.repository.JpaFolderStateRepository;
import edu.noia.myoffice.customer.application.query.FolderReadRepository;

public class FolderReadRepositoryJpaAdapter
        extends BaseFolderRepositoryJpaAdapter
        implements FolderReadRepository {

    public FolderReadRepositoryJpaAdapter(JpaFolderStateRepository repository) {
        super(repository);
    }
}
