package edu.noia.myoffice.customer.data.jpa.config;

import edu.noia.myoffice.customer.application.query.CustomerReadRepository;
import edu.noia.myoffice.customer.application.query.FolderReadRepository;
import edu.noia.myoffice.customer.data.jpa.adapter.CustomerReadRepositoryJpaAdapter;
import edu.noia.myoffice.customer.data.jpa.adapter.CustomerRepositoryJpaAdapter;
import edu.noia.myoffice.customer.data.jpa.adapter.FolderReadRepositoryJpaAdapter;
import edu.noia.myoffice.customer.data.jpa.adapter.FolderRepositoryJpaAdapter;
import edu.noia.myoffice.customer.data.jpa.entity.JpaCustomerState;
import edu.noia.myoffice.customer.data.jpa.entity.JpaFolderState;
import edu.noia.myoffice.customer.data.jpa.hibernate.converter.CustomerIdConverter;
import edu.noia.myoffice.customer.data.jpa.hibernate.converter.FolderIdConverter;
import edu.noia.myoffice.customer.data.jpa.repository.JpaCustomerStateRepository;
import edu.noia.myoffice.customer.data.jpa.repository.JpaFolderStateRepository;
import edu.noia.myoffice.customer.domain.repository.CustomerRepository;
import edu.noia.myoffice.customer.domain.repository.FolderRepository;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Profile("db_jpa")
@EnableJpaRepositories(
        basePackageClasses = {
                JpaCustomerStateRepository.class,
                JpaFolderStateRepository.class
        }
)
@EntityScan(
        basePackageClasses = {
                JpaCustomerState.class,
                JpaFolderState.class,
                CustomerIdConverter.class,
                FolderIdConverter.class
        }
)
@Configuration
public class CustomerDataJpaConfiguration {

    @Bean("data.folder.repository")
    public FolderRepository folderRepository(JpaFolderStateRepository repository) {
        return new FolderRepositoryJpaAdapter(repository);
    }

    @Bean("data.folder.read.repository")
    public FolderReadRepository folderReadRepository(JpaFolderStateRepository repository) {
        return new FolderReadRepositoryJpaAdapter(repository);
    }

    @Bean("data.customer.repository")
    public CustomerRepository customerRepository(JpaCustomerStateRepository repository) {
        return new CustomerRepositoryJpaAdapter(repository);
    }

    @Bean("data.customer.read.repository")
    public CustomerReadRepository customerReadRepository(JpaCustomerStateRepository repository) {
        return new CustomerReadRepositoryJpaAdapter(repository);
    }
}
