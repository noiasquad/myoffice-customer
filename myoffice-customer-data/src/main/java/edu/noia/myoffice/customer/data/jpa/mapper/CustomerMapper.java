package edu.noia.myoffice.customer.data.jpa.mapper;

import edu.noia.myoffice.common.util.mapper.Mapper;
import edu.noia.myoffice.customer.data.jpa.entity.JpaCustomerState;
import edu.noia.myoffice.customer.domain.entity.CustomerState;

public interface CustomerMapper {

    // Optimized mapping thank to the use of interfaces
    Mapper<CustomerState, JpaCustomerState> toJpaEntity = state ->
            state instanceof JpaCustomerState ? (JpaCustomerState) state : JpaCustomerState.of(state);

    // Optimized mapping thank to the use of interfaces
    Mapper<JpaCustomerState, CustomerState> toDomainEntity = state -> state;
}
