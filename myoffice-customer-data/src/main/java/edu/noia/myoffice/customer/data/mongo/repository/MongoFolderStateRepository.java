package edu.noia.myoffice.customer.data.mongo.repository;

import edu.noia.myoffice.customer.data.mongo.document.DocFolderState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;
import java.util.stream.Stream;

public interface MongoFolderStateRepository extends MongoRepository<DocFolderState, FolderId> {

    Optional<DocFolderState> findById(FolderId id);

    Stream<DocFolderState> findAllByIdIsIn(FolderId... id);

    Stream<DocFolderState> findAllByAffiliatesContaining(CustomerId affiliate);
}
