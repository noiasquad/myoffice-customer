package edu.noia.myoffice.customer.data.mongo.repository;

import edu.noia.myoffice.customer.data.mongo.document.DocCustomerState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;
import java.util.stream.Stream;

public interface MongoCustomerStateRepository extends MongoRepository<DocCustomerState, CustomerId> {

    Optional<DocCustomerState> findById(CustomerId id);

    Stream<DocCustomerState> findAllByIdIsIn(CustomerId... id);
}
