package edu.noia.myoffice.customer.data.mongo.mapper;

import edu.noia.myoffice.common.util.mapper.Mapper;
import edu.noia.myoffice.customer.data.mongo.document.DocFolderState;
import edu.noia.myoffice.customer.domain.entity.FolderState;

public interface FolderMapper {

    // Optimized mapping thank to the use of interfaces
    Mapper<FolderState, DocFolderState> toDocument = state ->
            state instanceof DocFolderState ? (DocFolderState) state : DocFolderState.of(state);

    // Optimized mapping thank to the use of interfaces
    Mapper<DocFolderState, FolderState> toDomainEntity = state -> state;
}
