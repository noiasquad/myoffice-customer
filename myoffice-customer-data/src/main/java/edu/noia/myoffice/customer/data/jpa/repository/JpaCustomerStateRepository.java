package edu.noia.myoffice.customer.data.jpa.repository;

import edu.noia.myoffice.customer.data.jpa.entity.JpaCustomerState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.Optional;
import java.util.stream.Stream;

public interface JpaCustomerStateRepository extends
        PagingAndSortingRepository<JpaCustomerState, Long>,
        QueryByExampleExecutor<JpaCustomerState> {

    Optional<JpaCustomerState> findById(CustomerId id);

    Stream<JpaCustomerState> findAllByIdIsIn(CustomerId... ids);
}
