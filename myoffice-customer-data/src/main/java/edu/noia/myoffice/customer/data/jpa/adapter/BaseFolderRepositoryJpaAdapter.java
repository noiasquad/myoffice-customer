package edu.noia.myoffice.customer.data.jpa.adapter;

import edu.noia.myoffice.common.data.matcher.FindCriteriaMatcher;
import edu.noia.myoffice.common.util.search.FindCriteria;
import edu.noia.myoffice.customer.data.jpa.entity.JpaFolderState;
import edu.noia.myoffice.customer.data.jpa.mapper.FolderMapper;
import edu.noia.myoffice.customer.data.jpa.repository.JpaFolderStateRepository;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.vo.Affiliation;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import org.javatuples.Pair;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;
import static org.springframework.data.domain.ExampleMatcher.matching;

@AllArgsConstructor
abstract class BaseFolderRepositoryJpaAdapter {

    @NonNull
    protected JpaFolderStateRepository repository;

    public Optional<FolderState> findOne(FolderId id) {
        return repository.findById(id).map(FolderMapper.toDomainEntity::map);
    }

    public Stream<Pair<FolderId, FolderState>> findOnes(FolderId... ids) {
        return repository.findAllByIdIsIn(ids).map(e -> Pair.with(e.getId(), FolderMapper.toDomainEntity.map(e)));
    }

    public Page<Pair<FolderId, FolderState>> findAll(Pageable pageable) {
        return repository.findAll(pageable).map(e -> Pair.with(e.getId(), FolderMapper.toDomainEntity.map(e)));
    }

    public Stream<Pair<FolderId, FolderState>> findSome(List<FindCriteria> criteria) {
        return (criteria.isEmpty() ?
                StreamSupport.stream(repository.findAll().spliterator(), false) :
                StreamSupport.stream(repository.findAll(toExample(criteria)).spliterator(), false))
                .map(e -> Pair.with(e.getId(), FolderMapper.toDomainEntity.map(e)));
    }

    public Page<Pair<FolderId, FolderState>> findSome(List<FindCriteria> criteria, Pageable pageable) {
        return (criteria.isEmpty() ?
                repository.findAll(pageable) :
                repository.findAll(toExample(criteria), pageable))
                .map(e -> Pair.with(e.getId(), FolderMapper.toDomainEntity.map(e)));
    }

    public Page<Pair<FolderId, FolderState>> search(Object value, Pageable pageable) {
        final String valueAsString = value.toString();
        return repository.findAll(
                Example.of(
                        new JpaFolderState()
                                .setName(valueAsString)
                                .setNotes(valueAsString),
                        ExampleMatcher.matchingAny()
                                .withMatcher("name", contains().ignoreCase())
                                .withMatcher("notes", contains().ignoreCase())), pageable)
                .map(e -> Pair.with(e.getId(), FolderMapper.toDomainEntity.map(e)));
    }

    public Stream<Affiliation> findAffiliationsById(CustomerId customerId) {
        return repository.findAllByAffiliatesContaining(customerId)
                .map(e -> Affiliation.of(e.getId(), customerId));
    }

    public Stream<Pair<FolderId, FolderState>> findByCustomerId(CustomerId customerId) {
        return repository.findAllByAffiliatesContaining(customerId)
                .map(e -> Pair.with(e.getId(), FolderMapper.toDomainEntity.map(e)));
    }

    private Example<JpaFolderState> toExample(List<FindCriteria> criteria) {
        return Example.of(
                criteria.stream().reduce(new JpaFolderState(), this::toExample, (a, b) -> a),
                criteria.stream().reduce(matching(), FindCriteriaMatcher::toMatcher, (a, b) -> a));
    }

    private JpaFolderState toExample(JpaFolderState origin, FindCriteria criteria) {
        if ("name".equalsIgnoreCase(criteria.getKey())) {
            origin.setName(criteria.getValue().toString());
        } else if ("notes".equalsIgnoreCase(criteria.getKey())) {
            origin.setNotes(criteria.getValue().toString());
        }
        return origin;
    }
}
