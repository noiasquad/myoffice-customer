package edu.noia.myoffice.customer.data.jpa.repository;

import edu.noia.myoffice.customer.data.jpa.entity.JpaFolderState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.Optional;
import java.util.stream.Stream;

public interface JpaFolderStateRepository extends
        PagingAndSortingRepository<JpaFolderState, Long>,
        QueryByExampleExecutor<JpaFolderState> {

    Optional<JpaFolderState> findById(FolderId id);

    Stream<JpaFolderState> findAllByIdIsIn(FolderId... id);

    Stream<JpaFolderState> findAllByAffiliatesContaining(CustomerId affiliate);
}
