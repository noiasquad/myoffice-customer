package edu.noia.myoffice.customer.data.jpa.adapter;

import edu.noia.myoffice.customer.application.query.CustomerReadRepository;
import edu.noia.myoffice.customer.data.jpa.repository.JpaCustomerStateRepository;

public class CustomerReadRepositoryJpaAdapter
        extends BaseCustomerRepositoryJpaAdapter
        implements CustomerReadRepository {

    public CustomerReadRepositoryJpaAdapter(JpaCustomerStateRepository repository) {
        super(repository);
    }
}
