package edu.noia.myoffice.customer.data.mongo.document;

import edu.noia.myoffice.address.domain.vo.Address;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.Profile;
import edu.noia.myoffice.person.domain.vo.Person;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document("customer")
@Accessors(chain = true)
@Data
public class DocCustomerState implements CustomerState {

    @Id
    CustomerId id;
    Person person;
    List<Address> addresses = new ArrayList<>();
    Profile profile;
    String notes;

    public static DocCustomerState of(CustomerState state) {
        return (DocCustomerState) new DocCustomerState().modify(state);
    }
}
