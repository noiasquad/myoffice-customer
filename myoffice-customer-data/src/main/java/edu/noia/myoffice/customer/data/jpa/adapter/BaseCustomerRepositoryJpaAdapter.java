package edu.noia.myoffice.customer.data.jpa.adapter;

import edu.noia.myoffice.customer.data.jpa.mapper.CustomerMapper;
import edu.noia.myoffice.address.domain.vo.PostalAddress;
import edu.noia.myoffice.common.data.matcher.FindCriteriaMatcher;
import edu.noia.myoffice.common.util.search.FindCriteria;
import edu.noia.myoffice.customer.data.jpa.entity.JpaCustomerState;
import edu.noia.myoffice.customer.data.jpa.repository.JpaCustomerStateRepository;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.person.domain.vo.Person;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import org.javatuples.Pair;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;
import static org.springframework.data.domain.ExampleMatcher.matching;

@AllArgsConstructor
@FieldDefaults(makeFinal = true)
abstract class BaseCustomerRepositoryJpaAdapter {

    @NonNull
    protected JpaCustomerStateRepository repository;

    public Optional<CustomerState> findOne(CustomerId id) {
        return repository.findById(id).map(CustomerMapper.toDomainEntity::map);
    }

    public Stream<Pair<CustomerId, CustomerState>> findOnes(CustomerId... ids) {
        return repository.findAllByIdIsIn(ids).map(e -> Pair.with(e.getId(), CustomerMapper.toDomainEntity.map(e)));
    }

    public Page<Pair<CustomerId, CustomerState>> findAll(Pageable pageable) {
        return repository.findAll(pageable).map(e -> Pair.with(e.getId(), CustomerMapper.toDomainEntity.map(e)));
    }

    public Stream<Pair<CustomerId, CustomerState>> findSome(List<FindCriteria> criteria) {
        return (criteria.isEmpty() ?
                StreamSupport.stream(repository.findAll().spliterator(), false) :
                StreamSupport.stream(repository.findAll(toExample(criteria)).spliterator(), false))
                .map(e -> Pair.with(e.getId(), CustomerMapper.toDomainEntity.map(e)));
    }

    public Page<Pair<CustomerId, CustomerState>> findSome(List<FindCriteria> criteria, Pageable pageable) {
        return (criteria.isEmpty() ?
                repository.findAll(pageable) :
                repository.findAll(toExample(criteria), pageable))
                .map(e -> Pair.with(e.getId(), CustomerMapper.toDomainEntity.map(e)));
    }

    public Page<Pair<CustomerId, CustomerState>> search(Object value, Pageable pageable) {
        final String valueAsString = value.toString();
        return repository.findAll(
                Example.of(
                        new JpaCustomerState()
                                .setPerson(new Person()
                                        .setFirstName(valueAsString)
                                        .setLastName(valueAsString))
                                .setAddresses(Arrays.asList(new PostalAddress()
                                        .setCity(valueAsString)
                                        .setStreet(valueAsString)))
                                .setNotes(valueAsString),
                        ExampleMatcher.matchingAny()
                                .withMatcher("person.firstName", contains().ignoreCase())
                                .withMatcher("person.lastName", contains().ignoreCase())
                                .withMatcher("notes", contains().ignoreCase())), pageable)
                .map(e -> Pair.with(e.getId(), CustomerMapper.toDomainEntity.map(e)));
    }

    private Example<JpaCustomerState> toExample(List<FindCriteria> criteria) {
        return Example.of(
                criteria.stream().reduce(new JpaCustomerState(), this::toExample, (a, b) -> a),
                criteria.stream().reduce(matching(), FindCriteriaMatcher::toMatcher, (a, b) -> a));
    }

    private JpaCustomerState toExample(JpaCustomerState origin, FindCriteria criteria) {
        if ("person.firstName".equalsIgnoreCase(criteria.getKey())) {
            origin.getPerson().setFirstName(criteria.getValue().toString());
        } else if ("person.lastName".equalsIgnoreCase(criteria.getKey())) {
            origin.getPerson().setLastName(criteria.getValue().toString());
        } else if ("notes".equalsIgnoreCase(criteria.getKey())) {
            origin.setNotes(criteria.getValue().toString());
        }
        return origin;
    }
}
