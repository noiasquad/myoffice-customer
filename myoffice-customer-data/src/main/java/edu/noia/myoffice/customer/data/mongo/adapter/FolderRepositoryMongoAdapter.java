package edu.noia.myoffice.customer.data.mongo.adapter;

import edu.noia.myoffice.customer.data.mongo.repository.MongoFolderStateRepository;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.repository.FolderRepository;
import edu.noia.myoffice.customer.domain.vo.FolderId;

public class FolderRepositoryMongoAdapter
        extends BaseFolderRepositoryMongoAdapter
        implements FolderRepository {

    public FolderRepositoryMongoAdapter(MongoFolderStateRepository repository) {
        super(repository);
    }

    @Override
    public void put(FolderId id, FolderState state) {
    }

    @Override
    public void remove(FolderId id) {
        repository
                .findById(id)
                .ifPresent(repository::delete);
    }
}
