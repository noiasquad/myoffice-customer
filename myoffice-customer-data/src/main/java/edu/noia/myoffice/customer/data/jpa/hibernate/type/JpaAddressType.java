package edu.noia.myoffice.customer.data.jpa.hibernate.type;

import edu.noia.myoffice.address.domain.vo.*;
import edu.noia.myoffice.common.data.jpa.hibernate.type.AbstractUserType;
import io.vavr.control.Try;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.StringType;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class JpaAddressType extends AbstractUserType {

    @Override
    public int[] sqlTypes() {
        return new int[]{
                StringType.INSTANCE.sqlType(),
                StringType.INSTANCE.sqlType(),
                StringType.INSTANCE.sqlType(),
                StringType.INSTANCE.sqlType(),
                StringType.INSTANCE.sqlType(),
                StringType.INSTANCE.sqlType(),
                StringType.INSTANCE.sqlType(),
                StringType.INSTANCE.sqlType()
        };
    }

    @Override
    public Class returnedClass() {
        return Address.class;
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner) throws SQLException {
        return Optional.ofNullable(rs.getString(names[0]))
                .map(AddressType::valueOf)
                .map(addressType -> getAddress(rs, names, addressType).getOrElse(() -> null))
                .orElseThrow(() -> new IllegalArgumentException("Address value not readable"));
    }

    private Try<Address> getAddress(ResultSet rs, String[] names, AddressType addressType) {
        return Try.of(() -> {
            switch (addressType) {
                case POSTAL:
                    return new PostalAddress()
                            .setStreet(rs.getString(names[2]))
                            .setHouseNbr(rs.getString(names[3]))
                            .setZip(rs.getString(names[4]))
                            .setCity(rs.getString(names[5]))
                            .setRegion(rs.getString(names[6]))
                            .setCountry(rs.getString(names[7]))
                            .setTags(rs.getString(names[1]));
                case PHONE:
                    return new PhoneAddress()
                            .setNumber(rs.getString(names[2]))
                            .setTags(rs.getString(names[1]));
                case EMAIL:
                    return new EmailAddress()
                            .setEmail(rs.getString(names[2]))
                            .setTags(rs.getString(names[1]));
                case WEB:
                    return new WebAddress()
                            .setUrl(rs.getString(names[2]))
                            .setTags(rs.getString(names[1]));
                default:
                    throw new IllegalArgumentException("Address type is unknown");
            }
        });
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws SQLException {
        if (value != null) {
            Address address = (Address) value;
            st.setString(index++, address.getType().name());
            st.setString(index++, address.getTags());

            switch (address.getType()) {
                case POSTAL:
                    PostalAddress postalAddress = (PostalAddress) address;
                    st.setString(index++, postalAddress.getStreet());
                    st.setString(index++, postalAddress.getHouseNbr());
                    st.setString(index++, postalAddress.getZip());
                    st.setString(index++, postalAddress.getCity());
                    st.setString(index++, postalAddress.getRegion());
                    st.setString(index, postalAddress.getCountry());
                    break;
                case PHONE:
                    PhoneAddress phoneAddress = (PhoneAddress) address;
                    st.setString(index++, phoneAddress.getNumber());
                    st.setNull(index++, StringType.INSTANCE.sqlType());
                    st.setNull(index++, StringType.INSTANCE.sqlType());
                    st.setNull(index++, StringType.INSTANCE.sqlType());
                    st.setNull(index++, StringType.INSTANCE.sqlType());
                    st.setNull(index, StringType.INSTANCE.sqlType());
                    break;
                case EMAIL:
                    EmailAddress emailAddress = (EmailAddress) address;
                    st.setString(index++, emailAddress.getEmail());
                    st.setNull(index++, StringType.INSTANCE.sqlType());
                    st.setNull(index++, StringType.INSTANCE.sqlType());
                    st.setNull(index++, StringType.INSTANCE.sqlType());
                    st.setNull(index++, StringType.INSTANCE.sqlType());
                    st.setNull(index, StringType.INSTANCE.sqlType());
                    break;
                case WEB:
                    WebAddress webAddress = (WebAddress) address;
                    st.setString(index++, webAddress.getUrl());
                    st.setNull(index++, StringType.INSTANCE.sqlType());
                    st.setNull(index++, StringType.INSTANCE.sqlType());
                    st.setNull(index++, StringType.INSTANCE.sqlType());
                    st.setNull(index++, StringType.INSTANCE.sqlType());
                    st.setNull(index, StringType.INSTANCE.sqlType());
                    break;
            }
        } else {
            for (int type : sqlTypes()) {
                st.setNull(index++, type);
            }
        }
    }
}
