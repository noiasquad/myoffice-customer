package edu.noia.myoffice.customer.data.jpa.entity;

import edu.noia.myoffice.common.data.jpa.JpaBaseEntity;
import edu.noia.myoffice.customer.domain.entity.Folder;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.vo.Affiliate;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static edu.noia.myoffice.common.util.exception.ExceptionSuppliers.itemNotFound;
import static edu.noia.myoffice.common.util.exception.ExceptionSuppliers.notFound;

@Table(name = "folder")
@Entity
@EqualsAndHashCode(of = "id", callSuper = false)
@Accessors(chain = true)
@Getter
@Setter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JpaFolderState extends JpaBaseEntity implements FolderState {

    FolderId id;
    String name;
    String notes;

    @ElementCollection
    @CollectionTable(name = "folder_affiliate", joinColumns = @JoinColumn(name = "fk_folder"))
    Set<CustomerId> affiliates = new HashSet<>();

    CustomerId primaryAffiliate;

    public static JpaFolderState of(FolderState state) {
        return (JpaFolderState) new JpaFolderState().modify(state);
    }

    @Override
    public JpaFolderState addAffiliate(CustomerId customerId) {
        getAffiliates().add(customerId);
        return this;
    }

    @Override
    public JpaFolderState removeAffiliate(CustomerId customerId) {
        if (!getAffiliates().remove(customerId)) {
            throw itemNotFound(Affiliate.class, customerId, Folder.class, id).get();
        }
        return this;
    }

}
