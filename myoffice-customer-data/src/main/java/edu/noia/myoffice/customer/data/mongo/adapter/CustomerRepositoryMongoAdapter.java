package edu.noia.myoffice.customer.data.mongo.adapter;

import edu.noia.myoffice.customer.data.mongo.repository.MongoCustomerStateRepository;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.repository.CustomerRepository;
import edu.noia.myoffice.customer.domain.vo.CustomerId;

public class CustomerRepositoryMongoAdapter
        extends BaseCustomerRepositoryMongoAdapter
        implements CustomerRepository {

    public CustomerRepositoryMongoAdapter(MongoCustomerStateRepository repository) {
        super(repository);
    }

    @Override
    public void put(CustomerId id, CustomerState state) {
    }

    @Override
    public void remove(CustomerId id) {
        repository
                .findById(id)
                .ifPresent(repository::delete);
    }
}
