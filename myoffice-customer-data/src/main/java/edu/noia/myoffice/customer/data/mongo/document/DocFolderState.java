package edu.noia.myoffice.customer.data.mongo.document;

import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.vo.Affiliate;
import edu.noia.myoffice.customer.domain.vo.CustomerId;
import edu.noia.myoffice.customer.domain.vo.FolderId;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Set;

import static edu.noia.myoffice.common.util.exception.ExceptionSuppliers.notFound;

@Document("folder")
@Accessors(chain = true)
@Data
public class DocFolderState implements FolderState {

    @Id
    FolderId id;
    String name;
    String notes;
    Set<CustomerId> affiliates = new HashSet<>();
    CustomerId primaryAffiliate;

    public static DocFolderState of(FolderState state) {
        return (DocFolderState) new DocFolderState().modify(state);
    }

    @Override
    public DocFolderState addAffiliate(CustomerId affiliate) {
        getAffiliates().add(affiliate);
        return this;
    }

    @Override
    public DocFolderState removeAffiliate(CustomerId affiliate) {
        if (!getAffiliates().remove(affiliate)) {
            throw notFound(Affiliate.class, affiliate).get();
        }
        return this;
    }

}
