package edu.noia.myoffice.customer.data.jpa.adapter;

import edu.noia.myoffice.customer.data.jpa.mapper.CustomerMapper;
import edu.noia.myoffice.customer.data.jpa.repository.JpaCustomerStateRepository;
import edu.noia.myoffice.customer.domain.entity.CustomerState;
import edu.noia.myoffice.customer.domain.repository.CustomerRepository;
import edu.noia.myoffice.customer.domain.vo.AffiliationId;
import edu.noia.myoffice.customer.domain.vo.CustomerId;

import java.util.Collections;
import java.util.List;

public class CustomerRepositoryJpaAdapter
        extends BaseCustomerRepositoryJpaAdapter
        implements CustomerRepository {

    public CustomerRepositoryJpaAdapter(JpaCustomerStateRepository repository) {
        super(repository);
    }

    @Override
    public void put(CustomerId id, CustomerState state) {
        repository.save(CustomerMapper.toJpaEntity.map(state).setId(id));
    }

    @Override
    public void remove(CustomerId id) {
        repository
                .findById(id)
                .ifPresent(repository::delete);
    }
}
