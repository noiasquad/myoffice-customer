package edu.noia.myoffice.customer.data.mongo.adapter;

import edu.noia.myoffice.customer.data.mongo.repository.MongoFolderStateRepository;
import edu.noia.myoffice.customer.application.query.FolderReadRepository;

public class FolderReadRepositoryMongoAdapter
        extends BaseFolderRepositoryMongoAdapter
        implements FolderReadRepository {

    public FolderReadRepositoryMongoAdapter(MongoFolderStateRepository repository) {
        super(repository);
    }
}
