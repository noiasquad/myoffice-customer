package edu.noia.myoffice.customer.data.mongo.adapter;

import edu.noia.myoffice.customer.application.query.CustomerReadRepository;
import edu.noia.myoffice.customer.data.mongo.repository.MongoCustomerStateRepository;

public class CustomerReadRepositoryMongoAdapter
        extends BaseCustomerRepositoryMongoAdapter
        implements CustomerReadRepository {

    public CustomerReadRepositoryMongoAdapter(MongoCustomerStateRepository repository) {
        super(repository);
    }
}
