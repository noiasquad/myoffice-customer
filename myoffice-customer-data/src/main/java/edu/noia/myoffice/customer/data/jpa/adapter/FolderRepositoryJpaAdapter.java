package edu.noia.myoffice.customer.data.jpa.adapter;

import edu.noia.myoffice.customer.data.jpa.mapper.FolderMapper;
import edu.noia.myoffice.customer.data.jpa.repository.JpaFolderStateRepository;
import edu.noia.myoffice.customer.domain.entity.FolderState;
import edu.noia.myoffice.customer.domain.repository.FolderRepository;
import edu.noia.myoffice.customer.domain.vo.FolderId;

public class FolderRepositoryJpaAdapter
        extends BaseFolderRepositoryJpaAdapter
        implements FolderRepository {

    public FolderRepositoryJpaAdapter(JpaFolderStateRepository repository) {
        super(repository);
    }

    @Override
    public void put(FolderId id, FolderState state) {
        repository.save(FolderMapper.toJpaEntity.map(state).setId(id));
    }

    @Override
    public void remove(FolderId id) {
        repository
                .findById(id)
                .ifPresent(repository::delete);
    }
}
