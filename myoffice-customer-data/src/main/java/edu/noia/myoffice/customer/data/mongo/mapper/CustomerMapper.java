package edu.noia.myoffice.customer.data.mongo.mapper;

import edu.noia.myoffice.common.util.mapper.Mapper;
import edu.noia.myoffice.customer.data.mongo.document.DocCustomerState;
import edu.noia.myoffice.customer.domain.entity.CustomerState;

public interface CustomerMapper {

    // Optimized mapping thank to the use of interfaces
    Mapper<CustomerState, DocCustomerState> toDocument = state ->
            state instanceof DocCustomerState ? (DocCustomerState) state : DocCustomerState.of(state);

    // Optimized mapping thank to the use of interfaces
    Mapper<DocCustomerState, CustomerState> toDomainEntity = state -> state;
}
