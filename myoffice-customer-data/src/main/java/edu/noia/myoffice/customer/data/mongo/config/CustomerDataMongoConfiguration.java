package edu.noia.myoffice.customer.data.mongo.config;

import edu.noia.myoffice.customer.application.query.CustomerReadRepository;
import edu.noia.myoffice.customer.application.query.FolderReadRepository;
import edu.noia.myoffice.customer.data.mongo.adapter.CustomerReadRepositoryMongoAdapter;
import edu.noia.myoffice.customer.data.mongo.adapter.CustomerRepositoryMongoAdapter;
import edu.noia.myoffice.customer.data.mongo.adapter.FolderReadRepositoryMongoAdapter;
import edu.noia.myoffice.customer.data.mongo.adapter.FolderRepositoryMongoAdapter;
import edu.noia.myoffice.customer.data.mongo.repository.MongoCustomerStateRepository;
import edu.noia.myoffice.customer.data.mongo.repository.MongoFolderStateRepository;
import edu.noia.myoffice.customer.domain.repository.CustomerRepository;
import edu.noia.myoffice.customer.domain.repository.FolderRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Profile("db_mongo")
@EnableMongoRepositories(
        basePackageClasses = {
                MongoCustomerStateRepository.class,
                MongoFolderStateRepository.class
        }
)
@Configuration
public class CustomerDataMongoConfiguration {

    @Bean("data.customer.repository")
    public CustomerRepository customerRepository(MongoCustomerStateRepository repository) {
        return new CustomerRepositoryMongoAdapter(repository);
    }

    @Bean("data.customer.read.repository")
    public CustomerReadRepository customerReadRepository(MongoCustomerStateRepository repository) {
        return new CustomerReadRepositoryMongoAdapter(repository);
    }

    @Bean("data.folder.repository")
    public FolderRepository folderRepository(MongoFolderStateRepository repository) {
        return new FolderRepositoryMongoAdapter(repository);
    }

    @Bean("data.folder.read.repository")
    public FolderReadRepository folderReadRepository(MongoFolderStateRepository repository) {
        return new FolderReadRepositoryMongoAdapter(repository);
    }
}
